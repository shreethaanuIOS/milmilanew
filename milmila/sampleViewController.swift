//
//  sampleViewController.swift
//  milmila
//
//  Created by prasun sarkar on 1/22/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit

class sampleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var collectionCunt: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
collectionCunt = 5
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sampleCollectionViewCell", for: indexPath) as! sampleCollectionViewCell
    cell.mylogo.image = #imageLiteral(resourceName: "Templogo")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionCunt
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return collectionCunt
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 210
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cells = tableView.dequeueReusableCell(withIdentifier:"SAMPLETableViewCell", for: indexPath) as! SAMPLETableViewCell
        return cells
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
