//
//  ExtendedCategoryTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class ExtendedCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var extCategoryImage: UIImageView!
    @IBOutlet weak var extCategoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
