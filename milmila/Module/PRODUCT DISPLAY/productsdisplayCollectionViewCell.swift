//
//  productsdisplayCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class productsdisplayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productlistname: UILabel!
    @IBOutlet weak var productsimage: UIImageView!
}
