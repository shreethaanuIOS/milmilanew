
//
//  productsDisplayViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class productsDisplayViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    @IBOutlet weak var productsdisplay: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        productsdisplay.dataSource = self
        productsdisplay.backgroundColor = UIColor.white
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 60
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productsdisplay", for: indexPath) as! productsdisplayCollectionViewCell
        
        cell.backgroundColor = UIColor.white
        cell.productsimage.image = #imageLiteral(resourceName: "shirt")
        cell.productlistname.text = "New Autumn Fashion Brand Men Clothes Slim Fit Men Long Sleeve Shirt Men Plaid Cotton Casual Men Shirt Social Plus Size M-5XL"
        return cell
    }
}
