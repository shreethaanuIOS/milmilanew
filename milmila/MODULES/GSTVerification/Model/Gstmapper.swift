//
//  Gstmapper.swift
//  milmila
//
//  Created by prasun sarkar on 2/7/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class Gstmapper: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var  respdata:  login!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
    }
}

/*
 
 {
 "msg":"Gst Verification submitted successfully",
 "sCode":200,
 "data":1,
 "error":false,
 "status":true}
 */
