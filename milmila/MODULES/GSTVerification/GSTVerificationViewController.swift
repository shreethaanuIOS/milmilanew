//
//  GSTVerificationViewController.swift
//  milmila
//
//  Created by prasun sarkar on 2/7/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper

class GSTVerificationViewController: UIViewController {

    @IBOutlet weak var greaterImg: UIImageView!
  
    @IBOutlet weak var greater: UIButton!
    
    @IBOutlet weak var lesser: UIButton!
    
    @IBOutlet weak var buissnessName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phoneNumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var gstnumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var pannumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lesserimg: UIImageView!
  
    @IBOutlet weak var submit: UIButton!
    
    var httpRequest = CSSwiftApiHttpRequest()
    var gstOpt = "N"
    var gstEmail = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        gstnumber.isEnabled = false
        gstEmail =  (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String)!
    }
    
    @IBAction func closeAction(_ sender: Any) {
     
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.present(vc, animated: true, completion: nil)
   
    }
    
    
    
    @IBAction func gstverification(_ sender: Any) {
    GSTSubmit()
    }
    
    @IBAction func greater(_ sender: Any) {
        if greaterImg.image == #imageLiteral(resourceName: "roundFill") {
          greaterImg.image = #imageLiteral(resourceName: "round")
        
        }
        else {
    greaterImg.image = #imageLiteral(resourceName: "roundFill")
    lesserimg.image = #imageLiteral(resourceName: "round")
    gstOpt = "Y"
    gstnumber.isEnabled = true
        }
    }
    
    @IBAction func lesser(_ sender: Any) {
        if lesserimg.image == #imageLiteral(resourceName: "roundFill") {
            lesserimg.image = #imageLiteral(resourceName: "round")
        }
        else {
            greaterImg.image = #imageLiteral(resourceName: "round")
            lesserimg.image = #imageLiteral(resourceName: "roundFill")
            gstOpt = "N"
            gstnumber.isEnabled = false
    }
    }
    
    func GSTSubmit()
    {
        let url:String = BASE_URL + "customerB2B/gstVerfication"
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        
        let params:[String:String] = [
            "gst":       gstnumber.text!,
            "gst_name":    buissnessName.text!,
            "gst_addr": phoneNumber.text!,
            "gst_zipcode": "",
            "pan_number": pannumber.text!,
            "is_gst_optional": gstOpt,
            "user_id": myuserid,
            "uemail": gstEmail
        ]
        print(params)
        
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<Gstmapper>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
                self.showToastMessageTop(message:"GST verification done")
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                self.present(vc, animated: true, completion: nil)
            }
            else {
                self.showToastMessageTop(message:"Internal Server error")
            }
        })
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension GSTVerificationViewController : UITextFieldDelegate {
    /// Scrollview's ContentInset Change on Keyboard hide
    
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    /// register Keyboard
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField!)
    {
       // pincode = textField as! SkyFloatingLabelTextField!
    }
    
    func textFieldDidEndEditing(textField: UITextField!)
    {
       // pincode = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == buissnessName {
            _ = phoneNumber.becomeFirstResponder()
        } else if textField == phoneNumber {
            _ = gstnumber.becomeFirstResponder()
        }
        else if textField == gstnumber {
            _ = pannumber.becomeFirstResponder()
        }
        else if textField == pannumber {
            dismissKeyboard()
        }
        
        return true
    }
}



