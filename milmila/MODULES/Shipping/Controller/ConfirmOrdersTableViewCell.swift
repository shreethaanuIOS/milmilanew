//
//  ConfirmOrdersTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 12/12/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class ConfirmOrdersTableViewCell: UITableViewCell {

    @IBOutlet weak var dispPrice: UILabel!
    @IBOutlet weak var dispQty: UILabel!
    @IBOutlet weak var dispColour: UILabel!
    @IBOutlet weak var dispSize: UILabel!
    @IBOutlet weak var displayName: UILabel!
    @IBOutlet weak var displayImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
