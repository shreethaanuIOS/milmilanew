//
//  ShippingViewController.swift
//  milmila
//
//  Created by prasun sarkar on 12/12/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper
import KRProgressHUD

class ShippingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, PGTransactionDelegate, UIWebViewDelegate {

    // SHIPPING VIEW
    @IBOutlet weak var condfrimaddress: UILabel!
    @IBOutlet weak var confirmaddressname: UILabel!
    @IBOutlet weak var shippingAddressTable: UITableView!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var shippingView: UIView!
    @IBOutlet weak var addressSegment: UISegmentedControl!
    @IBOutlet weak var confirmationButton: UIButton!
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var shippingButton: UIButton!
    @IBOutlet weak var confirmationline: UIView!
    @IBOutlet weak var paymentline: UIView!
    @IBOutlet weak var shippingLine: UIView!
    @IBOutlet weak var shippingScroll: UIScrollView!
    
    @IBOutlet weak var firstname: SkyFloatingLabelTextField!

    @IBOutlet weak var lastname: SkyFloatingLabelTextField!
    
    @IBOutlet weak var address: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phonenumber: SkyFloatingLabelTextField!
    
    @IBOutlet weak var city: SkyFloatingLabelTextField!
    
    @IBOutlet weak var state: SkyFloatingLabelTextField!
    
    @IBOutlet weak var country: SkyFloatingLabelTextField!
    
    @IBOutlet weak var zipcode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var noaddress: UIView!
    
    // PAYMENT VIEW
    
    @IBOutlet weak var nopromowholeview: UIView!
    @IBOutlet weak var nopromoview: UIView!
    @IBOutlet weak var paymentLabel: UILabel!
    @IBOutlet weak var paymrntView: UIView!
    @IBOutlet weak var promocode: UIView!
    @IBOutlet weak var promocodeText: UITextField!
    @IBOutlet weak var paymentTable: UITableView!
    @IBOutlet weak var continueToconfirm: UIButton!
    @IBOutlet weak var totalamount: UILabel!
   
    var merchant:PGMerchantConfiguration!
    var paymentType: String!
    var cellSpacingHeight: CGFloat = 5
    var httpRequest = CSSwiftApiHttpRequest()
    var UID = ""
    var paymentGateway = ""
    // CONFIRMATION VIEW
    @IBOutlet weak var confirmView: UIView!
    @IBOutlet weak var confirmLabel: UILabel!
    @IBOutlet weak var ordersTableview: UITableView!
    
    @IBOutlet weak var TotalAmount: UILabel!
    @IBOutlet weak var deliveryChanrge: UILabel!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var orderTableViewHeightConstraint: NSLayoutConstraint!
    
    var menuImageArray = ["PAYTM","CCAVENUE"]
    var AddressListingArray = [addresslist]()
    var checksumhash = ""
    var orderId = ""
    var txnamt = ""
    var custId = ""
    var callurl = ""
   
    var CHANNEL_ID = ""
    var MID = ""
    var WEBSITE = ""
    var INDUSTRY_TYPE_ID = ""
    
    let diceRoll = Int(arc4random_uniform(6))
    var cartListing = [cartListMapper]()
    var imgname : String!
    var addresses = ""
    var cityname = ""
    var countrys = ""
    var zipcod: String!
    var states = ""
    var myProdId = ""
    
    var fromWay = ""
    var mybuynowdata = ""
    var buyNowProductRelateId = ""
    var txnamount : Int?
    var GATEWAYNAME = ""
    var TXNID : Int?
    var RESPCODE = ""
    var RESPMSG = ""
    var BANKNAME = ""
    var PAYMENTMODE = ""
    var BANKTXNID : Int?
    var STATUS = ""
    var pTxnRespCode = ""
    var payDate = ""
    let arrayValue = [String]() //if it is a String array
    var addrId  = "0"
    var myzipCode = ""
    var zipcodeVerified = ""
    
    /// CC Avenue CODE
    
    var accessCode = String()
    var merchantId = String()
    var ordersId = String()
    var amount = String()
    var currency = String()
    var redirectUrl = String()
    var cancelUrl = String()
    var rsaKeyUrl = String()
    var rsaKeyDataStr = String()
    var rsaKey = String()
    static var statusCode = 0//zero means success or else error in encrption with rsa
    var encVal = String()
    
    lazy var viewWeb: UIWebView = {
        let webView = UIWebView()
        webView.backgroundColor = .white
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.delegate = self
        webView.scalesPageToFit = true
        webView.contentMode = UIViewContentMode.scaleAspectFill
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nopromowholeview.isHidden = true
        nopromoview.isHidden = true
        shippingAddressTable.isHidden = true
        confirmationButton.isEnabled = false
        paymentButton.isEnabled = false
        paymrntView.isHidden = true
        confirmView.isHidden = true
        shippingView.isHidden = false
        paymentline.isHidden = true
        noaddress.isHidden = true
        confirmationline.isHidden = true
        shippingLine.isHidden = false
        shippingLabel.textColor = UIColor.myBlueColour
        confirmaddressname.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
        setMerchant()//initialize merchant with config.
        addDoneButtonOnKeyboard()
        addressListing()
        print("MY BNID = " + mybuynowdata)
        print(fromWay)
    }
    
    func BuyNowPrepayment(){
        
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String =  BASE_URL + "transaction/prepayment/pay.html"
        let params:[String:String] = [
            "userId":       myuserid,
            "paymentType":  paymentGateway,
            "orderType":    "BuyNow",
            "bnId": mybuynowdata,
            "addrId": addrId
        ]
        print(url)
          print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<prepaymentMapper>().map(JSONString: content!)
            print(responseData)
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.checksumhash = (responseData?.respdata.CHECKSUMHASH)!
                self.txnamt = (responseData?.respdata.TXN_AMOUNT)!
                self.orderId = (responseData?.respdata.ORDER_ID)!
                self.custId = (responseData?.respdata.CUST_ID)!
                self.callurl = (responseData?.respdata.CALLBACK_URL)!
                self.INDUSTRY_TYPE_ID = (responseData?.respdata.INDUSTRY_TYPE_ID)!
                self.CHANNEL_ID = (responseData?.respdata.CHANNEL_ID)!
                self.WEBSITE = (responseData?.respdata.WEBSITE)!
                self.MID = (responseData?.respdata.MID)!
                
                self.deliveryChanrge.text = "₹ " + String(describing: self.cartListing[0].shipping_cost!)
                self.orderTotal.text = "₹  " + self.txnamt
               
                self.totalamount.text = "₹  " + self.txnamt
                self.TotalAmount.text = "₹  " + self.txnamt
            }
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
    }
    
    // Hit before payment
    func prepayment(){
        
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String =  BASE_URL + "transaction/prepayment/pay.html"
        let params:[String:String] = [
            "userId":       myuserid,
            "paymentType":  paymentGateway,
            "orderType":    "UserCart",
            "bnId": "0",
            "addrId": addrId
        ]
        print(url)
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<prepaymentMapper>().map(JSONString: content!)
             print(responseData)
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.checksumhash = (responseData?.respdata.CHECKSUMHASH)!
                self.txnamt = (responseData?.respdata.TXN_AMOUNT)!
                self.orderId = (responseData?.respdata.ORDER_ID)!
                self.custId = (responseData?.respdata.CUST_ID)!
                self.callurl = (responseData?.respdata.CALLBACK_URL)!
                self.INDUSTRY_TYPE_ID = (responseData?.respdata.INDUSTRY_TYPE_ID)!
                self.CHANNEL_ID = (responseData?.respdata.CHANNEL_ID)!
                self.WEBSITE = (responseData?.respdata.WEBSITE)!
                self.MID = (responseData?.respdata.MID)!
                
                self.deliveryChanrge.text = "₹ " + String(describing: self.cartListing[0].shipping_cost!)
                self.orderTotal.text = "₹  " + self.self.txnamt
                let MyTotalValue = self.cartListing[0].shipping_cost!
                self.totalamount.text = "₹  " + self.txnamt
                self.TotalAmount.text = "₹  " + self.txnamt
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
    }
    
    // Hit after Payment
    func postPayment(){
        
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String =  BASE_URL + "transaction/postPayment/doInsert.html"
        let params:[String:AnyObject] = [
            "userId":   myuserid as AnyObject,
            "orderId":  orderId as AnyObject,
            "pTxnRefNo":   TXNID as AnyObject,
            "pTxnRespCode": pTxnRespCode as AnyObject,
            "pTxnRespMsg":   RESPMSG as AnyObject,
            "pGatewayName":  GATEWAYNAME as AnyObject,
            "pBankName":    BANKNAME as AnyObject,
            "paymentMode": PAYMENTMODE as AnyObject,
            "paymentAmt":    txnamt as AnyObject,
            "pBankTxnRefNo": BANKTXNID as AnyObject,
            "pTxnStatus": STATUS as AnyObject
        ]
        
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<postpaymentMapper>().map(JSONString: content!)
            
            if(responseData?.sCode == 200)
            {
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
                self.present(vc, animated: true, completion: nil)
                
            }
                
            else if(responseData?.sCode == 500) {
                let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "oopsViewController") as! oopsViewController
                self.present(vc, animated: true, completion: nil)
              
            }
            else {
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "oopsViewController") as! oopsViewController
                self.present(vc, animated: true, completion: nil)
                self.showToastMessageTop(message: " OOOPS !! something went wrong")
            }
        })
    }
    
    
    
    func setMerchant(){
        merchant  = PGMerchantConfiguration.default()!
        //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
       // merchant.checksumGenerationURL = "http://getlook.in/cgi-bin/checksum_generate.cgi";
       // merchant.checksumValidationURL = "http://getlook.in/cgi-bin/checksum_validate.cgi";
        
        // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
        merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
        merchant.clientSSLCertPassword = nil; //@"password";
        
        //configure the PGMerchantConfiguration object specific to your requirements
        merchant.merchantID = MID;//paste here your merchant id  //mandatory
        merchant.website = WEBSITE;//mandatory
        merchant.industryID = INDUSTRY_TYPE_ID;//mandatory
        merchant.channelID = CHANNEL_ID; //provided by PG WAP //mandatory
    
    }
    
    
    func createPayment(){
        
        var orderDict = [String : String]()
     //   orderDict["MERCHANT_KEY"] = "DykWP&oQAExyw59I";
        orderDict["MID"] = MID;//paste here your merchant id   //mandatory
        orderDict["CHANNEL_ID"] = CHANNEL_ID; // paste here channel id                       // mandatory
        orderDict["INDUSTRY_TYPE_ID"] = INDUSTRY_TYPE_ID;//paste industry type              //mandatory
        orderDict["WEBSITE"] = WEBSITE;// paste website                            //mandatory
        //Order configuration in the order object
        orderDict["TXN_AMOUNT"] = txnamt; // amount to charge                      // mandatory
        orderDict["ORDER_ID"] = orderId;//change order id every time on new transaction
        orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
        orderDict["CUST_ID"] = custId; // change acc. to your database user/customers
        //orderDict["MOBILE_NO"] = "7777777777";// optional
      //  orderDict["EMAIL"] = "shree@milmila.com"; //optional
        orderDict["CHECKSUMHASH"] = checksumhash;
        orderDict["CALLBACK_URL"] = callurl
        
        let pgOrder = PGOrder(params: orderDict)
        
        let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
        transaction!.serverType = eServerTypeProduction
        transaction!.merchant = merchant
        transaction!.loggingEnabled = true
        transaction!.delegate = self
        self.present(transaction!, animated: true, completion: {
            self.showAlert(title: "paytm response", message: "completd transaction")
        })
        
        
    }
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }

/*all actions related to transaction are catched here*/



    @IBAction func continueToPayment(_ sender: Any) {
        
        if isValidationSuccess(){
            zipCodeVerifier()
            shipping()
            shippingButton.isEnabled = true
            paymentButton.isEnabled = true
            confirmationButton.isEnabled = false
            shippingView.isHidden = true
            paymrntView.isHidden = false
            confirmView.isHidden = true
            paymentline.isHidden = false
            confirmationline.isHidden = true
            shippingLine.isHidden = true
            paymentLabel.textColor = UIColor.myBlueColour
        }
    }
    @IBAction func continueToConfirm(_ sender: Any) {
        shippingButton.isEnabled = true
        paymentButton.isEnabled = true
        confirmationButton.isEnabled = true
        shippingView.isHidden = true
        paymrntView.isHidden  = true
        confirmView.isHidden  = false
        paymentline.isHidden = true
        confirmationline.isHidden = false
        shippingLine.isHidden = true
        confirmLabel.textColor = UIColor.myBlueColour
        
     //  addresses = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csaddress) as! String
        if fromWay == "BUYNOW" {
            BuyNowListing()
        }
        else {
            cartList()
        }
        
        confirmaddressname.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName) as? String
       
        cityname = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstreet) as! String
        states = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstateee) as! String
    
        condfrimaddress.text =  cityname + "," + states + "," + myzipCode
        
        self.orderTotal.text = "₹ " + self.txnamt
        self.totalamount.text = "₹ " + self.txnamt
        self.TotalAmount.text = "₹ " + self.txnamt
      
        
        ordersTableview.reloadData()
}
    
    func BuyNowListing() {
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myUId = String(UID)
        let myzippingCoding  = myzipCode
        let url:String = BASE_URL + "customerB2B/buyNowList?userId=" + myUId + "&userPinCode=" + myzippingCoding + "&bnId=" + mybuynowdata + "&bnDetailId=" + buyNowProductRelateId
    
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            self.parserJSON(data: response as! Data)
            KRProgressHUD.dismiss()
            self.ordersTableview.reloadData()
           self.BuyNowPrepayment()
//            self.deliveryChanrge.text = "₹  " + String(describing: self.cartListing[0].grandTotal!)
//            self.orderTotal.text = "₹  " + String(describing: self.cartListing[0].total_val!)
//            self.totalamount.text =  "₹  " + String(describing: self.cartListing[0].grandTotal!)
//            self.TotalAmount.text =  "₹  " + String(describing: self.cartListing[0].grandTotal!)
        })
    }
    
    @IBAction func addresssegment(_ sender: Any) {
        
        switch addressSegment.selectedSegmentIndex
        {
        case 0:
          
            shippingAddressTable.isHidden = true
          
            firstname.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csaddressname) as? String
            state.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstateee) as? String
            country.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.cscountry) as? String
            phonenumber.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csphonenumber) as? String
       //     zipcode.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.cszip) as? String
            city.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstreet) as? String
            self.view.endEditing(true)
      
           case 1:
           addressListing()
           if self.AddressListingArray.count == 0 {
            shippingAddressTable.isHidden = true
            }
           else {
            shippingAddressTable.isHidden = false
            }
        default:
            break;
        }
    }
    
    
    @IBAction func shippingaction(_ sender: Any) {
   
    }
    
    @IBAction func paymentAction(_ sender: Any) {
      
    }
    
    @IBAction func confirmAction(_ sender: Any) {
    
    }
    
    @IBAction func applyPromoCode(_ sender: Any) {
         nopromowholeview.isHidden = false
         nopromoview.isHidden = false
         dismissKeyboard()
    }
    
    @IBAction func closepromo(_ sender: Any) {
   
    nopromoview.isHidden = true
    }
    
    
    @IBAction func OrderNow(_ sender: Any) {
        if paymentGateway == "PAYTM" {
        createPayment()
        }
        else {
            view.backgroundColor = .white
            setupWebView()
        }
    }

    
    private func setupWebView(){
        
        //setup webview
        view.addSubview(viewWeb)
        if #available(iOS 11.0, *) {
            viewWeb.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
            viewWeb.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            // Fallback on earlier versions
            viewWeb.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            viewWeb.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        
        viewWeb.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        
        viewWeb.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        _ = [viewWeb .setContentCompressionResistancePriority(1000, for: UILayoutConstraintAxis.vertical)]
    }
    
    private func gettingRsaKey(completion: @escaping (_ success: Bool, _ object: AnyObject?) -> ()){
        
        let serialQueue = DispatchQueue(label: "serialQueue", qos: .userInitiated)
        
        serialQueue.sync {
            self.rsaKeyDataStr = "access_code=\(self.accessCode)&order_id=\(self.orderId)"
            //            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8, allowLossyConversion: true)
            
            let requestData = self.rsaKeyDataStr.data(using: String.Encoding.utf8)
            
            guard let urlFromString = URL(string: self.rsaKeyUrl) else{
                return
            }
            var urlRequest = URLRequest(url: urlFromString)
            urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            urlRequest.httpMethod = "POST"
            urlRequest.httpBody = requestData
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)
            
            
            session.dataTask(with: urlRequest as URLRequest) {
                (data, response, error) -> Void in
                
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{
                    
                    guard let data = data else{
                        print("No value for data")
                        completion(false, "Not proper data for RSA Key" as AnyObject?)
                        return
                    }
                    print("data :: ",data)
                    completion(true, data as AnyObject?)
                }
                else{
                    completion(false, "Unable to generate RSA Key please check" as AnyObject?)
                }
                }.resume()
        }
    }
    
    var request: NSMutableURLRequest?
    
    /**
     * encyptCardDetails method we will use the rsa key to encrypt amount and currency and onece the encryption is done we will pass this encrypted data to the initTrans to initiate payment
     */
    
    private func encyptCardDetails(data: Data){
        guard let rsaKeytemp = String(bytes: data, encoding: String.Encoding.ascii) else{
            print("No value for rsaKeyTemp")
            return
        }
        //  rsaKey = rsaKeytemp
        
        //  rsaKey = self.rsaKey.trimmingCharacters(in: CharacterSet.newlines)
        
        rsaKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArksOFGzX972stoZTD2D92FWEYSHahfM/\nRFrF2/tQl+GqlQJRtWpQ/kBk+ZU+gCIYmzZdpt1w5QqKzfl6i4S5woa21PV4Ro6Hou9eOQhXrOBI\nuJn9KFWM/QHnVkv/1BuhM/yr44JWVYXSIE36i5nLL0BUEesGEYP3NKDJI3/CrMvErrDZ7PfTrsX3\nPVuj19eA0l/Q19USTe+vZKNWCt4goYDkMF5hClXJUuZmAEwSDDvAPS/VvhoQCzAYz79knUlDHeg8\nU9t2j4Q0iiReLk5vULkYegxDLEirtabXrGpFRxScR2LVnFE1Yam6wGUp05TlkyAIY10X04g7Lw3n\nLfDPTwIDAQAB"
        rsaKey =  "-----BEGIN PUBLIC KEY-----\n\(self.rsaKey)\n-----END PUBLIC KEY-----\n"
        print("rsaKey :: ",rsaKey)
        
        let myRequestString = "amount=\(amount)&currency=\(currency)"
        
        let ccTool = CCTool()
        var encVal = ccTool.encryptRSA(myRequestString, key: rsaKey)
        
        encVal = CFURLCreateStringByAddingPercentEscapes(
            nil,
            encVal! as CFString,
            nil,
            "!*'();:@&=+$,/?%#[]" as CFString,
            CFStringBuiltInEncodings.UTF8.rawValue) as String?
        CCWebViewController.statusCode = 0
        
        //Preparing for webview call
        if CCWebViewController.statusCode == 0{
            let urlAsString = "https://secure.ccavenue.com/transaction/initTrans"
            let encryptedStr = "merchant_id=\(merchantId)&order_id=\(orderId)&redirect_url=\(redirectUrl)&cancel_url=\(cancelUrl)&enc_val=\(encVal!)&access_code=\(accessCode)"
            
            print("encValue :: \(encVal ?? "No val for encVal")")
            
            print("encryptedStr :: ",encryptedStr)
            let myRequestData = encryptedStr.data(using: String.Encoding.utf8)
            // request = NSMutableURLRequest(url: URL(string: urlAsString)!)
            
            request  = NSMutableURLRequest(url: URL(string: urlAsString)! as URL, cachePolicy: NSURLRequest.CachePolicy.reloadIgnoringCacheData, timeoutInterval: 30)
            request?.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "content-type")
            request?.setValue(urlAsString, forHTTPHeaderField: "Referer")
            request?.httpMethod = "POST"
            request?.httpBody = myRequestData
            print("\n\n\nwebview :: ",request?.url as Any)
            print("\n\n\nwebview :: ",request?.description as Any)
            print("\n\n\nwebview :: ",request?.httpBody?.description as Any)
            print("\n\n\nwebview :: ",request?.allHTTPHeaderFields! as Any)
            
            let session = URLSession(configuration: URLSessionConfiguration.default)
            print("session",session)
            
            session.dataTask(with: request! as URLRequest) {
                (data, response, error) -> Void in
                
                if let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode{
                    
                    guard let data = data else{
                        print("No value for data")
                        return
                    }
                    DispatchQueue.main.async {
                        self.viewWeb.loadRequest(self.request! as URLRequest)
                    }
                    print("data :: ",data)
                }
                else{
                    print("into else")
                    self.displayAlert(msg: "Unable to load webpage currently, Please try again later.")
                }
                }.resume()
            
            print(viewWeb.isLoading)
        }
        else{
            print("Unable to create requestURL")
            displayAlert(msg: "Unable to create requestURL")
        }
    }
    
    func displayAlert(msg: String){
        let alert: UIAlertController = UIAlertController(title: "ERROR", message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            UIAlertAction in
            LoadingOverlay.shared.hideOverlayView()
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func encryptRSA(raw: String, key pubKey: String) {
        
    }
    
    
    //MARK:
    //MARK: WebviewDelegate Methods
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print("webViewDidStartLoad",webView.request!)
        print(viewWeb.isLoading)
        print(request?.httpBodyStream as Any)
        print(request?.httpBody as Any)
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("Failed to load  webview")
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        LoadingOverlay.shared.hideOverlayView()
        
        //covert the response url to the string and check for that the response url contains the redirect/cancel url if true then chceck for the transaction status and pass the response to the result controller(ie. CCResultViewController)
        let string = (webView.request?.url?.absoluteString)!
        print("String :: \(string)")
        
        if(string.contains(redirectUrl)) //("http://122.182.6.216/merchant/ccavResponseHandler.jsp"))//
        {
            print(viewWeb.isLoading)
            guard let htmlTemp:NSString = webView.stringByEvaluatingJavaScript(from: "document.documentElement.outerHTML") as NSString? else{
                print("failed to evaluate javaScript")
                return
            }
            
            let html = htmlTemp
            print("html :: ",html)
            var transStatus = "Not Known"
            
            if ((html ).range(of: "tracking_id").location != NSNotFound) && ((html ).range(of: "bin_country").location != NSNotFound) {
                if ((html ).range(of: "Aborted").location != NSNotFound) || ((html ).range(of: "Cancel").location != NSNotFound) {
                    transStatus = "Transaction Cancelled"
                    let controller: CCResultViewController = CCResultViewController()
                    controller.transStatus = transStatus
                    self.present(controller, animated: true, completion: nil)
                }
                else if ((html ).range(of: "Success").location != NSNotFound) {
                    transStatus = "Transaction Successful"
                    let controller: CCResultViewController = CCResultViewController()
                    controller.transStatus = transStatus
                    self.present(controller, animated: true, completion: { _ in })
                }
                else if ((html ).range(of: "Fail").location != NSNotFound) {
                    transStatus = "Transaction Failed"
                    let controller: CCResultViewController = CCResultViewController()
                    controller.transStatus = transStatus
                    self.present(controller, animated: true, completion: { _ in })
                }
            }
            else{
                print("html does not contain any related data")
                displayAlert(msg: "html does not contain any related data for this transaction.")
            }
        }
    }
    
    func isValidationSuccess() -> Bool {
        
        if (firstname.text == "") {
            self.view.makeToast("Please enter the Name", duration: 3.0, position: .bottom)
            return false
        } else if (phonenumber.text == "") {
            self.view.makeToast("Please enter the Mobile number", duration: 3.0, position: .bottom)
            return false
        } else if (trimString(self.phonenumber.text!).count < 10) {
            self.view.makeToast("Please enter your valid MobileNo", duration: 3.0, position: .bottom)
            return false
        } else if (address.text == "") {
            self.view.makeToast("Please enter the address", duration: 3.0, position: .bottom)
            return false
        } else if (lastname.text == "") {
            self.view.makeToast("Please enter the lastname", duration: 3.0, position: .bottom)
            return false
        } else if (city.text == "") {
            self.view.makeToast("Please enter the city", duration: 3.0, position: .bottom)
            return false
        } else if (state.text == "") {
            self.view.makeToast("Please enter the state", duration: 3.0, position: .bottom)
            return false
        } else if (country.text == "") {
            self.view.makeToast("Please enter the country", duration: 3.0, position: .bottom)
            return false
        }else if (zipcode.text == "") {
           self.view.makeToast("Please enter the zipcode", duration: 3.0, position: .bottom)
        }else if (zipcodeVerified == "No") {
            self.view.makeToast("Please enter the Correct zipcode", duration: 3.0, position: .bottom)
             return false
        }else if (trimString(self.zipcode.text!).count < 6) {
            self.view.makeToast("Please enter your ZipCode", duration: 3.0, position: .bottom)
            return false
        }
        return true
    }
    
    
    func trimString(_ value: String) -> String {
        return value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    // SHIPPING API
    
    func shipping()
    {
        let url:String = BASE_URL + "customerB2B/doInsertAddress"
        
        let uid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuid = String(uid)
        
        let params:[String:String] = [
            "fname":       firstname.text!,
            "lname":       lastname.text!,
            "address":     address.text!,
            "phone":       phonenumber.text!,
            "city":        city.text!,
            "state":       state.text!,
            "country":     country.text!,
            "zipcode":     zipcode.text!,
            "deliver":     "Free",
            "customerId":      myuid,
            "user_address_id": "0"
        ]
        
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<ProfileModel>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
                GlobalConstants.General.USERDEFAULTS.setValue(self.address.text, forKey: GlobalConstants.Login.addressof)
                GlobalConstants.General.USERDEFAULTS.setValue(self.lastname.text, forKey: GlobalConstants.Login.cslastname)
                GlobalConstants.General.USERDEFAULTS.setValue(self.state.text, forKey: GlobalConstants.Login.csstateee)
                GlobalConstants.General.USERDEFAULTS.setValue(self.country.text, forKey: GlobalConstants.Login.cscountry)
                GlobalConstants.General.USERDEFAULTS.setValue(self.city.text, forKey: GlobalConstants.Login.csstreet)
                self.myzipCode = self.zipcode.text!
                GlobalConstants.General.USERDEFAULTS.setValue(self.firstname.text, forKey: GlobalConstants.Login.csaddressname)
                GlobalConstants.General.USERDEFAULTS.setValue(self.phonenumber.text, forKey: GlobalConstants.Login.csphonenumber)
                let Addr = (responseData?.msg)!
                self.addrId = String(Addr)
            }
        })
    }

    
    
    func zipCodeVerifier(){
        
        let url:String = "http://postalpincode.in/api/pincode/" + myzipCode
        print(url)
        let params = [String:String]()
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<PincodeMapper>().map(JSONString: content!)
            
            if(responseData?.msg == "Success" ) {
               self.showToastMessageTop(message: "Zipcode Verified")
                self.zipcodeVerified = "Yes"
            }
            else {
                self.showToastMessageTop(message: "Zipcode Wrong")
                self.zipcodeVerified = "No"
            }
        })
    }
    
    
    func addressListing(){
        let userId = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken)! as! Int
        let myuser = String(userId)
        let url:String = BASE_URL + "customerB2B/findListAddress?customerId=" + myuser
        print(url)
        let params = [String:String]()
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
        
            let responseData = Mapper<addressListingMapper>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.AddressListingArray = (responseData?.respdata)!
                self.shippingAddressTable.reloadData()
            }
                
            else {
                 self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
        
        
        
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func parserJSON(data: Data){
        var names = [String]()
        do {
            if let ipString = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue) {
                let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as! [AnyObject]
                var data = [cartListMapper]()
                for i in 0..<json.count {
                    let jsonString  = notPrettyString(from: json[i])
                    let responseData = Mapper<cartListMapper>().map(JSONString: jsonString!)
                    cartListing.append(responseData!)
                    print(responseData?.id)
                }
                if cartListing.count == 0 {
                   
                    print("Empty")
                    ordersTableview.reloadData()
                }
                else{
                    ordersTableview.reloadData()
                }
            }
        } catch {
           
            print(error)
        }
    }
    
    func cartList()
    {
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myUId = String(UID)
        let myzippingCoding  = myzipCode
        let url:String = BASE_URL + "customerB2B/cartList?userId=" + myUId + "&userPinCode=" + myzippingCoding
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            self.parserJSON(data: response as! Data)
            KRProgressHUD.dismiss()
            self.prepayment()
            
            self.ordersTableview.reloadData()
        })
    }
    
    func DeleteAddress() {
       
        let url:String = BASE_URL + "customerB2B/deleteAddress?addressID=" + myProdId
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<DeleteCartMapper>().map(JSONString: content!)            
            if(responseData?.sCode == 200) {
               self.showToastMessageTop(message: "Address Deleted Successfully")
            }
            else{
               self.showToastMessageTop(message: "Intrenal server error")
            }
        })
    }
    
    @IBAction func backAction(_ sender: Any) {
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        self.present(vc, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
// TABLEVIEW DELEGATES
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ordersTableview {
         
            let cells = tableView.dequeueReusableCell(withIdentifier:"ConfirmOrdersTableViewCell", for: indexPath) as! ConfirmOrdersTableViewCell
            cells.displayName.text = cartListing[indexPath.row].prod_name
            cells.dispPrice.text = "₹  " + String(Int(Double(cartListing[indexPath.row].total_val!)))
            cells.dispColour.text = cartListing[indexPath.row].prod_color
            cells.dispSize.text = cartListing[indexPath.row].prod_size
            cells.dispQty.text = cartListing[indexPath.row].prod_qty
         
            imgname = cartListing[indexPath.row].prod_image
         
            let  productURL  = img_URL + imgname
            cells.displayImg.setImageWithUrl(imageURl: productURL)
            return cells
        }
        else if tableView == shippingAddressTable {
            let cellAdresdd = tableView.dequeueReusableCell(withIdentifier:"savedAdressTableViewCell", for: indexPath) as! savedAdressTableViewCell
           
            cellAdresdd.fname.text = AddressListingArray[indexPath.row].fname!
            print(AddressListingArray[indexPath.row].fname!)
          
            cellAdresdd.uaddress.text = AddressListingArray[indexPath.row].phone! + ",   " + AddressListingArray[indexPath.row].city! + ",   " + AddressListingArray[indexPath.row].state! + ",   " + AddressListingArray[indexPath.row].country! + ",   " + myzipCode
            return cellAdresdd
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier:"paymentTableViewCell", for: indexPath) as! paymentTableViewCell
        cell.radioButton.image = #imageLiteral(resourceName: "round")
        cell.menuImage.image = UIImage(named:menuImageArray[indexPath.row])
        
        return cell
    }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if tableView == shippingAddressTable {
        if editingStyle == .delete {
    
            myProdId = String(Int(Double(AddressListingArray[indexPath.row].user_address_id!)))
            DeleteAddress()
            AddressListingArray.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
        
        }
        if AddressListingArray.count == 0 {
           shippingAddressTable.isHidden = true
        }
    }
        else {
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == ordersTableview {
        
            return cartListing.count
       
        }
        else if tableView == shippingAddressTable {
            
            return AddressListingArray.count
        
        }
        else {
            
        return menuImageArray.count
    }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == paymentTable {
        let cell = tableView.cellForRow(at: indexPath) as! paymentTableViewCell
      
        paymentGateway = menuImageArray[indexPath.row]
        
        if cell.radioButton.image == #imageLiteral(resourceName: "roundFill") {
           cell.radioButton.image  = #imageLiteral(resourceName: "round")
        }
        else {
            cell.radioButton.image = #imageLiteral(resourceName: "roundFill")
        }
        }
        else if tableView == shippingAddressTable {
           
            let cellAdresdd = tableView.cellForRow(at: indexPath) as! savedAdressTableViewCell
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].state!, forKey: GlobalConstants.Login.csstateee)
          
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].lname!, forKey: GlobalConstants.Login.cslastname)
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].country!, forKey: GlobalConstants.Login.cscountry)
           
            myzipCode = String(describing: AddressListingArray[indexPath.row].zipcode!)
    
            addrId = String(describing: AddressListingArray[indexPath.row].user_address_id!)
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].city!, forKey: GlobalConstants.Login.csstreet)
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].fname!, forKey: GlobalConstants.Login.csaddressname)
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].phone!, forKey: GlobalConstants.Login.csphonenumber)
            
            GlobalConstants.General.USERDEFAULTS.setValue(AddressListingArray[indexPath.row].address1!, forKey: GlobalConstants.Login.addressof)
            
            firstname.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csaddressname) as? String
            
            state.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstateee) as? String
            
            country.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.cscountry) as? String
            
            phonenumber.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csphonenumber) as? String

            
            city.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.csstreet) as? String
            
            lastname.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.cslastname) as? String
            
            address.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.addressof) as? String
            
            self.view.endEditing(true)
            
            shippingAddressTable.isHidden  = true            
            shippingButton.isEnabled = true
            paymentButton.isEnabled = true
            confirmationButton.isEnabled = false
            shippingView.isHidden = true
            paymrntView.isHidden = false
            confirmView.isHidden = true
            paymentline.isHidden = false
            confirmationline.isHidden = true
            shippingLine.isHidden = true
            paymentLabel.textColor = UIColor.myBlueColour
        }
        
        else {
            print("no table selected")
    }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
  
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
   
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(ShippingViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.zipcode.inputAccessoryView = doneToolbar
        self.phonenumber.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
    
        self.view.endEditing(true)
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    let str = "{\"name\":\"James\"}"
    
  
    // MARK :- PAYTM INTEGRATION DELEGATE +++++++++++++   PAYTM DELEGATES    +++++++++++++++++
    
    
    func didCancelTrasaction(_ controller: PGTransactionViewController!) {
    }
    
    func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {
       
        let msg  = responseString
        let dict = convertToDictionary(text: msg!)
        STATUS = dict!["STATUS"]! as! String
       
        if (STATUS == "TXN_FAILURE") {
            STATUS = "Failure"
            PAYMENTMODE = ""
            BANKNAME = ""
            RESPMSG = ""
            GATEWAYNAME = ""

            let tTXNID = dict!["TXNID"]! as! String
            TXNID = Int(tTXNID)
            BANKTXNID = 00
            controller.dismiss(animated: true) {
                self.showAlert(title: "Whoops", message: "Your transaction cancelled !! please try again")
            }
        }
        else {
            PAYMENTMODE = dict!["PAYMENTMODE"]! as! String
            BANKNAME = dict!["BANKNAME"]! as! String
            RESPMSG = dict!["RESPMSG"]! as! String
            pTxnRespCode = dict!["RESPCODE"]! as! String
            GATEWAYNAME = dict!["GATEWAYNAME"]! as! String
            print("Payment mode is : " + PAYMENTMODE)
            print("Payment mode is : " + BANKNAME)
            print("Payment mode is : " + RESPMSG)
            let ttxnamount  = dict!["TXNAMOUNT"]! as! String
            let tTXNID = dict!["TXNID"]! as! String
            let bBANKTXNID = dict!["BANKTXNID"]! as! String

            payDate = dict!["TXNDATE"]! as! String
            txnamount = Int(ttxnamount)
            TXNID = Int(tTXNID)
            BANKTXNID = Int(bBANKTXNID)
            STATUS = "Success"
        }
        postPayment()
        controller.dismiss(animated: true) {
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
            vc.msg = dict!["TXNID"]! as! String
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
   
    
    func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
        print(error)
        
        controller.dismiss(animated: true) {
            self.showAlert(title: "didCancelTrasaction", message: error.localizedDescription)
            let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
    func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        print(error)
        controller.dismiss(animated: true) {
            self.showAlert(title: "didCancelTrasaction", message: error.localizedDescription)
            let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
        
        showAlert(title: "Transaction Cancelled", message: error.localizedDescription)
        controller.dismiss(animated: true) {
            self.showAlert(title: "didCancelTrasaction", message: error.localizedDescription)
            let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
        print("my response isis :" )
        print(response)
        showAlert(title: "cas", message: "")
        
    }
    
   func didSucceedTransaction(controller: PGTransactionViewController, response: [AnyHashable : Any])  {
    print("my response is :" )
    print(response)
    showAlert(title: "Transaction Successfull", message: NSString.localizedStringWithFormat("Response- %@", response) as String)
      let msg: String = "Your order was completed successfully.\n Rs. \(response["TXNAMOUNT"]!)"
    print(msg)
        self.showToastMessageTop(message: "Thank You for Payment" + msg)
    
    controller.dismiss(animated: true) {
    let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
    vc.msg = msg
    self.present(vc, animated: true, completion: nil)
    }
    }
}

// MARK :-   ++++++++++++++++++  TEXTFIELD AND SCROLLING DELEGATES   +++++++++++++++++++++++++

extension ShippingViewController : UITextFieldDelegate {
    /// Scrollview's ContentInset Change on Keyboard hide
    
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    /// register Keyboard
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == firstname {
            _ = lastname.becomeFirstResponder()
        } else if textField == lastname {
             _ = address.becomeFirstResponder()
        }
        else if textField == lastname {
            _ = address.becomeFirstResponder()
        }
        else if textField == address {
            _ = phonenumber.becomeFirstResponder()
        }
        else if textField == phonenumber {
            _ = city.becomeFirstResponder()
        }
        else if textField == city {
            _ = state.becomeFirstResponder()
        }
        else if textField == state {
            _ = country.becomeFirstResponder()
        }
        else if textField == country {
            _ = zipcode.becomeFirstResponder()
        }
        else if textField == zipcode {
            dismissKeyboard()
        }
        
        return true
    }
}
