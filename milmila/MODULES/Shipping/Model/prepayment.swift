//
//  prepayment.swift
//  milmila
//
//  Created by prasun sarkar on 12/18/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class prepaymentMapper: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata:  prepay!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
        
    }
}

class prepay : prepaymentMapper{
    
    var CUST_ID: String?
    var CHANNEL_ID: String?
    var ORDER_ID: String?
    var TXN_AMOUNT: String?
    var CALLBACK_URL: String?
    var MID: String?
    var REQUEST_TYPE: String?
    var CHECKSUMHASH: String?
    var INDUSTRY_TYPE_ID: String?
    var WEBSITE: String?
    var rsaKey:String?
    
    override func mapping(map: Map) {
        
        CUST_ID <- map["CUST_ID"]
        CHANNEL_ID <- map["CHANNEL_ID"]
        ORDER_ID <- map["ORDER_ID"]
        TXN_AMOUNT <- map["TXN_AMOUNT"]
        CALLBACK_URL <- map["CALLBACK_URL"]
        MID <- map["MID"]
        REQUEST_TYPE <- map["REQUEST_TYPE"]
        CHECKSUMHASH <- map["CHECKSUMHASH"]
        INDUSTRY_TYPE_ID <- map["INDUSTRY_TYPE_ID"]
        WEBSITE <- map["WEBSITE"]
        rsaKey <- map["rsaKey"]
    }
}

/*
 
 {
 "msg": "Inventory Blocked Successfully",
 "sCode": 200,
 "data": {
 "CUST_ID": "61",
 "CHANNEL_ID": "WEB",
 "ORDER_ID": "49",
 "TXN_AMOUNT": "0.0",
 "CALLBACK_URL": "http://localhost:8090/mmb2b/payment/paytm.html",
 "MID": "MILMIL31498132006165",
 "REQUEST_TYPE": "DEFAULT",
 "CHECKSUMHASH": "AJCppIWpGAYzAA2tM6Moz7H40Ae0rnvS7RvSVt7PT99sAcVCJH0x+9T8hUZxr1mFCSIR03w4Z2cmvgK2u2qUaB0W87FADPu3FE+SBQf6+CI=",
 "INDUSTRY_TYPE_ID": "Retail",
 "WEBSITE": "WEB_STAGING"
 },
 "error": false,
 "status": true
 }
 
 */
