//
//  PincodeMapper.swift
//  milmila
//
//  Created by prasun sarkar on 3/14/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class PincodeMapper: Mappable {
    
    var error: Bool?
    var msg: String?
    var status: String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        error <- map["error"]
        msg <- map["Message"]
        status <- map["Status"]
        
    }
}
