//
//  addressListModel.swift
//  milmila
//
//  Created by prasun sarkar on 12/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class addressListModel: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata:  AddressDetail!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
        
    }
}

class AddressDetail : addressListModel{
    
    var zipcode: String?
    var country: String?
    var fname: String?
    var city: String?
    var user_address_id: String?
    var state: String?
    var userId: String?
    var phone: String?
    var lname: String?
    
    override func mapping(map: Map) {
        
        zipcode <- map["zipcode"]
        country <- map["country"]
        fname <- map["fname"]
        city <- map["city"]
        user_address_id <- map["user_address_id"]
        state <- map["state"]
        userId <- map["userId"]
        phone <- map["phone"]
        lname <- map["lname"]
}
}


/*
 
 {
 "msg": "List of address ",
 "sCode": 200,
 "data": [
 {
 "zipcode": "5879620",
 "country": "india",
 "fname": "vinay",
 "city": "bangalore",
 "user_address_id": "10",
 "state": "karnataka",
 "userId": "1"
 },
 {
 "zipcode": "5879620",
 "country": "india",
 "fname": "shree",
 "city": "bangalore",
 "user_address_id": "11",
 "state": "karnataka",
 "userId": "1"
 },
 {
 "zipcode": "5879620",
 "country": "india",
 "city": "bangalore",
 "user_address_id": "12",
 "state": "karnataka",
 "userId": "1"
 },
 {
 "zipcode": "5879620",
 "country": "india",
 "fname": "vinay",
 "lname": "kulkarni",
 "city": "bangalore",
 "phone": "9538249382",
 "user_address_id": "13",
 "state": "karnataka",
 "userId": "1"
 }
 ],
 "error": false,
 "status": true
 }
 
 */
