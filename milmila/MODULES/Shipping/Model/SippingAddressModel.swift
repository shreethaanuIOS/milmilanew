//
//  SippingAddressModel.swift
//  milmila
//
//  Created by prasun sarkar on 12/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class SippingAddressModel: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata:  Shippi!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
    
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]

    }
}

class Shippi : SippingAddressModel{



}
