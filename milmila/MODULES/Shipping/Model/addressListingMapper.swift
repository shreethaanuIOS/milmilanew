//
//  addressListingMapper.swift
//  milmila
//
//  Created by prasun sarkar on 12/18/17.
//  Copyright © 2017 Milmila. All rights reserved.
//



import UIKit
import ObjectMapper

class addressListingMapper: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata:  [addresslist]!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
        
    }
}

class addresslist : addressListingMapper{
    
    var zipcode: Int?
    var country: String?
    var fname: String?
    var city: String?
    var user_address_id: Int?
    var state: String?
   // var userId: String?
    var phone: String?
    var lname: String?
    var address1: String?
    override func mapping(map: Map) {
        
        zipcode <- map["zipcode"]
        country <- map["country"]
        fname <- map["fname"]
        city <- map["city"]
        user_address_id <- map["user_address_id"]
        state <- map["state"]
        phone <- map["phone"]
        lname <- map["lname"]
        address1 <- map["address"]
        
    }
}


/*
 {
 "msg": "Successfull",
 "sCode": 200,
 "data": [
 {
 "fname": "u42ruih43ir",
 "lname": "r74r43r",
 "address": "r4r4r4g3ruy43",
 "phone": "4444444444",
 "city": "fyfgyrgf",
 "state": "frygfyrgf",
 "country": "frrfryfg",
 "zipcode": 333333,
 "user_address_id": 65,
 "address2": "r4r4r4g3ruy43"
 },
 {
 "fname": "u42ruih43ir",
 "lname": "hffyvgryf",
 "address": "frfyrgfrgf",
 "phone": "4444444444",
 "city": "fyfgyrgf",
 "state": "frygfyrgf",
 "country": "frrfryfg",
 "zipcode": 333333,
 "user_address_id": 66,
 "address2": "frfyrgfrgf"
 }
 */
