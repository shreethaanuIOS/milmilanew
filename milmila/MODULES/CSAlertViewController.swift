/*
 * CSAlertViewController
 * This class is used for show the nodataview/whoopsview/nointernetview
 */
import UIKit
class CSAlertViewController: UIViewController {
    /// retry button Iboutlet
    @IBOutlet weak var retryButton: UIButton!
    /// show retry button hidde
    var hiddeRetryButton = false
    override func viewDidLoad() {
        super.viewDidLoad()
        if hiddeRetryButton {
            retryButton.isHidden = true
        }
        // Do any additional setup after loading the view.
    }
    
    /// This method is used to retry the api
    ///
    /// - Parameter sender: UIButton
    @IBAction func retryAction(_ sender: Any) {
        if parent is CSParentViewController {
            let parentController: CSParentViewController? = (parent as? CSParentViewController)
            parentController?.removeChildView(controller: self)
            parentController?.callApi()
        }
    }
    /// This method is used to pop the view
    ///
    /// - Parameter sender: UIButton
    @IBAction func backAction(_ sender: Any) {
         if parent is CSParentViewController {
            let parentController: CSParentViewController? = (parent as? CSParentViewController)
            parentController?.removeChildView(controller: self)
            _ = parentController?.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
