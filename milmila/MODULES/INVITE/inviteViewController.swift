//
//  InviteViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/26/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class inviteViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var invitecode: UITextField!
    var uid: Int!
    
    @IBOutlet weak var invitedView: UIView!
    
    var httpRequest = CSSwiftApiHttpRequest()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        invitecode.delegate = self
        uid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
    }
    
    @IBAction func closeInvite(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func invitecodeFunctionality(){
        let url:String = BASE_URL + "user/otpUpdate"
        let myuserid = String(uid)
        let params:[String:String] =
            [
            "user_id":    myuserid,
            "otp": invitecode.text!,
            "emp_device_id": deviceId
            ]
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
           
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<EnquiryMapper>().map(JSONString: content!)
            if(responseData?.sCode == 200) {
               self.showSuccessToastMessage(message: response as! String)
                print(response)
            }
           // let content = String(data: response as! Data, encoding: String.Encoding.utf8)
    
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let nsString = NSString(string: invitecode.text!)
    let newText = nsString.replacingCharacters(in: range, with: string)
    return newText.characters.count <= 6
    }
    
    @IBAction func doneAction(_ sender: Any) {
        if invitecode.text == "" {
        self.showToastMessageTop(message: "Enter the Invite code ")
        }
        else {
        invitecodeFunctionality()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    func presentFAQsHere() {
    
    }
}

