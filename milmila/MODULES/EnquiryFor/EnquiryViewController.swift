//
//  EnquiryViewController.swift
//  milmila
//
//  Created by prasun sarkar on 2/6/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
import Alamofire
import SkyFloatingLabelTextField

class EnquiryViewController: UIViewController {

    @IBOutlet weak var name: SkyFloatingLabelTextField!
    
    @IBOutlet weak var phone: SkyFloatingLabelTextField!
    
    @IBOutlet weak var pincode: SkyFloatingLabelTextField!
    
    @IBOutlet weak var email: SkyFloatingLabelTextField!
    
   var enquire = ""
    var httpRequest = CSSwiftApiHttpRequest()
    override func viewDidLoad() {
        super.viewDidLoad()
addDoneButtonOnKeyboard()
        // Do any additional setup after loading the view.
    }

    func isValidationSuccess() -> Bool {
        
        if (name.text == "") {
            self.showToastMessageTop(message: "Please enter the Name")
            return false
        } else if (phone.text == "") {
            self.showToastMessageTop(message: "Please enter the Mobile number")
            return false
        }
        else if (name.text?.isEmpty)! {
            self.showToastMessageTop(message: "Please enter the Name")
            return false
        }
        else if (trimString(self.phone.text!).count < 10) {
            self.showToastMessageTop(message: "Please enter your valid MobileNo")
            return false
        } else if (pincode.text == "") {
            self.showToastMessageTop(message: "Please enter the address")
            return false
        } else if (trimString(self.pincode.text!).count < 6) {
            self.showToastMessageTop(message: "Please enter your valid pincode")
                return false
        }
        else if (email.text == "") {
            self.view.makeToast("Please enter the email", duration: 3.0, position: .bottom)
            return false
        }
        if !((email?.text?.validateEmail(true))!) {
            self.showToastMessageTop(message:
                NSLocalizedString("Please enter a vaild E-mail id",
                                  comment: "Email"))
            return false
        }
        
        return true
    }
    
    func trimString(_ value: String) -> String {
        return value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    @IBAction func submitAction(_ sender: Any) {
        if isValidationSuccess() {
           EnquirySubmit()
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func EnquirySubmit()
    {
        let url:String = BASE_URL + "enquirey/submit"
        let params:[String:String] = [
            "name":       name.text!,
            "email":    email.text!,
            "phone_no": phone.text!,
            "zip_code": pincode.text!,
            "linkUrl": enquire,
            "jobID": "0"
        ]
        print(params)
      
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<EnquiryMapper>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
              self.showToastMessageTop(message:"Enquiry submitted succesfully")
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                self.present(vc, animated: true, completion: nil)
            }
            else {
            //  self.showToastMessageTop(message:"Please do resubmit")
                self.showToastMessageTop(message: "Internal server error")
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(EnquiryViewController.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.pincode.inputAccessoryView = doneToolbar
        self.email.inputAccessoryView = doneToolbar
        self.phone.inputAccessoryView = doneToolbar
    }
    
    func doneButtonAction() {
        self.pincode.resignFirstResponder()
        self.email.resignFirstResponder()
        self.phone.resignFirstResponder()
        self.name.resignFirstResponder()
}
}

extension EnquiryViewController : UITextFieldDelegate {
    /// Scrollview's ContentInset Change on Keyboard hide
    
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    /// register Keyboard
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldDidBeginEditing(textField: UITextField!)
    {
        pincode = textField as! SkyFloatingLabelTextField!
    }
    
    func textFieldDidEndEditing(textField: UITextField!)
    {
        pincode = nil
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == name {
            _ = phone.becomeFirstResponder()
        } else if textField == phone {
            _ = pincode.becomeFirstResponder()
        }
        else if textField == pincode {
            _ = email.becomeFirstResponder()
        }
        else if textField == email {
            dismissKeyboard()
        }
        
        return true
    }
}
