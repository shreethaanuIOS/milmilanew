import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    

    @IBOutlet weak var swipeImageView: UIImageView!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var homecollectionview: UICollectionView!
    @IBOutlet weak var scrMain: UIScrollView!

    
    let imageNames = ["banner1","banner2","banner3","banner4","TempTextSet"]
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.loadScrollView()
        homecollectionview.dataSource = self
        homecollectionview.backgroundColor = UIColor.white
        self.view.addSubview(homecollectionview)
    }
    
    func loadScrollView() {
        let pageCount : CGFloat = CGFloat(imageNames.count)
        scrMain.backgroundColor = UIColor.clear
        scrMain.delegate = self as? UIScrollViewDelegate
        scrMain.isPagingEnabled = true
        scrMain.contentSize = CGSize(width: scrMain.frame.size.width * pageCount, height: scrMain.frame.size.height)
        scrMain.showsHorizontalScrollIndicator = false
        pageControl.numberOfPages = Int(pageCount)
        pageControl.addTarget(self, action: #selector(self.pageChanged), for: .valueChanged)
        
        for i in 0..<Int(pageCount) {
            print(self.scrMain.frame.size.width)
            let image = UIImageView(frame: CGRect(x:self.scrMain.frame.size.width * CGFloat(i), y:0, width:self.scrMain.frame.size.width, height:self.scrMain.frame.size.height))
            image.image = UIImage(named: imageNames[i])!
            image.contentMode = UIViewContentMode.scaleAspectFit
            self.scrMain.addSubview(image)
        }
    }

    //MARK: UIScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let viewWidth: CGFloat = scrollView.frame.size.width
        // content offset - tells by how much the scroll view has scrolled.
        let pageNumber = floor((scrollView.contentOffset.x - viewWidth / 50) / viewWidth) + 1
        pageControl.currentPage = Int(pageNumber)
    }
    
    //MARK: UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! homeCollectionViewCell

        cell.backgroundColor = UIColor.white
        cell.productimage.image = #imageLiteral(resourceName: "Templogo")
        cell.productname.text = "milmila"
        return cell
    }

    
    //MARK: Page tap action
    func pageChanged() {
        let pageNumber = pageControl.currentPage
        var frame = scrMain.frame
        frame.origin.x = frame.size.width * CGFloat(pageNumber)
        frame.origin.y = 0
        scrMain.scrollRectToVisible(frame, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
