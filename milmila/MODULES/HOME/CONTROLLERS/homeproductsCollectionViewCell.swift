//
//  homeproductsCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/27/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class homeproductsCollectionViewCell: CSBaseCollectionViewCell {
    
    @IBOutlet weak var homeproductsImage: UIImageView!
    @IBOutlet weak var homeProductPrice: UILabel!
    @IBOutlet weak var homeProductName: UILabel!
    
}
