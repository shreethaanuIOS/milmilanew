//
//  TopMenuCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 11/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class TopMenuCollectionViewCell: CSBaseCollectionViewCell {
    @IBOutlet weak var menuName: UILabel!
    
    @IBOutlet weak var menuIcons: UIImageView!
    
    
}
