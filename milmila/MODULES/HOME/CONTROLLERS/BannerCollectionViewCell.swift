//
//  BannerCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 11/23/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
}
