//
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
import ParallaxHeader

class ViewController: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var mymenuCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var myBannerView: UIView!
    @IBOutlet weak var bannerheight: NSLayoutConstraint!
    @IBOutlet weak var userEmail: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var myprofilepic: UIImageView!
    @IBOutlet weak var blurryview: UIView!
    @IBOutlet weak var thewholetrailing: NSLayoutConstraint!
    @IBOutlet weak var thewholeleading: NSLayoutConstraint!
    @IBOutlet weak var thewholeview: UIView!
    @IBOutlet weak var sideview: UIView!
    @IBOutlet weak var sidemenuView: UITableView!
    @IBOutlet weak var TopMenuCollection: UICollectionView!
    @IBOutlet weak var cartCountNumber: UILabel!
    @IBOutlet weak var cartCount: UIView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var cartcount: UILabel!
    @IBOutlet weak var menucollection: UICollectionView!
    @IBOutlet weak var productscollectionView: UICollectionView!
    @IBOutlet weak var searchText: UITextField!
    
    private let leftAndRightPaddings: CGFloat = 15.0
    private let numberOfItemsPerRow: CGFloat = 2.0
    
    let HelpAndSupport = ["Referral Code","Terms and condition","About Us"]
    let MyProfile = ["My Order","My Cart"]

    
    // This is the size of our header sections that we will use later on.
    let SectionHeaderHeight: CGFloat = 25
    var contentOffset: CGFloat = 0
    // Data variable to track our sorted data.

    
    var open = "gghggg"
    
    let CategoryData = ["Top Trending","Featured","Latest arrivals","Summer special"]
    var imageNames = ["banner1","banner2","banner3","banner4","TempTextSet"]
   
    
    /// ACCOUNT DATA
    var accountData: [String]   = ["Recently viewed","Balance","Coupons","Enter invite code","Shipping","Return policy","FAQ","Invite friends","About Us"]
    let accountImages = ["viewed","balance","coupon","invitecode","shipping","policy","faq","invitation","about"]
    
    /// SECTIONs
    let sections: [String] = ["Categories", "Account"]
    let s1Data: [String] = ["Row 1", "Row 2", "Row 3", "Row 3", "Row 3"]
    let s2Data: [String] = ["Recently viewed","Balance","Coupons","Enter invite code","Shipping","Return policy","FAQ","Invite friends","About Us"]
    var sectionData: [Int: [String]] = [:]
  
    /// API
    var httpRequest = CSSwiftApiHttpRequest()
    var homeProductsListingArray = [homeprodList]()
    var homeMenuListingArray = [homeMenuList]()
    var ProductIdForUrl : String!
    var hamburgerMenuIsVisible = false
    var bannerImageArray = [bannerList]()
    let numberOfCellsPerRow: CGFloat = 3
    var passingArray = [categoryList]()
   
    /// TOP MENU
       let CategorysData = ["Shirt","Pant","Tshirt","watch","Pant","Tshirt","watch","Pant","Tshirt","watch"]
       var categoryIdPass:String!
       var rideListArray=[categoryList]()
       var mysubcatCount = ""
       var myIntValue: Int!
       var mystaticContent = ["","",""]
    
    /// PASSING DATA TO DETAILS CONTROLLER
    
      var IMGPASS : String!
      var NAMEPASS : String!
      var DESCRIPTIONPASS : String!
      var PRICEPASS : String!
      var categoryname : String!

    /// Cart count value
    
      var cartCountable: Int!
      let screenSize = UIScreen.main.bounds
    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            if Reachability.isConnectedToNetwork() == true {
                print("Internet connection OK")
                self.showToastMessageTop(message: "Internet connection OK")
                
            } else {
                print("Internet connection FAILED")
//                let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSNoInternet") as! WalkthroughViewController
//                self.present(vc, animated: true, completion: nil)
                self.showToastMessageTop(message: "NO INTERNET")
            }
            
            // Do any additional setup after loading the view, typically from a nib.
            ProductIdForUrl = "4"
            // API CALLS
            HomeProductListing()
            homeMenuListing()
            bannerAPICall()
            RideList()
            sidemenuView.reloadData()
            blurryview.isHidden = true
            cartCount.isHidden = true
            sectionData = [0:s1Data, 1:s2Data]
          
            if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName) == nil)
            {
                userName.text = "Hello User"
                myprofilepic.image = #imageLiteral(resourceName: "accountimage")
            }
            else
            {
                userName.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
                
                userEmail.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
                
                var myImg = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSProfilePicture) as? String
                
                if (myImg == "")
                {
                    myprofilepic.image = #imageLiteral(resourceName: "accountimage")
                }
                else if (myImg == nil){
                    myprofilepic.image = #imageLiteral(resourceName: "accountimage")
                }
                else {
        
                    myprofilepic.setImageWithUrl(imageURl: myImg)                    
                }
            }
            
            // 3 CELLS IN ROW
            
            let bounds = UIScreen.main.bounds
            let width = (bounds.size.width - leftAndRightPaddings*(numberOfItemsPerRow+1)) / numberOfItemsPerRow
            let layout = productscollectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.itemSize = CGSize(width: width, height: 190)
            
            if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
                let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.present(vc, animated: true, completion: nil)
            }
            else {
                cartcounting()
                cartCount.isHidden = false
            }
            }
    
        /// ButtonAction :-
        @IBAction func profileAction(_ sender: Any) {
            
            if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName) == nil){
                
            }
            else
            {
                
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.present(vc, animated: true, completion: nil)
            }
        }
    
    @IBAction func referralCode(_ sender: Any) {
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
            
            let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
            print ("Please Log In ")
        }
        else {
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "inviteViewController") as! inviteViewController
        self.present(vc, animated: true, completion: nil)
        }
    }
    
    
        @IBAction func searchAct(_ sender: Any) {
            
        }
        
        @IBAction func OpencartAction(_ sender: Any) {
            
            if(GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil )
            {
                let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                self.present(vc, animated: true, completion: nil)
            }
            else
            {
                
                let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
                self.present(vc, animated: true, completion: nil)
            }
        }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        if contentOffset > scrollView.contentOffset.y {
           print("upppppppppppppp")
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                self.bannerheight.constant = 150
                self.mymenuCollectionHeight.constant = 30
            }, completion: { (finished: Bool) in
                self.bannerheight.constant = 150
            })
            
           
        } else if contentOffset < scrollView.contentOffset.y {
          print("downnnuuuuuuuuuuuu")
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.bannerheight.constant = 0
                self.mymenuCollectionHeight.constant = 30
            }, completion: { (finished: Bool) in
               self.bannerheight.constant = 0
            })
        }
        contentOffset = scrollView.contentOffset.y
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        
        if !hamburgerMenuIsVisible {
            blurryview.isHidden = false
            thewholeleading.constant = 250
            hamburgerMenuIsVisible = true
            RideList()
        }
        else
        {
            thewholeleading.constant = 0
            thewholetrailing.constant = 0
            blurryview.isHidden = true
            hamburgerMenuIsVisible = false
        }
    }
    
        @IBAction func openSideMenu(_ sender: Any) {
            
            if !hamburgerMenuIsVisible {
                blurryview.isHidden = false
                thewholeleading.constant = 250
                hamburgerMenuIsVisible = true

            }
            else
            {
              
                thewholeleading.constant = 0
                thewholetrailing.constant = 0
                blurryview.isHidden = true
                hamburgerMenuIsVisible = false

            }
    }
        /// TAP GESTURE
        
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        /// MARK:- API CALL
        
        // Products Listing
        func HomeProductListing() {
            
            let url:String = BASE_URL + "group/product/list.html?g=" + ProductIdForUrl
            let params = [String : String]()
            print(url)
            httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
                print(content ?? String())
             
                let response = Mapper<HomeProductsMapper>().map(JSONString: content!)
                
                if (response?.mappie.sCode == 200) {
                    print("+++++++++ success ++++++++++")
                    self.homeProductsListingArray = (response?.mappie.respdata.prodObj)!
                    self.productscollectionView.reloadData()
                }else{
                    self.showToastMessageTop(message: "Internal server error")
                    print("+++++++++ vada pocheaaa  ++++++++++")
                }
            })
        }
        
        // Bottom menu API
        func homeMenuListing(){
            
            let url:String = BASE_URL + "page/group/findList.html?channelType=B2B"
            let params = [String : String]()
            
            httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
                print(content ?? String())
                /*
                 * call the the object mapper
                 */
                
                let response = Mapper<HomeMenuMapper>().map(JSONString: content!)
                
                if (response?.sCode == 200) {
                    print("+++++++++ success ++++++++++")
                    self.homeMenuListingArray = (response?.respdata)!
                    self.menucollection.reloadData()
                }
                else {
                    self.showToastMessageTop(message: "Internal server error")
                    print("+++++++++ vada pocheaaa  ++++++++++")
                }
            })
        }
    
        // Banner API
        func bannerAPICall(){
            
            let url:String = BASE_URL + "page/banner/findList.html"
            let params = [String : String]()
            
            httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
                
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
                print(content ?? String())
                /*
                 * call the the object mapper
                 */
                
                let response = Mapper<BannerMapper>().map(JSONString: content!)
                
                if (response?.mapi.sCode == 200) {
                    print("+++++++++ success ++++++++++")
                    self.bannerImageArray = (response?.mapi.respdata)!
                    self.bannerCollectionView.reloadData()
                }else{
                    self.showToastMessageTop(message: "Internal server error")
                    print("+++++++++ vada pocheaaa  ++++++++++")
                }
            })
        }
        
    
        // API for category list
        func RideList() {
            
            let url:String = BASE_URL + "product/categories/all-categories.html"
            let params = [String : String]()
            print(url)
            httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
                print(content ?? String())
                /*
                 * call the the object mapper
                 */
                
                let response = Mapper<categorylistModel>().map(JSONString: content!)
                
                if (response?.sCode == 200) {
                    print("+++++++++ success ++++++++++")
                    self.rideListArray = (response?.respdata)!
                    print(self.rideListArray)
                    self.TopMenuCollection.reloadData()
                    self.sidemenuView.reloadData()
                }else{
                    self.showToastMessageTop(message: "Internal server error")
                    print("+++++++++ vada pocheaaa  ++++++++++")
                }
            })
        }
        
        //  API for CART COUNT
        
        func cartcounting(){
            
            let uuid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
            let myUserId =  String(uuid)
            let url:String = BASE_URL + "customerB2B/cartCount?userId=" + myUserId
            let params = [String : String]()
            
            httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
               
                print(content ?? String())
    
                let response = Mapper<cartCountMappper>().map(JSONString: content!)
                
                if (response?.sCode == 200) {
                    self.cartCountable = (response?.size)!
                    self.cartcount.text = String(self.cartCountable)
                    print("+++++++++ success ++++++++++")
                    
                }else{
                    self.showToastMessageTop(message: "Internal server error")
                    print("+++++++++ vada pocheaaa  ++++++++++")
                }
            })
        }
        
        
        /// MARK :- TableviewDelegate
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            //return (sectionData[section]?.count)!
            return rideListArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cells = tableView.dequeueReusableCell(withIdentifier:"myAccountHeaderTableViewCell", for: indexPath) as! myAccountHeaderTableViewCell
            cells.accountame.text = rideListArray[indexPath.row].cateName
            let catecod = rideListArray[indexPath.row].cateCode
            let cateImg = rideListArray[indexPath.row].cateCode
            cells.accountImgae.image = UIImage(named: cateImg!)
            return cells
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           
            if(rideListArray[indexPath.row].subcate is NSNull){
                mysubcatCount = "NA"
                
            }
            else if (rideListArray[indexPath.row].subcate == nil){
                mysubcatCount = "NA"
            }
            else {
                
                mysubcatCount = String(rideListArray[indexPath.row].subcate.count)
            }
            
            if (rideListArray[indexPath.row].subcate is NSNull) {
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                
                categoryIdPass = rideListArray[indexPath.row].cateId
                categoryname = rideListArray[indexPath.row].cateName
                print(categoryIdPass)
                vc.categoryId = categoryIdPass
                vc.categoryName = categoryname
                self.present(vc, animated: true, completion: nil)
            }
            else if (rideListArray[indexPath.row].subcate == nil){
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                //  vc.catSubCatName = rideListArray[indexPath.row].subcate
                categoryIdPass = rideListArray[indexPath.row].cateId
                categoryname = rideListArray[indexPath.row].cateName
                print(categoryIdPass)
                vc.categoryId = categoryIdPass
                vc.categoryName = categoryname
                self.present(vc, animated: true, completion: nil)
            }
            else if (mysubcatCount == ""){
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                //  vc.catSubCatName = rideListArray[indexPath.row].subcate
                categoryIdPass = rideListArray[indexPath.row].cateId
                categoryname = rideListArray[indexPath.row].cateName
                print(categoryIdPass)
                vc.categoryId = categoryIdPass
                vc.categoryName = categoryname
                self.present(vc, animated: true, completion: nil)
            }
            else if rideListArray[indexPath.row].subcate.count >= 1  {
                
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                vc.catSubCatName = rideListArray[indexPath.row].subcate
                categoryIdPass = rideListArray[indexPath.row].cateId
                categoryname = rideListArray[indexPath.row].cateName
                print(categoryIdPass)
                vc.categoryId = categoryIdPass
                vc.categoryName = categoryname
                self.present(vc, animated: true, completion: nil)
            }
            else {
                
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productsdisplayviewcontroller") as! productsDisplayViewController
                categoryIdPass = rideListArray[indexPath.row].cateId
                categoryname = rideListArray[indexPath.row].cateName
                vc.categoryId = categoryIdPass
                vc.categoryName = categoryname
                self.present(vc, animated: true, completion: nil)
                
            }
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
        {
            return 80;
        }
    
        
        func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            
        }
        
        /// MARK:- COLLECTIONVIEW DEELEGATE
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            
            if collectionView == productscollectionView {
                return homeProductsListingArray.count
            }
            else if collectionView == bannerCollectionView {
                return bannerImageArray.count
            }
            else if collectionView == TopMenuCollection {
                return rideListArray.count
            }
            else{
                return homeMenuListingArray.count
            }
            
        }
        
        func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> SectionHeaderCollectionReusableView
        {
            if collectionView == productscollectionView {
                
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeader", for: indexPath as IndexPath) as! SectionHeaderCollectionReusableView
                header.headerText.text = "my value"
                return header
            }
            else {
                let headers = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "SectionHeader", for: indexPath as IndexPath) as! SectionHeaderCollectionReusableView
                headers.headerText.text = "my value"
                return headers
            }
        }
        
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       
        if collectionView == productscollectionView {
            let cell:homeproductsCollectionViewCell = cell as! homeproductsCollectionViewCell
            cell.collectionViewDisplayAnimation()
        }
         else {
        }
    }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            if collectionView == productscollectionView{
                
                let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "homeproductsCollectionViewCell", for: indexPath) as! homeproductsCollectionViewCell
                let productsimageURL = homeProductsListingArray[indexPath.row].imgURL
                let  productURL  = img_URL + productsimageURL!
                print(productURL)
                cells.homeproductsImage.setImageWithUrl(imageURl: productURL)
                cells.homeProductName.text = homeProductsListingArray[indexPath.row].pname
                scrollViewDidScroll(scrollView: productscollectionView)
                
                if (homeProductsListingArray[indexPath.row].price is NSNull ) {
                    
                    cells.homeProductPrice.text =  "₹ " + "price"
                }
                    
                else if (homeProductsListingArray[indexPath.row].price == nil){
                    
                    cells.homeProductPrice.text =  "₹ " + "price"
                }
                    
                else {
                    
    cells.homeProductPrice.text =  "₹ " + "\(homeProductsListingArray[indexPath.row].minPrice!)" + "   -  " + "₹ " + "\(homeProductsListingArray[indexPath.row].price!)"
                    
                }
                return cells
            }
            else if collectionView == bannerCollectionView{
                let cellb = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionViewCell", for: indexPath) as! BannerCollectionViewCell
                let BannerImageUrl = bannerImageArray[indexPath.row].bannerMobURL
                let  BannerProductURL  = img_URL + BannerImageUrl!
                print(BannerProductURL)
                cellb.bannerImage.setImageWithUrl(imageURl: BannerProductURL)
                return cellb
            }
            else if collectionView == TopMenuCollection {
                let cellt = collectionView.dequeueReusableCell(withReuseIdentifier: "TopMenuCollectionViewCell", for: indexPath) as! TopMenuCollectionViewCell
                cellt.menuName.text = rideListArray[indexPath.row].cateName
                let cateImg = rideListArray[indexPath.row].cateCode
                cellt.menuIcons.image = UIImage(named: cateImg!)
                return cellt
            }
                
            else{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCollectionViewCell", for: indexPath) as! homeCollectionViewCell
                cell.labelname.text = homeMenuListingArray[indexPath.row].value
                return cell
            }
        }
        
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            if collectionView == menucollection {
                let acell = collectionView.cellForItem(at: indexPath) as! homeCollectionViewCell
               acell.labelname.textColor = UIColor.myBlueColour
                if( acell.labelname.textColor == UIColor.myBlueColour){
                   acell.labelname.textColor = UIColor.black
                } else {
                    acell.labelname.textColor = UIColor.myBlueColour
                }
                ProductIdForUrl = "\(homeMenuListingArray[indexPath.row].id!)"
                HomeProductListing()
            }
            else if collectionView == bannerCollectionView {
            }
            else if collectionView == TopMenuCollection {
                if(rideListArray[indexPath.row].subcate is NSNull){
                    mysubcatCount = "NA"
                }
                else if (rideListArray[indexPath.row].subcate == nil){
                    mysubcatCount = "NA"
                }
                else {
                    mysubcatCount = String(rideListArray[indexPath.row].subcate.count)
                }
                if (rideListArray[indexPath.row].subcate is NSNull) {
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                    //  vc.catSubCatName = rideListArray[indexPath.row].subcate
                    categoryIdPass = rideListArray[indexPath.row].cateId
                    categoryname = rideListArray[indexPath.row].cateName
                    print(categoryIdPass)
                    vc.categoryId = categoryIdPass
                    vc.categoryName = categoryname
                    self.present(vc, animated: true, completion: nil)
                }
                else if (rideListArray[indexPath.row].subcate == nil){
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                    //  vc.catSubCatName = rideListArray[indexPath.row].subcate
                    categoryIdPass = rideListArray[indexPath.row].cateId
                    categoryname = rideListArray[indexPath.row].cateName
                    print(categoryIdPass)
                    vc.categoryId = categoryIdPass
                    vc.categoryName = categoryname
                    vc.fromtype = "home"
                    self.present(vc, animated: true, completion: nil)
                }
                else if (mysubcatCount == ""){
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                    //  vc.catSubCatName = rideListArray[indexPath.row].subcate
                    categoryIdPass = rideListArray[indexPath.row].cateId
                    categoryname = rideListArray[indexPath.row].cateName
                    print(categoryIdPass)
                    vc.categoryId = categoryIdPass
                    vc.categoryName = categoryname
                    vc.fromtype = "home"
                    self.present(vc, animated: true, completion: nil)
                }
                else if rideListArray[indexPath.row].subcate.count >= 1  {
                    
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
                    vc.catSubCatName = rideListArray[indexPath.row].subcate
                    categoryIdPass = rideListArray[indexPath.row].cateId
                    categoryname = rideListArray[indexPath.row].cateName
                    print(categoryIdPass)
                    vc.categoryId = categoryIdPass
                    vc.categoryName = categoryname
                    vc.fromtype = "home"
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productsdisplayviewcontroller") as! productsDisplayViewController
                    categoryIdPass = rideListArray[indexPath.row].cateId
                    categoryname = rideListArray[indexPath.row].cateName
                    vc.categoryId = categoryIdPass
                    vc.categoryName = categoryname
                    vc.fromtype = "home"
                    self.present(vc, animated: true, completion: nil)
                    
                }
            }
                
            else{
                IMGPASS = homeProductsListingArray[indexPath.row].imgURL
                NAMEPASS = homeProductsListingArray[indexPath.row].pname
                PRICEPASS = "\(homeProductsListingArray[indexPath.row].price!)"
                DESCRIPTIONPASS = "\(homeProductsListingArray[indexPath.row].pid!)"
               
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailProductDisplayViewController") as! DetailProductDisplayViewController
                vc.IMGPASSto = IMGPASS
                vc.NAMEPASSto = NAMEPASS
                vc.PRICEPASSto = PRICEPASS
                vc.PRODID = DESCRIPTIONPASS
                vc.fromtype = "home"
                self.present(vc, animated: true, completion: nil)
            }
        }
}


