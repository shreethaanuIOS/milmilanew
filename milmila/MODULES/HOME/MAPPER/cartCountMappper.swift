//
//  cartCountMappper.swift
//  milmila
//
//  Created by prasun sarkar on 11/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class cartCountMappper: Mappable {
    
    var sCode: Int?
    var error: String?
    var msg: String?
    var size: Int!
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        size <- map["data"]
    }
}

/*
 {
 "msg": "cart Information ",
 "sCode": 200,
 "error": false,
 "status": true
 }
 */
