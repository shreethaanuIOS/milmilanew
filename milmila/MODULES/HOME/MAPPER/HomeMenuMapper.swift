//
//  HomeMenuMapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/8/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeMenuMapper: Mappable {
    
    var sCode: Int?
    var error: String?
    var msg: String?
    var respdata:  [homeMenuList]!
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        sCode <- map["scode"]
        error <- map["error"]
        msg <- map["msg"]
        respdata <- map["data"]
    }
}
class homeMenuList : HomeMenuMapper{
    
    var limit: String!
    var id: Int!
    var value:String!
    
    override func mapping( map: Map) {
        // direct conversion
        limit    <- map["limit"]
        id     <- map["id"]
        value   <- map["value"]
    }
}

/*
 
 {
 "msg": "Successfull",
 "data": [
 {
 "limit": 10,
 "bannerURL": "grp-banners/milmila-selection.jpg",
 "id": 4,
 "value": "Selection"
 },
 {
 "limit": 10,
 "bannerURL": "grp-banners/lADPAuoR5Kr_FnpkzQSw_1200_100.jpg",
 "id": 18,
 "value": "Trendy Kids"
 },
 {
 "limit": 15,
 "bannerURL": "grp-banners/hotsales-milmilas.jpg",
 "id": 5,
 "value": "Hot Sale"
 },
 {
 "limit": 10,
 "bannerURL": "grp-banners/l2.png",
 "id": 3,
 "value": "Lingeries (in stock)"
 }
 ],
 "error": null,
 "status": true,
 "additionalProperties": {},
 "scode": 200
 }
 
 */
