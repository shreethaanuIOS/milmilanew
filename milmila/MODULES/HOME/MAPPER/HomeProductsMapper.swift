//
//  Home productsMapper.swift
//  milmila

//  Created by prasun sarkar on 11/8/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class HomeProductsMapper: Mappable {
    var mappie : Mapzi!
    
    required init?(map: Map) {
    }
     func mapping(map: Map) {
       mappie <- map["map"]
    }
}

class Mapzi: HomeProductsMapper {
    var sCode: Int?
    var error: String?
    var msg: String?
    var respdata: prodObjects!
    
    override func mapping(map: Map) {
        sCode <- map["scode"]
        error <- map["error"]
        msg <- map["msg"]
        respdata <- map["data"]
    }
}

class prodObjects: Mapzi {
   var prodObj : [homeprodList]!

    override func mapping(map: Map) {
        prodObj <- map["prodObj"]
}
}

class homeprodList : prodObjects{
    
    var imgURL: String!
    var pname: String!
    var price:Int?
    var qty:String!
    var pid:Int?
    var model:String!
    var sku : String!
    var minPrice :  Int?
    override func mapping( map: Map) {
        // direct conversion
        imgURL    <- map["imgURL"]
        pname     <- map["pname"]
        price   <- map["price"]
        minPrice <- map["minPrice"]
        qty   <- map["qty"]
        pid      <- map["pid"]
        model     <- map["model"]
        sku   <- map["sku"]
    }
}
