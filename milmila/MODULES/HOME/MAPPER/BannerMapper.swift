//
//  BannerMapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class BannerMapper: Mappable {
    var mapi : mapzi!
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        mapi <- map["map"]
    }
}

class mapzi : BannerMapper {
  
    var sCode: Int?
    var error: String?
    var msg: String?
    var respdata:  [bannerList]!
    
    override func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        respdata <- map["data"]
    }
}
class bannerList : mapzi{
    
    var showFrom: String!
    var webLinkInfo: String!
    var bannerType:String!
    var bannerId:String!
    var mobLinkInfo:String!
    var bannerWebURL:String!
    var showTillFormat : String!
    var bannerName:String!
    var isActive:String!
    var bannerMobURL:String!
    var showFromFormat : String!
    var bannerTitle:String!
    var sortOrder:String!
    var showTill:String!
    
    override func mapping( map: Map) {
        // direct conversion
        showFrom         <- map["showFrom"]
        webLinkInfo     <- map["webLinkInfo"]
        bannerType       <- map["bannerType"]
        bannerId         <- map["bannerId"]
        mobLinkInfo      <- map["mobLinkInfo"]
        bannerWebURL     <- map["bannerWebURL"]
        showTillFormat   <- map["showTillFormat"]
        bannerName       <- map["bannerName"]
        isActive          <- map["isActive"]
        bannerMobURL      <- map["bannerMobURL"]
        showFromFormat    <- map["showFromFormat"]
        bannerTitle          <- map["bannerTitle"]
        isActive          <- map["isActive"]
        sortOrder          <- map["sortOrder"]
        showTill          <- map["showTill"]
    }
}

/* 
 {
 "msg": "Found 2 Banner(s).",
 "sCode": 200,
 "error": false,
 "status": true
 "data": [
 {
 "showFrom": "2017-11-01 00:00:00.0",
 "webLinkInfo": "",
 "bannerType": "BOTH",
 "bannerId": "1",
 "mobLinkInfo": "",
 "bannerWebURL": "banners/november-offer-sale-for-kids-and-accessories-1140x380.jpg",
 "showTillFormat": "2017-12-31T00:00:00",
 "bannerName": "New November Sale",
 "isActive": "A",
 "bannerMobURL": "banners/november-offer-sale-for-kids-and-accessories-1140x380.jpg",
 "showFromFormat": "2017-11-01T00:00:00",
 "bannerTitle": "",
 "sortOrder": "1",
 "showTill": "2017-12-31 00:00:00.0"
 },
 {
 "showFrom": "2017-11-07 00:00:00.0",
 "webLinkInfo": "",
 "bannerType": "WEB",
 "bannerId": "2",
 "mobLinkInfo": "Link Details",
 "bannerWebURL": "banners/bags and shoes - banner-1140x380 (1).jpg",
 "showTillFormat": "2017-12-31T00:00:00",
 "bannerName": "Bags & Shoes",
 "isActive": "A",
 "bannerMobURL": "banners/",
 "showFromFormat": "2017-11-07T00:00:00",
 "bannerTitle": "",
 "sortOrder": "2",
 "showTill": "2017-12-31 00:00:00.0"
 }
 ]
 }
 
 */
