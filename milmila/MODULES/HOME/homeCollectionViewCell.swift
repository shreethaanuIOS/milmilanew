//
//  homeCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class homeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var productimage: UIImageView!
}
