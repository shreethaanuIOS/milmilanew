//
//  loginModel.swift
//  milmila
//
//  Created by prasun sarkar on 11/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class loginModel: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var  respdata:  login!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
    }
}


class login : loginModel{
    
    var user_id: Int?
    var first_name: String!
    var last_name:String!
    var login_name:String!
    var addr1:String!
    var addr2:String!
    var addr3 : String!
    var city_id:String!
    var city_name:String!
    var state_id:String!
    var state_name:String!
    var zip_code : String!
    var mobile:String!
    var dob:String!
    var gender:String!
    var profile_image : String!
    var is_active:String!
    var is_verified:String!
    var verified_date:String!
    var create_date:String!
    var create_user:String!
    var pwd:String!
    var baseData: baseData!
    
    override func mapping( map: Map) {
        // direct conversion
        user_id        <- map["customerId"]
        first_name     <- map["firstName"]
        last_name      <- map["lastName"]
        login_name     <- map["loginName"]
        pwd            <- map["pwd"]
        addr1          <- map["addr1"]
        addr2          <- map["addr2"]
        addr3          <- map["addr3"]
        city_id        <- map["city_id"]
        city_name      <- map["city_name"]
        state_id       <- map["state_id"]
        state_name     <- map["state_name"]
        zip_code       <- map["zip_code"]
        mobile         <- map["mobile"]
        dob            <- map["dateOfBirth"]
        gender         <- map["gender"]
        profile_image  <- map["profileImage"]
        is_active      <- map["isActive"]
        is_verified    <- map["isVerified"]
        verified_date  <- map["verifiedDate"]
        create_date    <- map["createDate"]
        create_user    <- map["createUser"]
    }
}

class baseData : login {
    
}

/*
 
 {
 "error": false,
 "sCode": 200,
 "msg": "Succesfully Logged-In",
 "status": true,
 
 "data": {
 "user_id": "26",
 "first_name": "Shreethaanu",
 "last_name": "Shreethaanu",
 "login_name": "shree96thaanu@gmail.com",
 "addr1": null,
 "addr2": null,
 "addr3": null,
 "city_id": null,
 "city_name": null,
 "state_id": null,
 "state_name": null,
 "zip_code": null,
 "mobile": "1234567890",
 "dob": null,
 "gender": null,
 "profile_image": null,
 "is_active": "Y",
 "is_verified": "Y",
 "verified_date": null,
 "create_date": 1510726635000,
 "create_user": "MM_API"
 }
 }
 
 */

