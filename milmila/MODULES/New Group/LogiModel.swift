/*
 * CSLoginModel
 * This login class is login model (object mapper class for login response)
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */
import UIKit
import ObjectMapper
class CSLoginModel: Mappable {
    var error: Bool?
    var statusCode = Int()
    var message = String()
    var response: CSResponse?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        error <- map["error"]
        statusCode <- map["statuscode"]
        response <- map["response"]
        message <- map["message"]
    }
}
class CSResponse: Mappable {
    var token = String()
    var data: CSData?
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        token <- map["token"]
        data <- map["customer"]
    }
}
class CSData: Mappable {
    var userId = Int()
    var name = String()
    var lastName = String()
    var email = String()
    var website = String()
    var birthDay = String()
    var phoneNumber = String()
    var stateName = String()
    var stateId = Int()
    var cityId = Int()
    var cityName = String()
    var countryName = String()
    var countryId = Int()
    var description = String()
    var birthPlace = String()
    var occupation = String()
    var gender = String()
    var spotifyUrl = String()
    var twitterUrl = String()
    var dribblesUrl = String()
    var facebookUrl = String()
    var rssfeedUrl = String()
    var status = String()
    var acessType = String()
    var profileImage =  String()
    var stripeId = String()
    var isPassword = Int()
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        userId <- map["id"]
        name <- map["name"]
        lastName <- map["last_name"]
        email <- map["email"]
        website <- map["website"]
        birthDay <- map["birthday"]
        phoneNumber <- map["mobile_number"]
        stateName <- map["state_name"]
        stateId <- map["state_id"]
        cityName <- map["city_name"]
        cityId <- map["city_id"]
        description <- map["description"]
        countryName <- map["country_name"]
        countryId <- map["country_id"]
        birthPlace <- map["birthplace"]
        occupation <- map["occupation"]
        gender <- map["gender"]
        status <- map["status"]
        dribblesUrl <- map["dribble_url"]
        spotifyUrl <- map["spotify_url"]
        twitterUrl <- map["twitter_url"]
        facebookUrl <- map["facebook_url"]
        rssfeedUrl <- map["rssfeed_url"]
        acessType <- map["access_type"]
        profileImage <- map["profile_image"]
        // favouriteVideos <- map["favourite_video"]
        stripeId <- map["stripe_id"]
        isPassword <- map["is_password"]
    }
}

