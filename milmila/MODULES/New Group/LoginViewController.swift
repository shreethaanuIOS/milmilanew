//
//  LoginViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/26/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
import Google
import GoogleSignIn
import KRProgressHUD
import FacebookLogin
import FBSDKLoginKit
class LoginViewController: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {

    @IBOutlet weak var trailing: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var fbButton: UIButton!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var signIn: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var loginScrollView: UIScrollView!
    @IBOutlet weak var twitter: UIButton!
    @IBOutlet var gestureRecogizer: UITapGestureRecognizer!
    @IBOutlet weak var googleButtonSignin: GIDSignInButton!
    
    var httpRequest = CSSwiftApiHttpRequest()
    var nam = ""
    var imga = ""
    var profimgString : String!
    weak var delegate: loginSuccess?
     var dict : [String : AnyObject]!
    
    // the url = file:///Users/Me/Desktop/Doc.txt
    
    enum AuthProvider: String {
        case facebook, twitter, google, normal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup afer loading the view.
        
        /// GOOGLE SIGNIN
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        }



    //function is fetching the user data
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.dict = result as! [String : AnyObject]
                    print(result!)
                    print(self.dict)
                    
                    let usersFBId = self.dict["id"] as? String
                    let usersFBName = self.dict["name"] as? String
                    let usersFBEmail = self.dict["email"]as? String
                    let fbimageURL = ((self.dict["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                  
                    GlobalConstants.General.USERDEFAULTS.setValue(fbimageURL, forKey: GlobalConstants.Login.CSProfilePicture)
                    GlobalConstants.General.USERDEFAULTS.setValue(usersFBName, forKey: GlobalConstants.Login.CSUserName)
                    GlobalConstants.General.USERDEFAULTS.setValue(usersFBEmail, forKey: GlobalConstants.Login.CSEmail)
                    CSSharedData.SharedInstance.isLoggedIn = true
                    self.fbLogins()
                }
            })
        }
    }
   
    @IBAction func googleSignInButtonAction(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func fbAction(_ sender: Any) {

        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
        loginManager.logIn(withReadPermissions: ["email"], from: self, handler: { (result, error) in
            if let error = error {
                self.showToastMessageTop(message: error.localizedDescription)
            } else if result!.isCancelled {
    
            } else {
                self.getFBUserData()
            }
        })
    }


    @IBAction func closeAction(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func createAction(_ sender: Any) {
    }
    
    @IBAction func SignIn(_ sender: Any) {
       login()
        
    }
    
    @IBAction func gestureAction(_ sender: Any) {
    }
    
    func hotlineIntegration(){
        
    }
    
    /// FB SIGNIN
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email"]).start { (connection, result, err) in
            
            if err != nil {
                print("Failed to start graph request:", err)
                return
            }
            print(result)
        }
    }
    
    /// GOOGLE SIGNIN
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        guard error == nil else {
            
            print("Error while trying to redirect : \(error)")
            return
        }
        
        print("Successful Redirection")
    }
    
    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    public func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
                       withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            var userId = user.userID                  // For client-side use only!
            var idToken = user.authentication.idToken // Safe to send to the server
            var fullName = user.profile.name
            var givenName = user.profile.givenName
            var familyName = user.profile.familyName
            var MyGoogleemail = user.profile.email
            var myprofImg = user.profile.imageURL(withDimension: 100)
            var urlString = myprofImg?.absoluteString


            print(myprofImg!)
            GlobalConstants.General.USERDEFAULTS.setValue(fullName, forKey: GlobalConstants.Login.CSUserName)
            GlobalConstants.General.USERDEFAULTS.setValue(urlString, forKey: GlobalConstants.Login.CSProfilePicture)
            GlobalConstants.General.USERDEFAULTS.setValue(MyGoogleemail, forKey: GlobalConstants.Login.CSEmail)
         
            CSSharedData.SharedInstance.isLoggedIn = true

            googleLogin()
            
           
          
           
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    // GOOGLE LOGIN
    
    
    func googleLogin(){
        let url:String = BASE_URL + "customerB2B/signIn"
        let Mygoogleemail = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
        let params:[String:String] = [
            "loginName":      Mygoogleemail!,
            "deviceId":    deviceId,
            "signedFrm": SIGNEDFROM,
            "pwd":         "",
            "social_sign_up": "Y"
        ]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<loginModel>().map(JSONString: content!)
            print(responseData?.respdata)
            if(responseData?.sCode == 200)
            {
                self.notificationMessageTop(message: "LOGGED in successfully")
                
                 let usersId = responseData?.respdata.user_id
                
                GlobalConstants.General.USERDEFAULTS.setValue(usersId, forKey: GlobalConstants.Login.CSUserToken)
                print(responseData?.respdata.user_id)
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                self.present(vc, animated: true, completion: nil)
            }
             else
            {
                let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
                self.present(vc, animated: true, completion: nil)
            }
        })
    }
        
    // FB LOGIN
        
        
        func fbLogins(){
           
            let url:String = BASE_URL + "customerB2B/signIn"

            let myFbEmail = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
           
            let myFbName = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
            
            let params:[String:String] = [
                "loginName":  myFbEmail!,
                "fname": myFbName!,
                "deviceId":    deviceId,
                "signedFrm": SIGNEDFROM,
                "pwd":         "",
                "social_sign_up": "Y"
            ]
           
            print(params)
          
            httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
                
                let content = String(data: response as! Data, encoding: String.Encoding.utf8)
                
                let responseData = Mapper<loginModel>().map(JSONString: content!)
              
                if(responseData?.sCode == 200) {
    
                    let usersId = responseData?.respdata.user_id
                    GlobalConstants.General.USERDEFAULTS.setValue(usersId, forKey: GlobalConstants.Login.CSUserToken)
                    
                    let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    self.showToastMessageTop(message: "Internal server error")
                }
            })
        
    }
    
    
    /// MANUAL LOGIN
    func login()
    {
        let url:String = BASE_URL + "customerB2B/signIn"
        //
        //        let categoryId:Int! = GlobalConstants.General.USERDEFAULTS.value(forKey: GlobalConstants.Login.CSTeamId) as! Int
        
        let params:[String:String] = [
            "loginName":       email.text!,
            "deviceId":    deviceId,
            "signedFrm": SIGNEDFROM,
            "pwd":         password.text!,
            "social_sign_up": "N"
        ]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<loginModel>().map(JSONString: content!)
            
            if(responseData?.sCode == 200)
            {
                self.notificationMessageTop(message: "LOGGED in successfully")
                
                let usernamea = responseData?.respdata.first_name
                GlobalConstants.General.USERDEFAULTS.setValue(usernamea, forKey: GlobalConstants.Login.CSUserName)
                
                let emaila = responseData?.respdata.login_name
                 GlobalConstants.General.USERDEFAULTS.setValue(emaila, forKey: GlobalConstants.Login.CSEmail)
                
              //  print(GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)!)
            
                let userid = responseData?.respdata.user_id
                
                //String(describing: responseData?.respdata.user_id)
                
                GlobalConstants.General.USERDEFAULTS.setValue(userid, forKey: GlobalConstants.Login.CSUserToken)
                
                CSSharedData.SharedInstance.isLoggedIn = true
               
                // self.hotlineIntegration()u
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                self.present(vc, animated: true, completion: nil)
            }
            else {
                
                
            }
        })
    }
    
    // MARK: - Api Call
        /// Social Login Parameter
        ///
        /// - Parameters:
        ///   - email: valid email address
        ///   - providerName: name of the API provider
        ///   - name: username
        ///   - profilePicture: image of the user
        ///   - tokenSocial: social token
        func socialLoginParameterSet(email: String,
                                     providerName: String!,
                                     name: String,
                                     profilePicture: String?,
                                     tokenSocial: String!) -> [String:String] {
            
            return [
                "email": email,
                "password": "",
                "provider_name": providerName,
                "device_type": "IOS",
                "name": name,
                "image": profilePicture!,
                "access_token": tokenSocial,
                "device_token": ""
                
            ]
        }
    /// API for social Login
    ///
    /// - Parameters:
    ///   - parameter: parameter in dictionary
    ///   - socialLoginId: UserId for the requested login
    ///   - loginType: Type of login
    func socialLoginApiRequest( parameter: [String:String], socialLoginId: String!,
                                loginType: String) {
        
        var parameterSet: [String : String] = ["provider_id": socialLoginId]
        parameterSet.merge(with: parameter)
        CSLoginApiModel.loginRequest(parentView: self, parameters: parameterSet) { (response) in
            /// response data
            let responseData: CSResponse = response.response!
            
            self.loginResponceHandler(responces: responseData)
        }
    }
    
    /// Response handler for login API
    ///
    /// - Parameter responces: responces
    func loginResponceHandler(responces: CSResponse!) {
        let accesstoken: String  = responces.token
        if responces.data!.profileImage.validateNotEmpty() {
            let profileImage: String = responces.data!.profileImage
            CSSharedData.SharedInstance.setUserDefaults(key: "profileImage", value: profileImage)
        }
        let userID: String = String(describing: responces.data!.userId)
        let stripeID: String = String(describing: responces.data!.stripeId)
        /// Store the user data globally
        CSSharedData.SharedInstance.setUserDefaults(key: "accessToken", value: accesstoken)
        CSSharedData.SharedInstance.setUserDefaults(key: "email", value: responces.data!.email)
        CSSharedData.SharedInstance.setUserDefaults(key: "userId", value: userID)
        CSSharedData.SharedInstance.setUserDefaults(key: "stripeId", value: stripeID)
        CSSharedData.SharedInstance.setUserDefaults(key: "productCount", value: "0")
        CSSharedData.SharedInstance.setUserDefaults(key: "loginType",
                                                    value: String(responces.data!.isPassword))
       
        CSSharedData.SharedInstance.isLoggedIn = true
        
        nam = responces.data!.birthDay
        imga = responces.data!.profileImage
        // UIApplication.shared.keyWindow?.rootViewController = UIApplication.setHomeAsRootViewController()
    }
}
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    *}
 */
extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 15
        self.layer.cornerRadius = 20
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
}
}
protocol loginSuccess:class {
    func loginSuccessFull(controller: UIViewController!)
}
extension LoginViewController : UITextFieldDelegate {
    /// Scrollview's ContentInset Change on Keyboard hide
    
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    /// register Keyboard
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == email {
            _ = password.becomeFirstResponder()
        } else if textField == password {
            dismissKeyboard()
        }
        return true
    }
}


