//
//  CSLoginAPImODULE.swift
//  milmila
//
//  Created by prasun sarkar on 11/13/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
class CSLoginApiModel: NSObject {
    /// API for Login request
    ///
    /// - Parameters:
    ///   - parentView: Any object
    ///   - parameters: parameters that are passed for function
    ///   - completionHandler: calling API while complted function
    
    class func loginRequest(parentView: AnyObject, parameters: [String:String]?,
                            completionHandler: @escaping (_ response: CSLoginModel) -> Void) {
        /// Start Loader
        let parentViewController = parentView as? CSParentViewController
        parentViewController?.startAnimating()
        CSApiHttpRequest.sharedInstance.executeRequestWithMethod(httpMethod: .post, path: LOGINAPI,
                                                                 parameters: parameters,
                                                                 completionHandler: { (response) in
                                                                    parentViewController?.stopAnimating()
                                                                    //Convert my response data to string
                                                                    let content = String(data: (response as? Data)!, encoding: String.Encoding.utf8)
                                                                    //call the the object mapper
                                                                    let responseData = Mapper<CSLoginModel>().map(JSONString: content!)
                                                                    if responseData?.statusCode == 200 {
                                                                        completionHandler(responseData!)
                                                                    } else {
                                                                        // Failure Message
                                                                        CSSharedData.SharedInstance.displayErrorAlert(parentView: parentViewController!, message: (responseData?.message)!)
                                                                    }
        })
{ (errorOccurred) in
            /// Stop Animating
            parentViewController?.stopAnimating()
//            CSSharedData.SharedInstance.displayErrorAlert(parentView: parentViewController!, message: (errorOccurred?.localizedDescription)!)
        }
    }
}
