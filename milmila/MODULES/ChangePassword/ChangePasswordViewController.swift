//
//  ChangePasswordViewController.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePasswordViewController: UIViewController {
    /// UIScrollView for current password
    @IBOutlet weak var changePasswordScrollView: UIScrollView!
    /// UITextField for current password
    @IBOutlet weak var currentPassword: SkyFloatingLabelTextField!
    /// UITextfield for new password
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    /// UITextField for confirm  new password
    @IBOutlet weak var confirmPassword: SkyFloatingLabelTextField!
    /// MARK:- UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        registerKeyboardNotifications()
    }
    @IBAction func bck(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.present(vc, animated: true, completion: nil)
    
    }
    override func viewWillDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    /// Tap gesture handler
    func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - Private Method
extension ChangePasswordViewController {
    /// change Password Button Action
    ///
    /// - Parameter sender: Any
    @IBAction func changePasswordAction(_ sender: Any) {
        self.view.endEditing(true)
       // self.resetPasswordValidation()
    }
    /// change Password Button Action
    ///
    /// - Parameter sender: Any
    @IBAction func endEditingAction(_ sender: Any) {
        self.view.endEditing(true)
    }
}
// MARK: - Validation Of all textField
extension ChangePasswordViewController {
    /// reset Password Validation
    func resetPasswordValidation() {
        if !CSUserDetailValidation.validatePassword(textField: currentPassword, viewController: self) { return }
        if !CSUserDetailValidation.validatePassword(textField: newPassword, viewController: self) {return}
        if(!newPassword.text!.isPasswordValid()) /* Check Password is Empty */ {
            self.showToastMessageTop(message:
                NSLocalizedString("Password should contain 1 Special Character,1 Number and 1 Uppercase Letters",
                                  comment: "password"))
            return
        }
        if !CSUserDetailValidation.validatePassword(textField: confirmPassword, viewController: self) { return }
        if newPassword.text != confirmPassword.text {
            self.showToastMessageTop(message:
                NSLocalizedString("Confirm password does not match with New password",
                                  comment: "Login"))
            return
        }
      /*  changePasswordApi(currentPassKey: currentPassword.text!,
                          newPassKey: newPassword.text!,
                          confirmPassKey: confirmPassword.text!)
 */
    }
}
// MARK: - UItextfield Delegates
extension ChangePasswordViewController: UITextFieldDelegate {
    /// Scrollview's ContentInset Change on Keyboard hide
    func keyboardWillHide(notification: NSNotification) {
        changePasswordScrollView.contentInset = UIEdgeInsets.zero
        changePasswordScrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    /// Scrollview's ContentInset Change on Keyboard show
    func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue
        let keyboardSize = keyboardInfo?.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0,
                                         bottom: (keyboardSize?.height)!, right: 0)
        changePasswordScrollView.contentInset = contentInsets
        changePasswordScrollView.scrollIndicatorInsets = contentInsets
    }
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    /// register Keyboard
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentPassword {
            _ = newPassword.becomeFirstResponder()
        } else if textField == newPassword {
            _ = confirmPassword.becomeFirstResponder()
        } else if textField == confirmPassword {
            _ = confirmPassword.resignFirstResponder()
        }
        return true
    }
}
// MARK: - Api Request
extension ChangePasswordViewController {
    /// api request for change password
    ///
    /// - Parameters:
    ///   - currentPassKey: current password
    ///   - newPassKey: new password
    ///   - confirmPassKey: confirmation password
  /*  func changePasswordApi(currentPassKey: String!,
                           newPassKey: String!,
                           confirmPassKey: String!) {
        let parameters: [String:String]! = ["old_password": currentPassKey,
                                            "new_password": newPassKey,
                                            "confirm_password": confirmPassKey]
        CSAccountModelApi.resetPassword(parentView: self, parameters: parameters) { (response) in
            self.currentPassword.text = ""
            self.confirmPassword.text = ""
            self.newPassword.text = ""
            self.showSuccessToastMessage(message:response.message)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                _ = self.navigationController?.popViewController(animated: true)
            }
        } */
    }


