//
//  WalkthroughViewController.swift
//  milmila
//
//  Created by prasun sarkar on 3/8/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit

class WalkthroughViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    /// Get started Action
    @IBAction func getStarted(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
