//
//  ProfileModel.swift
//  milmila
//
//  Created by prasun sarkar on 12/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class ProfileModel: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: Int?
    var status: Bool?
    var data: Int?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["data"]
        status <- map["status"]
        
        
    }
}

/*
 
 {"error":false,"sCode":200,"msg":"successfull added","status":true}
 
 */
