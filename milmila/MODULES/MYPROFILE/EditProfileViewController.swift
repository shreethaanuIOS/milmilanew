/*
 * CSEditProfileViewController
 * This class  is used to controll all functionality that happen in my profile Video detail view controller
 */
import UIKit
import DropDown
import DatePickerDialog
import SkyFloatingLabelTextField
import ObjectMapper
import Alamofire

class EditProfileViewController: UIViewController, UIGestureRecognizerDelegate {
    /// User First Name
    @IBOutlet var firstName: SkyFloatingLabelTextField!
    /// User Last Name
    @IBOutlet var lastName: SkyFloatingLabelTextField!
    /// User Email Id
    @IBOutlet var yourEmail: SkyFloatingLabelTextField!
    /// User Website
    @IBOutlet var yourWebsite: SkyFloatingLabelTextField!
    /// User BirthDate
    @IBOutlet var yourBirthDay: SkyFloatingLabelTextField!
    /// User PhoneNumber
    @IBOutlet var yourPhoneNumber: SkyFloatingLabelTextField!
    /// User living State
    @IBOutlet var yourState: SkyFloatingLabelTextField!
    /// User current living City
    @IBOutlet var yourCity: SkyFloatingLabelTextField!
    /// User one or two sentences about user
    @IBOutlet var writeAboutYou: FloatLabelTextView!
    /// User BirthPlace
    @IBOutlet var yourBirthplace: SkyFloatingLabelTextField!
    /// User current Occupation
    @IBOutlet var yourOccupation: SkyFloatingLabelTextField!
    /// User Married Status
    @IBOutlet var marriedStatus: SkyFloatingLabelTextField!
    /// User Gender
    @IBOutlet var yourGender: SkyFloatingLabelTextField!
    /// User Country
    @IBOutlet var yourCountry: SkyFloatingLabelTextField!
    /// User Facebook account url
    @IBOutlet var faceBookUrl: SkyFloatingLabelTextField!
    /// User Twitter Account url
    @IBOutlet var twitterUrl: SkyFloatingLabelTextField!
    /// User Save changes made to all textfields
    @IBOutlet var saveButton: UIButton!
    /// Scroll View
    @IBOutlet var editScrollView: UIScrollView!
    @IBOutlet var editScrollViewContent: UIView!
    /// get user Details Values
   // ---------> var userDetails: CSData?
    /// Gender dropdown
    var genderDropDown: DropDown!
    /// Married Status dropdown
    var marriedStatusDropDown: DropDown!
    /// city dropdown
    var cityDropDown: DropDown!
    /// state dropdown
    var stateDropDown: DropDown!
    /// Country drop DropDown
    var countryDropDown: DropDown!
    ///  conntry state list along with state id
 // --------->   var stateList = [CSCountryStates]()
 // --------->   var stateCompleteList = [CSCountryStates]()
    /// state city list along with city id
// --------->    var cityList = [CSCountryStates]()
 // --------->   var cityCompleteList = [CSCountryStates]()
    /// country list along with country id
// --------->    var countryList = [CSCountryStates]()
    /// country list
    var countryNamelist = [String]()
    /// state list
    var stateNamelist = [String]()
    /// state list
    var cityNamelist = [String]()
    var isEditApi = false
    /// set current
    var currentDateString = Date()
    
    var httpRequest = CSSwiftApiHttpRequest()
    // MARK: - UIViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        /// fetch state Details
        fetchCountry()
        // genderDropDown view cell height
        DropDown.appearance().cellHeight = 45
        /// Gender dropdown
        genderDropDown = DropDown()
        /// Married Status dropdown
        marriedStatusDropDown = DropDown()
        /// country drop down
        countryDropDown = DropDown()
        /// keyboard view done button view
        self.addDoneButtonOnKeyboard()
        self.addDoneButtonOnForTextViewKeyboard()
        self.yourEmail.isEnabled = true
        //SetNotification Status
        let tapgesture = UITapGestureRecognizer(target: self,
                                                action:
            #selector(EditProfileViewController.handleTap(sender:)))
        tapgesture.delegate = self
        self.editScrollViewContent.addGestureRecognizer(tapgesture)

        presetdata()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func BackAction(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "uploadSegue" {
            CSApplicationConstant.isEditVideoUploadView = false
        }
    }
    override func viewDidAppear(_ animated: Bool) {
      //  callApi()
        registerKeyboardNotifications()
    }
    override func viewDidDisappear(_ animated: Bool) {
        unregisterKeyboardNotifications()
    }
    // MARK: - Call Api

    // --------->
    /*    override func callApi() {
        if !isEditApi {
          self.fetchUserDetails()
        } else {
          self.vaildationSuccessCallApi()
        }
    } */
    // --------->
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
// MARK: - Button Action
extension EditProfileViewController {
    /// calendar button
    ///
    /// - Parameter sender: any
    @IBAction func calendarButtonAction(_ sender: Any) {
        if self.yourBirthDay.text! == "" {
            currentDateString = Date()
        }
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let date = Date()
        let yearsAgo = Calendar.current.date(byAdding: dateComponents, to: date)
        DatePickerDialog().show( NSLocalizedString("Select date",
                                                   comment: ""),
                                 doneButtonTitle: NSLocalizedString("Done",
                                                                    comment: ""),
                                 cancelButtonTitle: NSLocalizedString("Cancel",
                                                                      comment: ""),
                                 minimumDate: yearsAgo,
                                 maximumDate: date,
                                 datePickerMode: .date) { (date) in
                if let dt = date {
                    self.currentDateString = dt
                    self.yourBirthDay.text = dt.getDateStringFromUTCTime()
                }
        }
    }
    /// Select married Status
    /// - Parameter sender: Any
    @IBAction func marriedStatusButtonAction(_ sender: Any) {
         self.view.endEditing(true)
        CSDropDown.marriedDropDown(dropDown: marriedStatusDropDown, textField: marriedStatus)
    }
    /// Select Gender
    /// - Parameter sender: Any
    @IBAction func yourGenderButtonAction(_ sender: Any) {
         self.view.endEditing(true)
        CSDropDown.gnderDropDown(dropDown: genderDropDown, textField: yourGender)
    }
    /// Select Country
    ///
    /// - Parameter sender: Any
    @IBAction func yourCountryButtonAction(_ sender: Any) {
        self.view.endEditing(true)
        CSDropDown.countryDropDown(dropDown: countryDropDown,
                                   textField: yourCountry,
                                   dataSource: countryNamelist,
                                   completionHandler: { (index) in
                self.yourState.text = ""
                self.yourCity.text = ""
                self.yourCountry.tag = index
        })
    }
    /// Save button action
    /// - Parameter sender: Any
    @IBAction func saveButtonAction(_ sender: Any) {
        isEditApi = true
        setParameter()
        updateProf()
    }
    
    func presetdata(){
        
        yourEmail.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
        
        firstName.text =  GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
        yourCity.text = ""
        
    }
    
    
    func updateProf(){
        
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String =  BASE_URL + "customerB2B/userProfileUpdate"
        let params:[String:String] = [
            
            "userId":   myuserid,
            "firstName":  firstName.text!,
            "lastName":   lastName.text!,
            "loginName": yourEmail.text!,
            "pwd":   "",
            "mobile":  yourPhoneNumber.text!,
            "dateOfBirth": yourBirthDay.text!,
            "gender": yourGender.text!,
            "profileImage": "",
            "deviceId": DEVICE_TOKEN,
            "deviceType": "IOS",
            "signedFrm": "MOBILE",
            "socialSignUp": "N",
            "socialNwName": "",
            "isActive": "Y",
            "isVerified": "Y",
            "verifiedDate": "",
            "createDate": "1513777202000",
            "createUser": "MM_API",
            "gstNumber": yourBirthplace.text!,
            "gstName": yourOccupation.text!,
            "gstZipcode": "",
            "panNumber": yourWebsite.text!,
            "isGstVerified": "N",
            "gstVerifiedDate": "",
            "verifyLink": "",
            "gstAddr": "",
            "isGstOptional": ""
        ]
        print(url)
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<cartCountMappper>().map(JSONString: content!)
            
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.showToastMessageTop(message: "Profile updated succesfully")
                
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })

    }
    
    func setParameter() {
        self.view.endEditing(true)
        if lastName.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateLastName(textField: lastName, viewController: self) { scrolltop(lastName); return }
        }
        if yourWebsite.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateWebsite(textField: yourWebsite, viewController: self, checkType: 0) { scrolltop(yourWebsite); return }
        }
        if yourBirthDay.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateBirthDay(textField: yourBirthDay, viewController: self) { scrolltop(yourBirthDay); return }
        }
        if yourGender.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateGender(textField: yourGender, viewController: self) { return }
        }
        if yourOccupation.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateOccupation(textField: yourOccupation, viewController: self) { return }
        }
        self.validationOfOtherPersonalTextField()
    }
    
    
    /// Validation Of Textfield
    func validationOfOtherPersonalTextField() {
        if yourBirthplace.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateBirthPlace(textField: yourBirthplace, viewController: self) { return }
        }
        if marriedStatus.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateMarried(textField: marriedStatus, viewController: self) { return }
        }
        if yourCountry.text!.validateNotEmpty() {
            if !CSUserAddressValidation.validateCountry(textField: yourCountry, viewController: self, checkType: 0) { return }
        }
        if yourState.text!.validateNotEmpty() {
            if !CSUserAddressValidation.validateState(textField: yourState, viewController: self, checkType: 0) { return }
        }
        if yourCity.text!.validateNotEmpty() {
            if !CSUserAddressValidation.validateCity(textField: yourCity, viewController: self, checkType: 0) { return}
        }
        self.toValidateMadatoryFields()
    }
    func toValidateMadatoryFields() {
        if faceBookUrl.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateWebsite(textField: faceBookUrl, viewController: self,
                                                       checkType: 1) { return }
        }
        if twitterUrl.text!.validateNotEmpty() {
            if !CSUserDetailValidation.validateWebsite(textField: twitterUrl, viewController: self,
                                                       checkType: 2) {return }
        }
        if !CSUserDetailValidation.validateName(textField: firstName, viewController: self,
                                                checkType: 0) { scrolltop(firstName); return }
        if !CSUserDetailValidation.validateEmail(textField: yourEmail, viewController: self) { scrolltop(yourEmail); return }
        if !CSUserDetailValidation.validatePhone(textField: yourPhoneNumber, viewController: self) { scrolltop(yourPhoneNumber); return }
        vaildationSuccessCallApi()
    }
    /// Restore Button Action
    /// - Parameter sender: Any
    @IBAction func restoreButtonAction(_ sender: Any) {
        self.setValuesToTextField()
    }
    /// Button action to print menu content.
    /// - Parameter sender: Any
    @IBAction func menuListButtonAction(_ sender: Any) {
      //  self.addChildView(identifier:CSApplicationConstant.menuViewControllerIdentifier,
                      //    storyBoard: CSApplicationConstant.menuStoryboard)
    }
}
// MARK: - Text Field Delegate And Text view Delegate
extension EditProfileViewController: UITextFieldDelegate, UITextViewDelegate {
    func textViewShouldReturn(textView: UITextView!) -> Bool {
        self.view.endEditing(true)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField as? SkyFloatingLabelTextField == firstName {
            _ = lastName.becomeFirstResponder()
        } else if textField == lastName {
            _ = yourWebsite.becomeFirstResponder()
        } else if textField == yourWebsite {
            _ = yourPhoneNumber.becomeFirstResponder()
        } else if textField == yourPhoneNumber {
            _ = yourPhoneNumber.resignFirstResponder()
        } else if textField == yourState {
            _ = yourCity.becomeFirstResponder()
        } else if textField == yourCity {
            _ = yourCity.resignFirstResponder()
        }
        self.changeTextFieldAccordingToReturn(textField: textField)
        return true
    }
    func changeTextFieldAccordingToReturn(textField: UITextField) {
        if textField == yourBirthDay {
            _ = yourPhoneNumber.becomeFirstResponder()
        } else if textField == yourPhoneNumber {
            _ = yourPhoneNumber.resignFirstResponder()
        } else if textField == yourBirthplace {
            _ = yourOccupation.becomeFirstResponder()
        } else if textField == yourOccupation {
            _ = yourOccupation.resignFirstResponder()
        } else if textField == faceBookUrl {
            _ = twitterUrl.becomeFirstResponder()
        } else if textField == twitterUrl {
            _ = twitterUrl.resignFirstResponder()
        }
    }
    /// Text field Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let csTextField = textField as? SkyFloatingLabelTextField
        if csTextField == yourState {
            yourCity.text = ""
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange,
                  replacementText text: String) -> Bool {
        writeAboutYou.isScrollEnabled = true
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.characters.count // for Swift use count(newText)
        return numberOfChars < 250
    }
    /// After Validation Success call Api
    func vaildationSuccessCallApi() {
//        let otherProfileDetail: [String:String] =
//            setProfileOthersParameters(describe: writeAboutYou.text,
//                                       birthPlace: yourBirthplace.text,
//                                       occupation: yourOccupation.text,
//                                       gender: yourGender.text,
//                                       married: marriedStatus.text)
//        let addressProfileDetail: [String:String] =
//            setProfileAddressParameters(country: yourCountry.tag,
//                                        city: yourCity.text!,
//                                        state:yourState.text!)
//        let webProfileDetail: [String:String] =
//            setProfileWebsiteParameters(facebook: faceBookUrl.text,
//                                        twitter: twitterUrl.text,
//                                        email: yourEmail.text)
//        /// name
//        let nameProfileDetail: [String:String] = setProfileNameParameters(
//            name: firstName.text, last: lastName.text, webSite:yourWebsite.text,
//            birthDate: yourBirthDay.text, phone: yourPhoneNumber.text)
//        /// Set Profile Parameters
//        self.editUserDetails(paramters:
//            nameProfileDetail.merged(with: webProfileDetail)
//                .merged(with: addressProfileDetail)
//                .merged(with: webProfileDetail)
//                .merged(with: otherProfileDetail))
    }
}
