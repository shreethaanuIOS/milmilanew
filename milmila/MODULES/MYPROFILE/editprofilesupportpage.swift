//
//  editprofilesupportpage.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//


import UIKit
import SkyFloatingLabelTextField
// MARK: - Private Methods
extension EditProfileViewController {
    /// MARK:- Add Done button to Keyboard
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0,
                                                             width: self.view.frame.size.width,
                                                             height:
            self.view.frame.size.height/17))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
                                        target: nil,
                                        action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Done",
                                                                             comment: ""),
                                                    style: UIBarButtonItemStyle.done,
                                                    target: self,
                                                    action:
            #selector(EditProfileViewController.doneButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.yourPhoneNumber.inputAccessoryView = doneToolbar
    }
    /// Keyboard done button dfor textview
    func addDoneButtonOnForTextViewKeyboard() {
        writeAboutYou.isScrollEnabled = false
        let doneToolbar: UIToolbar = UIToolbar(frame:
            CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/17))
        doneToolbar.barStyle = UIBarStyle.default
        let flexSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace,
                                        target: nil,
                                        action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done",
                                                    style: UIBarButtonItemStyle.done,
                                                    target: self,
                                                    action:
            #selector(EditProfileViewController.doneTextViewButtonAction))
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.writeAboutYou.returnKeyType = UIReturnKeyType.init(rawValue: 0)!
        self.writeAboutYou.inputAccessoryView = doneToolbar
    }
    /// Tap gesture handler
    func handleTap(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    /// done button finction
    func doneButtonAction() {
        self.view.endEditing(true)
    }
    /// done button finction
    func doneTextViewButtonAction() {
        self.view.endEditing(true)
    }
    /// setting the values to text field on fetching data
    func setValuesToTextField() {
//        firstName.text = userDetails?.name
//        lastName.text = userDetails?.lastName
//        yourEmail.text = userDetails?.email
//        yourEmail.textColor = UIColor.lightGray
//        yourWebsite.text = userDetails?.website
//        yourPhoneNumber.text = userDetails?.phoneNumber
//        yourState.text = userDetails?.stateName
//        yourCity.text = userDetails?.cityName
//        yourCountry.text = userDetails?.countryName
//        writeAboutYou.text = userDetails?.description
//        yourBirthplace.text = userDetails?.birthPlace
//        yourOccupation.text = userDetails?.occupation
//        marriedStatus.text = userDetails?.status
//        yourGender.text = userDetails?.gender
        if yourGender.text == "male" || yourGender.text == "Male" {
            self.yourGender.tag = 0
        } else {
            self.yourGender.tag = 1
        }
        if marriedStatus.text == "married" || marriedStatus.text == "Married" {
            self.marriedStatus.tag = 0
        } else {
            self.marriedStatus.tag = 1
        }
//        faceBookUrl.text = userDetails?.facebookUrl
//        twitterUrl.text = userDetails?.twitterUrl
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
       // if userDetails?.birthDay != "" {
          //  currentDateString = dateFormatter.date(from: (userDetails?.birthDay)!)!
          //  self.yourBirthDay.text = currentDateString.getDateStringFromUTCTime()
        }
      //  setCountryTag(country: userDetails?.countryId)
  //  }
    /// country Tag
    ///
    /// - Parameter state: country id
    func setCountryTag(country: Int!) {
       // for list in 0..<self.countryList.count where self.countryList[list].countryStateId == country {
         //   yourCountry.tag = list
            return
        }
    }
// MARK: - StateCity
extension EditProfileViewController {
    /// fetch Country
    func fetchCountry() {
//        CSEditProfileApiModel.fetchCounrtyStateAndCity(parentView: self,
//                                                       parameters: "",
//                                                       completionHandler: { (responce) in
//                                                        self.countryList .removeAll()
//                                                        self.countryNamelist.removeAll()
//                                                        self.countryList = (responce.response?.places)!
//                                                        for list in 0..<self.countryList.count {
//                                                            self.countryNamelist.append(self.countryList[list].name)
//                                                        }
//        })
    }
    /// date Convert
    /// - Parameter date: string date
    /// - Returns: return string date
    func dateToString(_ date: Date) -> String {
        let currentDateFormatter = DateFormatter()
        currentDateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        // change to a readable time format and change to local time zone
        currentDateFormatter.timeZone = NSTimeZone.local
        currentDateFormatter.dateStyle = .long
        currentDateFormatter.dateFormat = "yyyy-MM-dd" // reset the locale
        let dateString = currentDateFormatter.string(from: currentDateString)
        return dateString
    }
}
// MARK: - Parameter set Request and Api Request
extension EditProfileViewController {
    /// Parameters Of Profile Address
    /// - Parameters:
    /// - country: Country of user
    /// - state: current state of user
    /// - city: current city
    /// - Returns: Dictionary
    //func setProfileAddressParameters(country: Int!, city: String, state: String)
    //    -> [String:String] {
//            var profileAddressDetails = [String: String]()
//            profileAddressDetails["state"] = state
//            profileAddressDetails["city"] = city
//            if !(yourCountry.text!.validateNotEmpty()) {
//                profileAddressDetails["country"] = "0"
//                return profileAddressDetails
//            }
//            profileAddressDetails["country"] = String(self.countryList[country].countryStateId)
           // return profileAddressDetails
   // }
    ///  Parameters Of Profile Website
    ///
    /// - Parameters:
    /// - facebook: facebook account id
    /// - twitter: twitter account id
    /// - Returns: Dictionary
    
    
//    func setProfileWebsiteParameters(facebook: String!,
//                                     twitter: String!,
//                                     email: String!)
//        -> [String:String] {
//            let webDetails: [String:String] = ["facebook_url": facebook,
//                                               "twitter_url": twitter,
//                                               "email": (userDetails?.email)!]
//            return webDetails
//    }
    /// Set Name Details
    ///
    /// - Parameters:
    /// - name: name of user
    /// - last: last name of user
    /// - webSite: website of user
    /// - birthDate: date of birt of user
    /// - phone: phone number of user
    /// - Returns: Dictionary
//    func setProfileNameParameters(name: String!, last: String!,
//                                  webSite: String!, birthDate: String!,
//                                  phone: String!) -> [String:String] {
//        var profileNameDetails: [String:String] = ["name": name,
//                                                   "last_name": last,
//                                                   "mobile_number": phone,
//                                                   "website": webSite]
//        if birthDate.validateNotEmpty() { profileNameDetails["birthday"] = dateToString(currentDateString) }
//        return profileNameDetails
//    }
    /// Description Details
    ///
    /// - Parameters:
    /// - describe: about user few lines
    /// - birthPlace: place of birth
    /// - occupation: occupation of user
    /// - gender: gender of user
    /// - married: married status
    /// - Returns: Dictionary
    func setProfileOthersParameters(describe: String!, birthPlace: String!,
                                    occupation: String!, gender: String!,
                                    married: String!) -> [String:String] {
        let profileOtherDetails: [String:String] = ["description": describe,
                                                    "birthplace": birthPlace,
                                                    "occupation": occupation,
                                                    "status": married,
                                                    "gender": gender]
        return profileOtherDetails
    }
    /// edit profile detail call api
    ///
    /// - Parameter paramters: parameters as dict
    func editUserDetails(paramters: [String:String]) {
//        CSEditProfileApiModel.editUserDetail(parentView: self, parameters:paramters, completionHandler: { (response) in
//            self.showSuccessToastMessage(message:response.message)
//            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                _ = self.navigationController?.popViewController(animated: true)
//            }
//        })
    }
    /// fetch user detail api
    func fetchUserDetails() {
//        CSEditProfileApiModel.fetchUserDetail(parentView: self,
//                                              parameters: nil,
//                                              completionHandler: { (response) in
//                                                self.userDetails = (response.response?.customers)!
//                                                self.setValuesToTextField()
//        })
    }
    /// scroll to postion
    func scrolltop(_ scrollToView: AnyObject) {
        editScrollView.scrollsToTop = true
        let view = scrollToView as? SkyFloatingLabelTextField
        editScrollView.setContentOffset(CGPoint(x: 0, y: (view?.frame.origin.y)!),
                                        animated: true)
    }
}
// MARK: - Private Methods
extension EditProfileViewController {
    /// Register Notification
    func registerKeyboardNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector:
            #selector(EditProfileViewController.keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector:
            #selector(EditProfileViewController.keyboardWillHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardWillHide,
                                               object: nil)
    }
    /// Scrollview's ContentInset Change on Keyboard show
    func keyboardDidShow(notification: NSNotification) {
        let userInfo: NSDictionary = notification.userInfo! as NSDictionary
        let keyboardInfo = userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue
        let keyboardSize = keyboardInfo?.cgRectValue.size
        let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: (keyboardSize?.height)!, right: 0)
        editScrollView.contentInset = contentInsets
        editScrollView.scrollIndicatorInsets = contentInsets
    }
    /// Scrollview's ContentInset Change on Keyboard hide
    func keyboardWillHide(notification: NSNotification) {
        editScrollView.contentInset = UIEdgeInsets.zero
        editScrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    /// UnRegistering Notifications
    func unregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
}

