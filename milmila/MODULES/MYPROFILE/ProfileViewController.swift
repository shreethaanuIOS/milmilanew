//
//  ProfileViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/25/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import GoogleSignIn
import Google

class ProfileViewController: UIViewController, UITableViewDelegate,UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIGestureRecognizerDelegate {

    @IBOutlet weak var userLoc: UILabel!
    @IBOutlet weak var userDetail: UILabel!
    @IBOutlet weak var mediaBottom: NSLayoutConstraint!
    @IBOutlet weak var profileTableview: UITableView!
    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var mediaview: UIView!
    @IBOutlet weak var blurryView: UIVisualEffectView!
    @IBOutlet weak var profilePic: UIImageView!
   
    
    var shownIndexes : [IndexPath] = []
    let imagePicker = UIImagePickerController()
    let Profiledata = ["Edit Profile","Notification","Update Address","Reffereal Code","GST Information","My Orders","Logout"]
    let cellSpacingHeight: CGFloat = 5
    var passingtoimg = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var myImg = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSProfilePicture) as? String
       
        if (myImg == "")
        {
        profilePic.image = #imageLiteral(resourceName: "accountimage")
        }
        else if (myImg == nil){
        profilePic.image = #imageLiteral(resourceName: "accountimage")
        }
            
        else {
    //     let data = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSProfilePicture)
      //      profilePic.image = UIImage(data: data as! Data)
       profilePic.setImageWithUrl(imageURl: myImg)
        }
    
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        imagePicker.delegate = self
        profileTableview.tableFooterView = UIView(frame: .zero)
        blurryView.isHidden = true
        
        let tapGestOnBlurView = UITapGestureRecognizer(target: self, action: #selector(dismissMedia))
        blurryView.addGestureRecognizer(tapGestOnBlurView)
        tapGestOnBlurView.delegate = self
        
        // Do any additional setup after loading the view.
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName) == nil){
           userDetail.text = "Hello User"
        }
        else {
            userDetail.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
            userLoc.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
        }
        
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    @IBAction func EditImage(_ sender: Any) {
        showMediaOption()
    }
    @IBAction func GalleryAction(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else {
        noCamera()
        }
        self.mediaview.isHidden = true
        self.blurryView.isHidden = true
    }
    
    @IBAction func EditProfile(_ sender: Any) {

        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            var imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            }
        else {
            noCamera()
        }
        self.mediaview.isHidden = true
        self.blurryView.isHidden = true
    }
   
    
    //MARK:- Methods
    // To show bottom media view
    func showMediaOption() {
        self.view.endEditing(true)
        self.blurryView.isHidden = false
        self.mediaview.isHidden = false
        
        UIView.animate(withDuration: 0.20, delay: 0, options: .curveEaseIn, animations: {() -> Void in
            self.mediaBottom.constant = 10
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
   
    
    //MARK:- To hide Media view
    func dismissMedia(){
        
        UIView.animate(withDuration: 0.20, delay: 0, options: .curveEaseOut, animations: {() -> Void in
            
            self.mediaBottom.constant = -200
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.mediaview.isHidden = true
            self.blurryView.isHidden = true
        })
    }
    
    //MARK:- when camera option is not available
    func noCamera(){
        let alertVC = UIAlertController(title: "No Camera",message: "Sorry, This device has no camera",preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK",style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(alertVC,animated: true,completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        profilePic.image = image
        dismiss(animated:true, completion: nil)
        let alert = UIAlertView(title: "Wow",
                                message: "Profile picture added sucessfully",
                                delegate: nil,
                                cancelButtonTitle: "Ok")
        alert.show()
        
    }
    
    @IBAction func back(_ sender: Any) {

      //  let imgdata: NSData = UIImagePNGRepresentation(profilePic.image!) as! NSData
      //  GlobalConstants.General.USERDEFAULTS.setValue(imgdata, forKey: GlobalConstants.Login.CSProfilePicture)
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Profiledata.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.row == 1 {
        }
        else if indexPath.row == 2 {
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "ShippingInfoViewController") as! ShippingInfoViewController
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.row == 3 {
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "inviteViewController") as! inviteViewController
               self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.row == 4 {
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GSTVerificationViewController") as! GSTVerificationViewController
            self.present(vc, animated: true, completion: nil)
        }
        else if indexPath.row == 5 {
                    let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "myordersViewController") as! myordersViewController
                    self.present(vc, animated: true, completion: nil)
        }
        else {
          self.logOutAction ()
        }
    }
    
    
   func logOutAction(){
    
    let alert = UIAlertController(title: "Alert", message: "Would you like to logout?", preferredStyle: UIAlertControllerStyle.alert)
   
        alert.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default, handler:  { action in
       
        GIDSignIn.sharedInstance().signOut()
        UserDefaults.standard.set(nil, forKey: GlobalConstants.Login.CSUserToken)
        UserDefaults.standard.set(nil, forKey: GlobalConstants.Login.CSEmail)
        UserDefaults.standard.set(nil, forKey: GlobalConstants.Login.CSUserName)
        UserDefaults.standard.set(nil, forKey: GlobalConstants.Login.CSProfilePicture)
       
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }))
    alert.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel, handler: nil))
    self.present(alert, animated: true, completion: nil)
    }
  
    
    func tableView(_ tableView: UITableView,willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.contentView.backgroundColor = UIColor.clear
        let whiteRoundedView : UIView = UIView(frame: CGRect(x:0, y:10, width:self.view.frame.size.width, height:70))
        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 3.0
        whiteRoundedView.layer.shadowOffset = CGSize(width: -1, height: 1)
        whiteRoundedView.layer.shadowOpacity = 0.5
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubview(toBack: whiteRoundedView)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
     let cell = tableView.dequeueReusableCell(withIdentifier:"profilecell", for: indexPath) as! profileTableViewCell
         cell.profileData.text = Profiledata[indexPath.row]
        
        if indexPath.row == 1 {
            cell.notify.isHidden = false
        } else{
            cell.notify.isHidden = true
        }
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
