//
//  updateProfMapper.swift
//  milmila
//
//  Created by prasun sarkar on 2/27/18.
//  Copyright © 2018 Milmila. All rights reserved.
//
import UIKit
import ObjectMapper

class updateProfMapper: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
    }
}

