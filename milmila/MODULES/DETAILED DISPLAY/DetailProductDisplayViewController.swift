//
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

typealias ButtonHandler = (coloursTableViewCell) -> Void

class DetailProductDisplayViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate,UITableViewDelegate, UITableViewDataSource, PGTransactionDelegate {
    
    @IBOutlet weak var availableitems: UILabel!
    @IBOutlet weak var theGreenContent: UILabel!
    @IBOutlet weak var colourheightTable: NSLayoutConstraint!
    @IBOutlet weak var sizesCollectionView: UICollectionView!
    @IBOutlet weak var colourTableView: UITableView!
    @IBOutlet weak var priceCollectionView: UICollectionView!
    @IBOutlet weak var qtyValue: UITextField!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var sizeCollectionView: UIView!
    @IBOutlet weak var closeCongratS: UIButton!
    @IBOutlet weak var congratsView: SpringView!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var itemnumber: UILabel!
    @IBOutlet weak var itemname: UILabel!
    @IBOutlet weak var nosizesView: UIView!
    @IBOutlet weak var enquiryView: UIView!
    
    var tabselect : Bool!
    var buynowdata: Int!
    var mybuynowdata = ""
    var buyNowProductRelateId: Int!
  
    @IBOutlet weak var adtocartViewHeight: NSLayoutConstraint!
    @IBOutlet weak var DetailedImageDisplay: UICollectionView!
    @IBOutlet weak var ContentView: UIView!
   
     var isSizeAvail: String!
     var arrayValue = [String]()
     var spuArray = [String]()   //if it is a String array
     var textfieldarray = [String]()
     var myanotherarray = [String]()
     var merchant:PGMerchantConfiguration!
     var checksumhash = ""
     var orderId = ""
     var custId = ""
     var element : String!
     var qtyElement : String!
    var spuElement: String!
     var txnamount : Int?
     var SizeToStore = NSMutableArray()
     var httpRequest = CSSwiftApiHttpRequest()
     var resultO: Int!
    
    /// PASSING DATA TO DETAILS CONTROLLER
    
    var fromtype = ""
    var detailedImageContentArray = [DisplayCateObjs]()
    var productsImageDArray = [imgDetails]()
    var whAvailabilityArray = [Availability]()
    var StrCID = ""
    @IBOutlet weak var descTex: UILabel!
    
    var c = [qtyPriceDetails]()
    var mrpriceList = [qtyPriceDetails]()
    // var MyArrayList = [MyArrayListMapping]()
   //  var myarrarcount =  [MyArrayListMapping]()
    var MyQtyDetails = [qtyDetails]()
    var qtycount : Int!
    var IMGPASSto = ""
    var NAMEPASSto = ""
    var DESCRIPTIONPASSto = ""
    var PRICEPASSto: String!
    var IMAGEARRAYTOPASS = ""
    var PRODID = ""
    var sizeIndexPath: Int!
    var colourcomaRaw = ""
    var qtyCommaRaw = ""
    var cateid: Int!
    var callurl = ""
    var categname = ""
    var isColourDetalAvailable = ""
    var fullNameArr: [String]!
    var qtynameArr: [String]!
    var myColurListValueArray: [String]!
    var guess:Int!
  
    @IBOutlet weak var enquireSView: UIView!
    
    var thaanusCSS = "<style>.table.table-bordered td:nth-child(2n+1) {background: #f7f7f7;font-weight: normal;font-family: robotob;font-size: 12px;color: #666;}.table.table-bordered td p {background: #f7f7f7;font-weight: normal;font-family: robotob;font-size: 12px;color: #666;}.table.table-bordered td{border: 1px solid #dddddd;text-align: left;padding: 3px;  font-size: 12px;}table{border-collapse:collapse;border:none;}</style>"
 
    var qtyToPassTextField = "1"
    var mycolourCountcount = ""
    var changeValues = "1"
    var colourPssesTo  = ""
  
    @IBOutlet weak var descriptionViewWeb: UIWebView!
 
    var orderedType = ""
    var sizsPassesTo = ""
    var moq: Int?
    var RESPCODE = ""
    var RESPMSG = ""
    var BANKNAME = ""
    var PAYMENTMODE = ""
    var BANKTXNID : Int?
    var STATUS = ""
    var payDate = ""
    var txnamt = ""
    var GATEWAYNAME = ""
    var TXNID : Int?
    
      // DATA COREDATA
    
    var arr = NSMutableArray()
    
    var savedarr = NSMutableArray()
    
    var count = Int()
    
    var data: [UInt] = Array(repeating: 0, count: 10)
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var tasks: [Entity] = []
    
    // GST INFORMATION
    
    var cgst: Int!
    var sgst: Int!
    var igst: Int!
    var cgst_val = ""
    var sgst_val = ""
    var igst_val: Int!
    var sku = ""
    var gst: Int!
    var sum = 0
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setMerchant()
        buynowdata = 0
        mybuynowdata = "0"
        tabselect = false
        count = 0
        congratsView.isHidden = true
        
        if(NAMEPASSto == "") {
        
        }
        else {
        
        detailedProductListing()
        generator()
        DetailedImageDisplay.isHidden = false
        itemname.text = NAMEPASSto
        rate.text = "Price details"
        
        var moqStr = ""
        nosizesView.isHidden = true
        
        moqStr = String(describing: moq)
        itemnumber.text = ""
        
        priceCollectionView.isHidden = false
        qtyValue.text = qtyToPassTextField
        print(isSizeAvail)
        
       // GSTCalc()
        enquiryView.isHidden = true
        enquireSView.isHidden = true
        hideQty()
        GSTCalc()
        self.colourTableView.reloadData()
        
        qtynameArr = qtyCommaRaw.components(separatedBy: ",")
        fullNameArr = colourcomaRaw.components(separatedBy: ",")
        }
    }
    
    func hideQty() {
        qtyValue.isHidden = true
        plus.isHidden = true
        minus.isHidden = true
    }
    
    @IBAction func enqire(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EnquiryViewController") as! EnquiryViewController
        vc.enquire  = sku
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    func GSTCalc() {
        
        let url:String =  BASE_URL + "transaction/tax/findList.html?prodId=" + PRODID
        print(url)
        let params:[String:String] = [:]
        
       httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<gstMapper>().map(JSONString: content!)
        print(responseData)
            if(responseData?.sCode == 200) {
           
            self.sgst = responseData?.dataS.sgst
            self.gst = responseData?.dataS.gst
            self.igst = responseData?.dataS.igst
            self.cgst = responseData?.dataS.gst
          
            print("######## GST ###########")
            print("My gst value is :   ", self.gst)
            print("My sgst value is :   ", self.sgst)
            print("My igst value is :   ", self.igst)
        } 
        else {
          self.showToastMessageTop(message: "Internal server error")
            print("GST FAILED")
        }
        })
    }
    
    func sumup(){
   
    let intArray = textfieldarray.map({Int($0) ?? 0})
    let sumedArr =  intArray.reduce(0, {$0 + $1})
    
    print(sumedArr)
    }
    
    func generator(){
        
        let stackView = UIStackView()
       
        stackView.axis = .horizontal
        stackView.alignment = .fill // .leading .firstBaseline .center .trailing .lastBaseline
        stackView.distribution = .fill // .fillEqually .fillProportionally .equalSpacing .equalCentering
        
        let label = UILabel()
        label.text = "Text"
        stackView.addArrangedSubview(label)
        }
  
    @IBAction func closecongAction(_ sender: Any) {
        congratsView.isHidden = true
        ContentView.alpha = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func addAction(_ sender: Any) {
        if (Int(qtyValue.text!)! > 0){
            qtyValue.text = String(Int(qtyValue.text!)! + 1)
        } else {
            qtyValue.text = "1"
        }
    }
    
    @IBAction func minus(_ sender: Any) {
    
        if (Int(qtyValue.text!)! > 1) {
            qtyValue.text = String(Int(qtyValue.text!)! - 1)
        }
        else {
            qtyValue.text = "1"
        }
    }
    
    @IBAction func addToCart(_ sender: Any) {
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ) {
            let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
        
        else {
            ContentView.backgroundColor = UIColor.white
            ContentView.alpha = 0.5
            print(myColurListValueArray)
            process(arrayValue)
        }
    }
    
    func decrementAction(sender: UIButton!) {
        if (Int(changeValues)! > 1) {
            changeValues = String(Int(changeValues)! - 1)
        }
        else {
            changeValues = "1"
        }
    }
    
    func incrementAction(sender: UIButton!) {
        
        if (Int(changeValues)! > 0) {
            changeValues = String(Int(changeValues)! + 1)
        }
        else {
        
            changeValues = "1"
        
        }
    }
    
     func incrementHandler() -> ButtonHandler {
        return { [unowned self] cell in
            guard let row = self.colourTableView.indexPath(for: cell)?.row else { return }
            self.data[row] = self.data[row] + UInt(1)
            guard let tablecell = cell as? coloursTableViewCell else {
                return
                
            }
            
            if self.data[row] > 0 {
                
                if self.savedarr.contains(tablecell.changeValue.text)
                {
                    
                }
                else
                {
                    print(tablecell.changeValue.text)
                    self.savedarr.add(tablecell.changeValue.text)
                   
                    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    let task = Entity(context: context)
                    task.name = tablecell.changeValue.text!
                    task.quantity =  Int64(Int(self.data[row]))
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                }
            }
            self.reloadCellAtRow(row: row)
        }
    }
    
    
     func decrementHandler() -> ButtonHandler {
        return { [unowned self] cell in
            guard
                let row = self.colourTableView.indexPath(for: cell)?.row, self.data[row] > 0
                else { return }
            self.data[row] = self.data[row] - UInt(1)
            guard let tablecell = cell as? coloursTableViewCell else { return }
            
            if self.data[row] > 0 {
                if self.savedarr.contains(tablecell.changeValue.text)
                {
                    
                }
                else
                {
                    self.savedarr.add(tablecell.changeValue.text)
                    print(self.savedarr)
                    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                    
                    let task = Entity(context: context)
                    task.name = tablecell.changeValue.text!
                    // Save the data to coredata
                    
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                }
                
            }
            else{
                
                self.savedarr.remove(tablecell.changeValue.text)
                print(self.savedarr)
                let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                
                let task = Entity(context: context)
                //  task.name = tablecell.itemlbl.text!
                // Save the data to coredata
                context.delete(task)
                (UIApplication.shared.delegate as! AppDelegate).saveContext()
            }
            self.reloadCellAtRow(row: row)
        }
    }
    
    private func reloadCellAtRow(row: Int) {
        let indexPath = IndexPath(row: row, section: 0)
        colourTableView.beginUpdates()
        colourTableView.endUpdates()
    }
    
    func prepayment(){
        
       let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
         let myuserid = String(UID)
        let url:String =  BASE_URL + "/transaction/prepayment/pay.html"
        let params:[String:String] = [
            "userId":       myuserid,
            "paymentType":  "paytm",
            "orderType":    orderedType,
            "bnId":         mybuynowdata
        ]
        print(url)
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
           
            let responseData = Mapper<prepaymentMapper>().map(JSONString: content!)
            print(responseData?.respdata)
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.checksumhash = (responseData?.respdata.CHECKSUMHASH)!
                self.txnamt = (responseData?.respdata.TXN_AMOUNT)!
                self.orderId = (responseData?.respdata.ORDER_ID)!
                self.custId = (responseData?.respdata.CUST_ID)!
                self.callurl = (responseData?.respdata.CALLBACK_URL)!
                self.createPayment()
            }
                
            else {
                 self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
    }
    
    func setMerchant(){
        merchant  = PGMerchantConfiguration.default()!
        //user your checksum urls here or connect to paytm developer team for this or use default urls of paytm
        // merchant.checksumGenerationURL = "http://getlook.in/cgi-bin/checksum_generate.cgi";
        // merchant.checksumValidationURL = "http://getlook.in/cgi-bin/checksum_validate.cgi";
        
        // Set the client SSL certificate path. Certificate.p12 is the certificate which you received from Paytm during the registration process. Set the password if the certificate is protected by a password.
        merchant.clientSSLCertPath = nil; //[[NSBundle mainBundle]pathForResource:@"Certificate" ofType:@"p12"];
        merchant.clientSSLCertPassword = nil; //@"password";
        
        //configure the PGMerchantConfiguration object specific to your requirements
        merchant.merchantID = "Retail109";//paste here your merchant id  //mandatory
        merchant.website = "MILMILWAP";//mandatory
        merchant.industryID = "Retail109";//mandatory
        merchant.channelID = "WAP"; //provided by PG WAP //mandatory
    }
    
    
    func createPayment(){
        
        var orderDict = [String : String]()
        //   orderDict["MERCHANT_KEY"] = "DykWP&oQAExyw59I";
        
        orderDict["MID"] = "MILMIL83960179030841";//paste here your merchant id   //mandatory
        orderDict["CHANNEL_ID"] = "WAP"; // paste here channel id                       // mandatory
        orderDict["INDUSTRY_TYPE_ID"] = "Retail109";//paste industry type              //mandatory
        orderDict["WEBSITE"] = "MILMILWAP";// paste website                            //mandatory
        
        //Order configuration in the order object
        
        orderDict["TXN_AMOUNT"] = txnamt; // amount to charge                      // mandatory
        orderDict["ORDER_ID"] = orderId;//change order id every time on new transaction
        orderDict["REQUEST_TYPE"] = "DEFAULT";// remain same
        orderDict["CUST_ID"] = custId; // change acc. to your database user/customers
       
        //orderDict["MOBILE_NO"] = "7777777777";// optional
        //  orderDict["EMAIL"] = "shree@milmila.com"; //optional
        
        orderDict["CHECKSUMHASH"] = checksumhash;
        orderDict["CALLBACK_URL"] = callurl
        
        let pgOrder = PGOrder(params: orderDict)
        
        let transaction = PGTransactionViewController.init(transactionFor: pgOrder)
        transaction!.serverType = eServerTypeProduction
        transaction!.merchant = merchant
        transaction!.loggingEnabled = true
        transaction!.delegate = self
        self.present(transaction!, animated: true, completion: {
            self.showAlert(title: "paytm response", message: "completd transaction")
        })
        
    }
    
    func showAlert(title:String,message:String)  {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func buynowApi(){
        
        self.GSTCalc()
            if arrayValue.isEmpty {
            self.showToastMessageTop(message: "Choose Colour")
            return
        }
            if textfieldarray.isEmpty {
            self.showToastMessageTop(message: "Choose Colour")
            return
        }
        
        element = arrayValue.first!
        qtyElement = textfieldarray.first!
        
        var myqty = qtyValue.text
        guess = Int(qtyValue.text!)
       
        let guess1:Int! = Int(PRICEPASSto)
        resultO = guess * guess1
        
        let myGst =  gst! * resultO / 100
        let smyGst = sgst! * resultO / 100
        let imyGst = igst! * resultO / 100
        let MyOrderValue = resultO + imyGst
        let userid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken)! as! Int
        let myuserid = String(userid)
      
        let url:String = BASE_URL + "user/addBuyNow"
        let params:[String:String] = [
            "userId":myuserid,
            "prodId": PRODID,
            "pname": NAMEPASSto,
            "pimage": IMGPASSto,
            "qty": qtyValue.text!,
            "size"  : sizsPassesTo,
            "color" : element,
            "price": String(resultO),
            "bnId" : mybuynowdata,
            "orderVal" : String(MyOrderValue),
            "cgst": String(describing: cgst),
            "sgst": String(describing: gst),
            "isgst": String(describing: igst),
            "sku": sku,
            "cgst_val": String(myGst),
            "sgst_val": String(smyGst),
            "igst_val": String(imyGst),
            "tax_val": String(describing: igst),
            "total_val":""
        ]
        
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<buynowmapper>().map(JSONString: content!)
           
            if(responseData?.sCode == 200) {
                if self.arrayValue.isEmpty {
                   self.showSuccessToastMessage(message: "Item Ready to buy")
                    let vc =  UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
                    vc.fromWay = "BUYNOW"
                    vc.mybuynowdata = self.mybuynowdata
                    self.present(vc, animated: true, completion: nil)
                   // self.prepayment()
                }
                else {
                     self.process(self.textfieldarray)
                     self.process(self.textfieldarray)
                    self.arrayValue = Array(self.arrayValue.dropFirst())
                    self.textfieldarray = Array(self.textfieldarray.dropFirst())
                    print("one more to go")
                }
            }
        })
    }
    
    func detailedProductListing()
    {
        let url:String = BASE_URL + "product/productDetails?p=" + PRODID
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            print(url)
           
            let responseData = Mapper<DetailedDisplaModel>().map(JSONString: content!)
            if(responseData?.maping.sCode == 200)
           
            {
              
                print("++++++++++++++ succes +++++++++++++++")
               
                self.moq = (responseData?.maping.respdata[0].moq)
            
                self.detailedImageContentArray = (responseData?.maping.respdata)!
                
                self.sku = (responseData?.maping.respdata[0].sku)!
                
                self.isSizeAvail = (responseData?.maping.respdata[0].isSizeAvail)!
            
                self.productsImageDArray = (responseData?.maping.respdata[0].imgDetails)!
               
                self.whAvailabilityArray = (responseData?.maping.respdata[0].whAvailability)!
                
                self.mrpriceList = (responseData?.maping.respdata[0].wsDetails[0].qtyPriceDetails)!
                
                var htmldetail = self.detailedImageContentArray[0].descDetails[0].pDesc + self.thaanusCSS;
                
                let characterset = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789,.")
               
                if self.whAvailabilityArray.isEmpty {
                    self.enquiryView.isHidden = false
                    self.enquireSView.isHidden = false
                    self.theGreenContent.text = "Sorry the product is not available in warehouse, please fill the form to let us know"
                    self.availableitems.textColor = UIColor.red
                    self.availableitems.text = "Sold out"
                }
                    
                else {
                     self.availableitems.textColor = UIColor.green
                     let myProdQuantity = self.whAvailabilityArray[0].availQty!
                     let StringProd = String(describing: myProdQuantity)
                     self.availableitems.text = StringProd + "   Available"
                }
                
                if  self.isSizeAvail == "N" {
                    self.sizesCollectionView.bounds.height == 0
                    self.colourTableView.reloadData()
                    self.textfieldarray.append(self.qtyValue.text!)
                    self.nosizesView.isHidden = false
                }
                    
                else {
                    self.nosizesView.isHidden = true
                    self.sizesCollectionView.bounds.height == 50
                    self.sizesCollectionView.reloadData()
                    self.colourTableView.reloadData()
                }
                
                if htmldetail.rangeOfCharacter(from: characterset.inverted) != nil {
                    print("string contains special characters")
                    self.descriptionViewWeb.loadHTMLString(htmldetail, baseURL: nil)
                    self.descTex.isHidden = true
                }
                    
                else {
                    self.descriptionViewWeb.isHidden = true
                    self.descTex.isHidden = false
                    self.descTex.text = htmldetail
                }
            
                self.MyQtyDetails = (responseData?.maping.respdata[0].qtyDetails)!
                self.colourheightTable.constant = CGFloat(self.MyQtyDetails.count * 50)
                self.descriptionViewWeb.loadHTMLString(htmldetail, baseURL: nil)
                self.DetailedImageDisplay.reloadData()
                self.priceCollectionView.reloadData()
                
                if self.MyQtyDetails.count == 1 {
                   self.isColourDetalAvailable = "false"
                 //  self.colourheightTable.constant = 50
                   self.colourTableView.reloadData()
                }
                else
                {
                 self.isColourDetalAvailable = ""
                 self.colourTableView.reloadData()
            }
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
        
        
        
    }
    
    func fetchDisplayDetails() {

    }
    
    @IBAction func nosizeAction(_ sender: Any) {
    
    }
    
    @IBAction func BuyNow(_ sender: Any) {
       
        if((GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as? Int)) == nil {
            let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.present(vc, animated: true, completion: nil)
        }
        
        else
        
        {
            buyingProcess(arrayValue)
        }
    }

    func GstProcess(_ array: [String]) {
       
         qtyElement = textfieldarray.first!
         let myFinalGst = qtyElement
    }
   
    
    /// ADD TO CART
    func process(_ array: [String]){
        orderedType = "AddToCart"
        if arrayValue.isEmpty {
            self.showToastMessageTop(message: "Choose Colour")
            print(arrayValue)
            return
        }
        
        if textfieldarray.isEmpty {
            self.showToastMessageTop(message: "Choose quantity")
            print(textfieldarray)
            return
        }
    
        element = arrayValue.first!
        qtyElement = textfieldarray.first!
         spuElement = spuArray.first!
        let url:String = BASE_URL + "customerB2B/addToCartItem"
        guess = Int(qtyElement)
       
        var MyPriceOfQty : Int!
        
        // LOOP FOR PRICE DETAILS
        if (qtyElement <= String(mrpriceList[0].pUFrom)) {
            MyPriceOfQty = Int(mrpriceList[0].pQPrice)
            print(MyPriceOfQty)
        }
        else if (qtyElement <= String(mrpriceList[1].pUFrom))  {
            MyPriceOfQty = Int(mrpriceList[1].pQPrice)
            print(MyPriceOfQty)
        }
        else if (qtyElement <= String(mrpriceList[2].pUFrom))  {
            MyPriceOfQty = Int(mrpriceList[2].pQPrice)
            print(MyPriceOfQty)
        }
        else  {
            MyPriceOfQty = Int(mrpriceList[3].pQPrice)
            print(MyPriceOfQty)
        }
     
        resultO = guess * MyPriceOfQty
        
        let myGst =  gst! * resultO / 100
        let smyGst = sgst! * resultO / 100
        let imyGst = igst! * resultO / 100
        let MyOrderValue = resultO + imyGst
      
        let userid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken)! as! Int
        let myuserid = String(userid)
        
        let params:[String:AnyObject] = [
            "customerId": myuserid as AnyObject,
            "prodId": PRODID as AnyObject,
            "prodName": NAMEPASSto as AnyObject,
            "prodImage": IMGPASSto as AnyObject,
            "prodQty": qtyElement as AnyObject,
            "prodSize": sizsPassesTo as AnyObject,
            "prodColor" : element as AnyObject,
            "prodPrice": MyPriceOfQty as AnyObject,
            "totalVal":String(resultO) as AnyObject,
            "grandTotal" : String(MyOrderValue) as AnyObject,
            "cgstPer": String(cgst) as AnyObject,
            "sgstPer": String(gst) as AnyObject,
            "igstPer": String(igst) as AnyObject,
            "prodSku": spuElement as AnyObject,
            "cgstVal": String(myGst) as AnyObject,
            "sgstVal": String(smyGst) as AnyObject,
            "igstVal": String(imyGst) as AnyObject,
            "taxTotal": String(igst) as AnyObject
        ]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)            
            let responseData = Mapper<myCartModel>().map(JSONString: content!)
           
            if self.arrayValue.isEmpty
            {
                self.showSuccessToastMessage(message: "Item added to cart")
                let indexPath = IndexPath(row: 0, section: 0)
                let acell = self.colourTableView.cellForRow(at: indexPath) as! coloursTableViewCell
                acell.radioActive.image = #imageLiteral(resourceName: "round")
                self.colourTableView.reloadData()
            }
                
            else {
            self.arrayValue = Array(self.arrayValue.dropFirst())
            self.textfieldarray = Array(self.textfieldarray.dropFirst())
            self.spuArray = Array(self.spuArray.dropFirst())
            }
            
            if self.arrayValue.isEmpty {
                self.showSuccessToastMessage(message: "Item added to cart")
                let indexPath = IndexPath(row: 0, section: 0)
                let acell = self.colourTableView.cellForRow(at: indexPath) as! coloursTableViewCell
                acell.radioActive.image = #imageLiteral(resourceName: "round")
                self.colourTableView.reloadData()
                }
                else {
                    self.process(self.arrayValue)
                    self.process(self.textfieldarray)
                    self.process(self.spuArray)
                    print("one more to go")
            }
        })
    }
    
    
    
    /// BUYNOW API CALLING
    func buyingProcess(_ array: [String]){
    
        orderedType = "BuyNow"
        if arrayValue.isEmpty {
            self.showToastMessageTop(message: "Choose Colour")
            print(arrayValue)
            return
        }
        
        if textfieldarray.isEmpty {
            self.showToastMessageTop(message: "Choose quantity")
            print(textfieldarray)
            return
        }
        
        element = arrayValue.first!
        qtyElement = textfieldarray.first!
        spuElement = spuArray.first!
        let url:String =    BASE_URL + "customerB2B/addBuyNow"
        
        /// Buy now Logic
        var MyPricesOfQty = Int(mrpriceList[0].pQPrice)
        guess = Int(qtyElement)
    
        resultO = guess * MyPricesOfQty
        
        // LOOP FOR PRICE DETAILS
        if (qtyElement <= String(mrpriceList[0].pUFrom)) {
            MyPricesOfQty = Int(mrpriceList[0].pQPrice)
            print(MyPricesOfQty)
            
        }
        else if (qtyElement <= String(mrpriceList[1].pUFrom))  {
            MyPricesOfQty = Int(mrpriceList[1].pQPrice)
            print(MyPricesOfQty)
        }
        else if (qtyElement <= String(mrpriceList[2].pUFrom))  {
            MyPricesOfQty = Int(mrpriceList[2].pQPrice)
            print(MyPricesOfQty)
        }
        else  {
            MyPricesOfQty = Int(mrpriceList[3].pQPrice)
            print(MyPricesOfQty)
        }
        
        let myGst =  gst! * resultO / 100
        let smyGst = sgst! * resultO / 100
        let imyGst = igst! * resultO / 100
        
        let MyOrderValue = resultO + imyGst
      
        
        let userid = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken)! as! Int
        let myuserid = String(userid)
        
        let params:[String:AnyObject] = [
            "userId": myuserid as AnyObject,
            "prodId": PRODID as AnyObject,
            "pname": NAMEPASSto as AnyObject,
            "pimage": IMGPASSto as AnyObject,
            "qty": qtyElement as AnyObject,
            "size": sizsPassesTo as AnyObject,
            "color": element as AnyObject,
            "price": MyPricesOfQty as AnyObject,
            "orderVal": String(MyOrderValue) as AnyObject,
            "total_val": String(resultO) as AnyObject,
            "cgst": String(cgst) as AnyObject,
            "sgst": String(gst) as AnyObject,
            "igst": String(igst) as AnyObject,
            "sku": spuElement as AnyObject,
            "cgst_val": String(myGst) as AnyObject,
            "sgst_val": String(smyGst) as AnyObject,
            "igst_val": String(imyGst) as AnyObject,
            "tax_val": String(igst) as AnyObject,
            "bnId": String(buynowdata) as AnyObject
        ]
        
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<myCartModel>().map(JSONString: content!)
          
            if responseData?.sCode == 200 {
                if self.arrayValue.isEmpty
                   {
                   
                    self.showSuccessToastMessage(message: "Item ready to buy")
                    let indexPath = IndexPath(row: 0, section: 0)
                    let acell = self.colourTableView.cellForRow(at: indexPath) as! coloursTableViewCell
                    acell.radioActive.image = #imageLiteral(resourceName: "round")
                    self.colourTableView.reloadData()
                    self.buynowdata = responseData?.data.bnId
                    self.buyNowProductRelateId = responseData?.data.buyNowProductRelateId
                    let vc =  UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
                    
                    vc.fromWay = "BUYNOW"
                    vc.mybuynowdata = String(self.buynowdata)
                    vc.buyNowProductRelateId = String(self.buyNowProductRelateId)
                    
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    self.arrayValue = Array(self.arrayValue.dropFirst())
                    self.textfieldarray = Array(self.textfieldarray.dropFirst())
                    self.spuArray = Array(self.spuArray.dropFirst())
                     self.buynowdata = responseData?.data.bnId
                }
                
                if self.arrayValue.isEmpty {
                    self.showSuccessToastMessage(message: "Item ready to buy")
                    let indexPath = IndexPath(row: 0, section: 0)
                    let acell = self.colourTableView.cellForRow(at: indexPath) as! coloursTableViewCell
                    acell.radioActive.image = #imageLiteral(resourceName: "round")
                    self.colourTableView.reloadData()
                    self.buynowdata = responseData?.data.bnId
                    self.buyNowProductRelateId = responseData?.data.buyNowProductRelateId
                    let vc =  UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
                    vc.fromWay = "BUYNOW"
                    vc.mybuynowdata = String(self.buynowdata)
                    vc.buyNowProductRelateId = String(self.buyNowProductRelateId)
                    self.present(vc, animated: true, completion: nil)
                }
                else {
                    self.buyingProcess(self.arrayValue)
                    self.buyingProcess(self.textfieldarray)
                    self.buyingProcess(self.spuArray)
                    print("one more to go")
                }
            }
            else {
               self.showToastMessageTop(message: "Internal Server error")
            }
        })
        
    }
    
    func postPayment(){
        
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String =  BASE_URL + "user/order"
        let params:[String:AnyObject] = [
            "userId":       myuserid as AnyObject,
            "orderId":  orderId as AnyObject,
            "pTxnRefNo":   TXNID as AnyObject,
            "pTxnRespCode": 01 as AnyObject,
            "pTxnRespMsg":   RESPMSG as AnyObject,
            "GATEWAYNAME":  GATEWAYNAME as AnyObject,
            "pBankName":    BANKNAME as AnyObject,
            "paymentMode": PAYMENTMODE as AnyObject,
            "paymentAmt":    txnamt as AnyObject,
            "pBankTxnRefNo": BANKTXNID as AnyObject
        ]
        print(url)
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<prepaymentMapper>().map(JSONString: content!)
            
            if(responseData?.sCode == 200)
            {
                
            print("++++++++++++++ succes +++++++++++++++")
                
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
        }
        })
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    ///MARK:-  CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
        if collectionView == DetailedImageDisplay {
             return productsImageDArray.count
        }
        else if collectionView == sizesCollectionView {
            
           return MyQtyDetails.count
        }
        else {
           return mrpriceList.count
        }
    }
    @IBAction func closel(_ sender: Any) {
        if fromtype == "home" {
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
            self.present(vc, animated: true, completion: nil)
        }
        else if fromtype == "search" {
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BuyersPickViewController") as! BuyersPickViewController
            self.present(vc, animated: true, completion: nil)
        }
        else {
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
            vc.categoryId = StrCID
            vc.categoryName = categname
            self.present(vc, animated: true, completion: nil)
        }
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == DetailedImageDisplay {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailedCollectionViewCell", for: indexPath) as! DetailedCollectionViewCell
            
            let BannerImageUrl = productsImageDArray[indexPath.row].pImgURL
            let  BannerProductURL  = img_URL + BannerImageUrl!
          
            cell.detailedImages.setImageWithUrl(imageURl: BannerProductURL)
            cell.backgroundColor = UIColor.white
            cell.layer.shadowOpacity = 0.7
            cell.layer.shadowRadius = 5.0
            return cell
        }
        else if collectionView == sizesCollectionView {
            
             let cellSize = collectionView.dequeueReusableCell(withReuseIdentifier: "DetailsSizesCollectionViewCell", for: indexPath) as! DetailsSizesCollectionViewCell

            if(MyQtyDetails[indexPath.row].pSize is NSNull) {
                 cellSize.sizes.text = "NA"
            }
            else if(MyQtyDetails[indexPath.row].pSize == nil){
               
                cellSize.sizes.text = "NA"
            
            }
            else if(MyQtyDetails[indexPath.row].pSize == ""){
            
                cellSize.sizes.text = "NA"
            
            }
            else if(MyQtyDetails[indexPath.row].pSize == " "){
            
                cellSize.sizes.text = "NA"
            
            }
            else {
            
                cellSize.sizes.text = MyQtyDetails[indexPath.row].pSize
            }
            
                return cellSize
        
        }
            
        else
        {
            let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "colourCollectionViewCell", for: indexPath) as! colourCollectionViewCell
            cells.priFrom.text = String(Int(Double(mrpriceList[indexPath.row].pUFrom!)))
            cells.prito.text = String(Int(Double(mrpriceList[indexPath.row].pUTo!)))
            cells.priceof.text = "₹  " + String(Int(Double(mrpriceList[indexPath.row].pQPrice!)))
            cells.enabledView.backgroundColor = UIColor.white
            return cells
    }
    }
    
    func nullToNil(value : AnyObject?) -> AnyObject? {
        if value is NSNull {
            return nil
        } else {
            return value
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == DetailedImageDisplay {
        let cell:DetailedCollectionViewCell = cell as! DetailedCollectionViewCell
            cell.collectionViewDisplayAnimation()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == DetailedImageDisplay {
        
 }
  else if collectionView == sizesCollectionView {

        let cellvariety = collectionView.cellForItem(at: indexPath) as! DetailsSizesCollectionViewCell
        sizeIndexPath = indexPath.row

        if MyQtyDetails[indexPath.row].pSize == nil {
            showToastMessageTop(message: "No colours Available")
        }
        
        colourTableView.isHidden = false
        
        self.colourTableView.reloadData()
        
        if MyQtyDetails.count == 0 {
           colourheightTable.constant = 0
           hideQty()
        }
        else if MyQtyDetails.count == 1 {
            colourheightTable.constant = colourTableView.contentSize.height
        }
        else {
           colourheightTable.constant = colourTableView.contentSize.height
        }
        }
 
        else {
        let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "colourCollectionViewCell", for: indexPath) as! colourCollectionViewCell
       cells.enabledView.isHidden = false
       print(qtyToPassTextField)
       qtyValue.text = qtyToPassTextField
        }
    }
    
    // Tableview Delegates

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        let acell = tableView.dequeueReusableCell(withIdentifier:"coloursTableViewCell", for: indexPath) as! coloursTableViewCell
        
        if sizeIndexPath == nil {
            colourcomaRaw = MyQtyDetails[0].pColor
            qtyCommaRaw = MyQtyDetails[indexPath.row].pQty
            fullNameArr = colourcomaRaw.components(separatedBy: ",")
            qtynameArr = qtyCommaRaw.components(separatedBy: ",")
            acell.colours.text = MyQtyDetails[indexPath.row].pColor
        }
        else if MyQtyDetails.count == 0 {
          acell.colours.text = "Random Colours"
        }
        else
        {
         acell.colours.text = MyQtyDetails[indexPath.row].pColor
         acell.availableItems.text = "( " + "\(whAvailabilityArray[indexPath.row].availQty!)" + " Available )"
        }
         acell.colours.text = MyQtyDetails[indexPath.row].pColor
         acell.changeValue.text = changeValues
         acell.availableItems.text = "( " + "\(whAvailabilityArray[indexPath.row].availQty!)" + " Available )"
      return acell
        
    }
    
     func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
     func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.contentView.backgroundColor = UIColor.white
        cell?.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
            return MyQtyDetails.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let acell = tableView.cellForRow(at: indexPath) as! coloursTableViewCell
        
        colourPssesTo = MyQtyDetails[indexPath.row].pColor
        let skuIs = MyQtyDetails[indexPath.row].prodSku
        while arrayValue.contains("") {
            if let itemToRemoveIndex = arrayValue.index(of: "") {
                arrayValue.remove(at: itemToRemoveIndex)
                print(arrayValue)
                print(textfieldarray)
            }
        }
        
        let itemToRemove = colourPssesTo
        
        if acell.radioActive.image == #imageLiteral(resourceName: "round") {
            
            if acell.changeValue.text == "1" {
               self.showToastMessageTop(message: "Choose a minimum order quantity first")
            }
                
            else {
                acell.radioActive.image = #imageLiteral(resourceName: "roundFill")
               
                if acell.changeValue.text! < "\(whAvailabilityArray[indexPath.row].availQty!)" {
                    
                    acell.changeValue.text = "\(whAvailabilityArray[indexPath.row].availQty!)"
                    
                    self.showToastMessageTop(message: "only " + "\(whAvailabilityArray[indexPath.row].availQty!)" + "  Available")
                   
                    let aValue = colourPssesTo
                    arrayValue.insert(aValue, at: indexPath.row)
                   
                    let Csku = skuIs
                    spuArray.append(Csku!)
                    
                    let bvalue = acell.changeValue.text
                    textfieldarray.insert(bvalue!, at: indexPath.row)
                   
                    print(textfieldarray)
                    print(arrayValue)
                    print(spuArray)
                }
                    
              else
        {
               let aValue = colourPssesTo
               arrayValue.insert(aValue, at: indexPath.row)
            
               let bvalue = acell.changeValue.text
               textfieldarray.insert(bvalue!, at: indexPath.row)
            
               let Csku = skuIs
               spuArray.append(Csku!)
            
               print(textfieldarray)
               print(arrayValue)
        }
        }
        }
            
        else {
           
            acell.radioActive.image = #imageLiteral(resourceName: "round")
            acell.changeValue.text = String(mrpriceList[0].pUFrom)
            while arrayValue.contains(itemToRemove) {
                if let itemToRemoveIndex = arrayValue.index(of: itemToRemove) {
                    arrayValue.remove(at: itemToRemoveIndex)
                    textfieldarray.remove(at: itemToRemoveIndex)
                    spuArray.remove(at: itemToRemoveIndex)
                    print(textfieldarray)
                    print(arrayValue)
                    print(spuArray)
                }
            }
        }
      
//        if sizeIndexPath == nil {
//
//            tabselect = true
//            colourPssesTo = MyQtyDetails[indexPath.row].pColor
//
//            let aValue = colourPssesTo
//            arrayValue.insert(aValue, at: indexPath.row)
//
//            let bvalue = acell.changeValue.text
//            textfieldarray.insert(bvalue!, at: indexPath.row)
//
//            let spuvalue = MyQtyDetails[indexPath.row].prodSku
//            spuArray.append(spuvalue!)
//            print(arrayValue)
//
//        } else
//        {
//            tabselect = true
//            colourPssesTo = fullNameArr[indexPath.row]
//        }
    }

    func signInButtonTapped() {
     //   NSIndexPath(forRow: 0, inSection: 0)
        let indexpathForEmail = NSIndexPath(row: 0, section: 0)
        let emailCell = colourTableView.cellForRow(at: indexpathForEmail as IndexPath)! as! coloursTableViewCell
        
        let indexpathForPass = NSIndexPath(row: 0, section: 0)
        let passCell =  colourTableView.cellForRow(at: indexpathForPass as IndexPath)! as! coloursTableViewCell
        
        if emailCell.changeValue.placeholder!.isEmpty || passCell.changeValue.placeholder!.isEmpty {
            
            let alert = UIAlertController(title: "Alert", message: "all fields are required to be filled in", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
// MARK :- PAYTM INTEGRATION DELEGATE +++++++++++++   PAYTM DELEGATES    +++++++++++++++++


func didCancelTrasaction(_ controller: PGTransactionViewController!) {
    controller.dismiss(animated: true) {
        let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
        self.present(vc, animated: true, completion: nil)
    }
}

func didFinishedResponse(_ controller: PGTransactionViewController!, response responseString: String!) {

    let msg  = responseString
    let dict = convertToDictionary(text: msg!)
    STATUS = dict!["STATUS"]! as! String
    
    if (STATUS == "TXN_FAILURE") {
        STATUS = "Failure"
        controller.dismiss(animated: true) {
            self.showAlert(title: "Whoops", message: "Your transaction cancelled !! please try again")
        }
    }
    else {
        PAYMENTMODE = dict!["PAYMENTMODE"]! as! String
        BANKNAME = dict!["BANKNAME"]! as! String
        RESPMSG = dict!["RESPMSG"]! as! String
        GATEWAYNAME = dict!["GATEWAYNAME"]! as! String
        let ttxnamount  = dict!["TXNAMOUNT"]! as! String
        let tTXNID = dict!["TXNID"]! as! String
        let bBANKTXNID = dict!["BANKTXNID"]! as! String
        payDate = dict!["TXNDATE"]! as! String
        txnamount = Int(ttxnamount)
        TXNID = Int(tTXNID)
        BANKTXNID = Int(bBANKTXNID)
        STATUS = "Success"
    }
    postPayment()
    controller.dismiss(animated: true) {
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
        vc.msg = dict!["TXNID"]! as! String
        self.present(vc, animated: true, completion: nil)
    }
}

func errorMisssingParameter(_ controller: PGTransactionViewController!, error: Error!) {
    controller.dismiss(animated: true) {
        self.showAlert(title: "didCancelTrasaction", message: error.localizedDescription)
        let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
        self.present(vc, animated: true, completion: nil)
}
    }

func didFailTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
    controller.dismiss(animated: true) {
        self.showAlert(title: "didCancelTrasaction", message: error.localizedDescription)
        let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
        self.present(vc, animated: true, completion: nil)
    }
}
    
func didCancelTransaction(_ controller: PGTransactionViewController!, error: Error!, response: [AnyHashable : Any]!) {
    showAlert(title: "Transaction Cancelled", message: error.localizedDescription)
    let vc =  UIStoryboard(name: "Alert", bundle: nil).instantiateViewController(withIdentifier: "CSWhoopsView") as! CSAlertViewController
    
    self.present(vc, animated: true, completion: nil)
    
}

func didFinishCASTransaction(_ controller: PGTransactionViewController!, response: [AnyHashable : Any]!) {
    print("my response isis :" )
    print(response)
    showAlert(title: "cas", message: "")
    
}

func didSucceedTransaction(controller: PGTransactionViewController, response: [AnyHashable : Any])  {
    print("my response is :" )
    print(response)
    showAlert(title: "Transaction Successfull", message: NSString.localizedStringWithFormat("Response- %@", response) as String)
    let msg: String = "Your order was completed successfully.\n Rs. \(response["TXNAMOUNT"]!)"
    print(msg)
    self.showToastMessageTop(message: "Thank You for Payment" + msg)
    
    postPayment()
   
    controller.dismiss(animated: true) {
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "confirmViewController") as! confirmViewController
        vc.msg = msg
        self.present(vc, animated: true, completion: nil)
    }
}
}

extension String{
    func convertHtml() -> NSAttributedString{
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}
