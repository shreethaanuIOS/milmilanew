//
//  coloursTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 12/21/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class coloursTableViewCell: CSBaseTableViewCell {

    typealias ButtonHandler = (coloursTableViewCell) -> Void

    @IBOutlet weak var availableItems: UILabel!
    @IBOutlet weak var minus: UIButton!
    @IBOutlet weak var plus: UIButton!
    @IBOutlet weak var colours: UILabel!
    @IBOutlet weak var changeValue: UITextField!
    @IBOutlet weak var radioActive: UIImageView!
    
    var incrementHandler: ButtonHandler?
    var decrementHandler: ButtonHandler?
    var mymoqvalue: Int!
    
    @IBAction func subtraction(_ sender: Any) {
        if (Int(changeValue.text!)! > 1) {
            changeValue.text = String(Int(changeValue.text!)! - 3)
        }
        else {
            changeValue.text = "1"
        }
    }
    
    @IBAction func addition(_ sender: Any) {
        
        if (Int(changeValue.text!)! > 0) {
            changeValue.text = String(Int(changeValue.text!)! + 3)
        }
        else {
            changeValue.text = "1"
        }
    }

    func configureWithValue(value: UInt, incrementHandler: ButtonHandler?, decrementHandler: ButtonHandler?) {
        changeValue.text = String(value)
        self.incrementHandler = incrementHandler
        self.decrementHandler = decrementHandler
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
