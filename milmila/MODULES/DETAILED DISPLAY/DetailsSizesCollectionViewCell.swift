//
//  DetailsSizesCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 12/20/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class DetailsSizesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var sizes: UILabel!
}
