//
//  DetailedCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 11/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class DetailedCollectionViewCell: CSBaseCollectionViewCell {
    
    @IBOutlet weak var detailedImages: UIImageView!
}
