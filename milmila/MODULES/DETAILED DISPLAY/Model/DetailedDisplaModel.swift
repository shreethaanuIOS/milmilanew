
/*
 * DetailedDisplaModel
 * This class is display page model (object mapper class for cart response)
 */

import UIKit
import ObjectMapper

class DetailedDisplaModel: Mappable {
    var maping : mapzD!
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        maping <- map["map"]
        
    }
}

class mapzD : DetailedDisplaModel{
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata :  [DisplayCateObjs]!
    
    
    override func mapping( map: Map) {
        sCode <- map["scode"]
        error <- map["error"]
        respdata <- map["data"]
        msg <- map["msg"]
    }
}

class DisplayCateObjs : mapzD {
    
    var pid: Int!
    var pname:String!
    var model:String!
    var price:Int?
    var sku : String!
    var colorAvail:String!
    var colors:String!
    var sizeAvail:String!
    var size : String!
    var weight:String!
    var height:String!
    var length:String!
    var imgURL : String!
    var moq : Int?
    var priceType : String!
    var cDetails : [CdisplayDetails]!
    var imgDetails : [imgDetails]!
    var descDetails : [descDetails]!
    var wsDetails : [wsDetails]!
    var qtyDetails : [qtyDetails]!
    var isSizeAvail : String!
    var whAvailability : [Availability]!
    
    override func mapping( map: Map) {
        // direct conversion
        
        pid     <- map["pid"]
        pname   <- map["pname"]
        model      <- map["model"]
        sku     <- map["sku"]
        price   <- map["price"]
        colorAvail       <- map["colorAvail"]
        colors          <- map["colors"]
        sizeAvail      <- map["sizeAvail"]
        size          <- map["size"]
        weight          <- map["weight"]
        height          <- map["height"]
        length          <- map["length"]
        moq             <- map["moq"]
        priceType       <- map["price_type"]
        cDetails        <- map["cDetails"]
        descDetails     <- map["descDetails"]
        wsDetails        <- map["wsDetails"]
        qtyDetails      <- map["qtyDetails"]
        isSizeAvail      <- map["isSizeAvail"]
        imgDetails        <- map["imgDetails"]
        whAvailability <- map["whAvailability"]
    }
}

class CdisplayDetails :DisplayCateObjs {
    var cid: Int!
    var cCode:String!
    var cName:String!
    var sOrder:Int?
    override func mapping( map: Map) {
        // direct conversion
        
        cid     <- map["cid"]
        cCode   <- map["cCode"]
        cName      <- map["cName"]
        sOrder     <- map["sOrder"]
}
}
class imgDetails : DisplayCateObjs {
    var pImgURL: String!
    var sOrder:Int!
    override func mapping( map: Map) {
        // direct conversion
        
        pImgURL     <- map["pImgURL"]
        sOrder   <- map["sOrder"]
    }
}

class descDetails : DisplayCateObjs {
    var sDesc: String!
    var pDesc:String!
    var seoLink:String!
    var prod_feature: String!
    
    override func mapping( map: Map) {
        // direct conversion
        seoLink <- map["seoLink"]
        sDesc     <- map["sDesc"]
        pDesc   <- map["pDesc"]
        prod_feature <- map["prod_feature"]
    }
    
}

class wsDetails : DisplayCateObjs {
    var prodSkuId : String!
    var qtyPriceDetails : [qtyPriceDetails]!
    
    override func mapping( map: Map) {
        // direct conversion
        prodSkuId <- map["prodSkuId"]
        qtyPriceDetails <- map["qtyPriceDetails"]
        
    }
}

//class wsDetailsMapping : wsDetails {
//
//    }
//}

//class qtyPriceDetails : wsDetailsMapping {
//    var myArrayList : [myArrayList]!
//    override func mapping( map: Map) {
//        // direct conversion
//        myArrayList <- map["myArrayList"]
//
//}
//}

//class myArrayList : qtyPriceDetails {
//    var myarraylistMap: MyArrayListMapping!
//
//
//    override func mapping( map: Map) {
//        // direct conversion
//
//        myarraylistMap <- map["map"]
//    }
//
//}

class Availability : DisplayCateObjs {

   var warehouseId: Int?
   var wName: String!
   var wPinCode: String!
   var psku: String!
   var prodQty: Int?
   var soldQty: Int?
   var availQty: Int?
   var warehouseProductRelateId: Int?

        override func mapping( map: Map) {
         
            warehouseId <- map["warehouseId"]
            wName <- map["wName"]
            wPinCode <- map["wPinCode"]
            psku <- map["psku"]
            prodQty <- map["prodQty"]
            soldQty <- map["soldQty"]
            availQty <- map["availQty"]
            warehouseProductRelateId <- map["warehouseProductRelateId"]
        
    }
    }
//whAvailability

class qtyPriceDetails : wsDetails {
    var pUFrom: Int!
    var pUTo:Int!
    var pQPrice:Int!
    var showPopUp: String!
    var productsSku: String!
    var pUType: String!
   
    override func mapping( map: Map) {
        // direct conversion
        
         pUFrom <- map["pUFrom"]
         pUTo <- map["pUTo"]
         pQPrice <- map["pQPrice"]
         showPopUp <- map["showPopUp"]
         productsSku <- map["prodSku"]
         pUType <- map["pUType"]
    }
    
}

class qtyDetails: DisplayCateObjs {
    var prodSku : String!
    var pColor : String!
    var pSize : String!
    var pQty : String!
    override func mapping( map: Map) {
        // direct conversion
        
        prodSku <- map["prodSku"]
        pColor <- map["pColor"]
        pSize <- map["pSize"]
        pQty <- map["pQty"]
    }
}

/*

 {
 "map": {
 "msg": "Successfull",
 "data": [
 {
 "pid": 2395,
 "pname": "New USB bag fashion crossbody bag",
 "model": "070400015",
 "sku": "07040001501,07040001502,07040001503",
 "spu": "070400015",
 "price": 461,
 "colorAvail": "N",
 "colors": null,
 "sizeAvail": "N",
 "size": null,
 "weight": 0,
 "height": 0,
 "length": 0,
 "breadth": 0,
 "moq": 3,
 "price_type": 1,
 "isSizeAvail": "N",
 "sellType": null,
 "imgDetails": [
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/anti-theft-usb-single-sling-bag-jegadeshsimadere-1711-11-F607763_1.jpg",
 "sOrder": 1
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/anti-theft-usb-single-sling-bag-jegadeshsimadere-1711-11-F607763_2.jpg",
 "sOrder": 2
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/anti-theft-usb-single-sling-bag-jegadeshsimadere-1711-11-F607763_3.jpg",
 "sOrder": 3
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/anti-theft-usb-single-sling-bag-jegadeshsimadere-1711-11-F607763_4.jpg",
 "sOrder": 4
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/PA4116GY-1-4481-a0wL.jpg",
 "sOrder": 5
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/PA4116GY-1-4481-VT1n.jpg",
 "sOrder": 6
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/PA4116GY-1-4481-vXdg.jpg",
 "sOrder": 7
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/ghimg (4).jpg",
 "sOrder": 8
 },
 {
 "pImgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/C0208/ghimg (5).jpg",
 "sOrder": 9
 }
 ],
 "descDetails": [
 {
 "sDesc": "",
 "pDesc": "<table class=\"table table-bordered\" style=\"width: 577px; margin-bottom: 20px;\"><tbody><tr><td style=\"line-height: 1.42857;\">Item Type<br></td><td style=\"line-height: 1.42857;\">Backpacks<br></td><td style=\"line-height: 1.42857;\"><p>Handle/Strap Type</p></td><td style=\"line-height: 1.42857;\">Single Strap/Cross body</td></tr><tr><td style=\"line-height: 1.42857;\">Backpacks Type<br></td><td style=\"line-height: 1.42857;\">Softback<br></td><td style=\"line-height: 1.42857;\"><p>Lining Material</p></td><td style=\"line-height: 1.42857;\">Polyester<br></td></tr><tr><td style=\"line-height: 1.42857;\">Gender<br></td><td style=\"line-height: 1.42857;\">Female,Ladies,Women,Unisex,Men<br></td><td style=\"line-height: 1.42857;\"><p>Closure Type</p></td><td style=\"line-height: 1.42857;\">Zipper and&nbsp; Velcro tape<br></td></tr><tr><td style=\"line-height: 1.42857;\">Interior<br></td><td style=\"line-height: 1.42857;\">Interior Compartment,Computer Inter-layer,Cell<br></td><td style=\"line-height: 1.42857;\"><p>Carrying System</p></td><td style=\"line-height: 1.42857;\">Resin Mesh<br></td></tr><tr><td style=\"line-height: 1.42857;\">Exterior<br></td><td style=\"line-height: 1.42857;\">Silt Pocket<br></td><td style=\"line-height: 1.42857;\">Style<br></td><td style=\"line-height: 1.42857;\">Travel backpacks,Leisure backpack,Business Bag<br></td></tr><tr><td style=\"line-height: 1.42857;\">Technics<br></td><td style=\"line-height: 1.42857;\">Jacquard<br></td><td style=\"line-height: 1.42857;\">Features<br></td><td style=\"line-height: 1.42857;\">Rechargeable Shoulder Bag<br></td></tr><tr><td style=\"line-height: 1.42857;\">Capacity<br></td><td style=\"line-height: 1.42857;\">20-35 Liter<br></td><td style=\"line-height: 1.42857;\"><p>Popular element</p></td><td style=\"line-height: 1.42857;\">Charging function<br><br></td></tr></tbody></table>",
 "seoLink": "",
 "prod_feature": "<ul style=\"margin-right: 0px; margin-bottom: 0px; margin-left: 0px; list-style: none; color: rgb(40, 40, 40); font-family: micro; font-size: medium; padding: 0px 0px 0px 13px !important;\"><li class=\"MsoNormal\" style=\"margin: 0px; padding: 10px 25px 0px 0px; list-style: square; float: left; width: 890.625px; font-size: 12px;\">Hidden zippers at the back preventing attempts at unauthorized opening</li><li class=\"MsoNormal\" style=\"margin: 0px; padding: 10px 25px 0px 0px; list-style: square; float: left; width: 890.625px; font-size: 12px;\">Built in USB charging port with cable</li><li class=\"MsoNormal\" style=\"margin: 0px; padding: 10px 25px 0px 0px; list-style: square; float: left; width: 890.625px; font-size: 12px;\">Wide padded shoulder straps for comfort and a convenient slide out handle</li><li class=\"MsoNormal\" style=\"margin: 0px; padding: 10px 25px 0px 0px; list-style: square; float: left; width: 890.625px; font-size: 12px;\">Water resistant</li></ul><p style=\"margin-bottom: 9px; padding: 0px; list-style: none; color: rgb(40, 40, 40); font-size: 12px; float: left; width: 903.611px; font-family: micro;\"></p><p style=\"margin-bottom: 10px; padding: 0px; list-style: none; color: rgb(40, 40, 40); font-size: 12px; float: left; width: 903.611px; font-family: micro;\">Unique and innovatively designed, this USB charging anti-theft smart laptop backpack keeps your expensive laptop fully secure. Ergonomically designed to be comfortable to carry and with a number of internal zippered mesh compartments, this smart backpack is made of a combination of super tough ballistic nylon and hardy polyurethane for long life, light weight and ease of use.</p>",
 "videoLink": ""
 }
 ],
 "wsDetails": [
 {
 "prodSkuId": "07040001501",
 "qtyPriceDetails": [
 {
 "prodSku": "07040001501",
 "pUFrom": 3,
 "pUTo": 29,
 "pQPrice": 461,
 "showPopUp": "N"
 },
 {
 "prodSku": "07040001501",
 "pUFrom": 30,
 "pUTo": 99,
 "pQPrice": 449,
 "showPopUp": "N"
 },
 {
 "prodSku": "07040001501",
 "pUFrom": 100,
 "pUTo": 999,
 "pQPrice": 432,
 "showPopUp": "N"
 },
 {
 "prodSku": "07040001501",
 "pUFrom": 1000,
 "pUTo": 0,
 "pQPrice": 407,
 "showPopUp": "N"
 }
 ]
 }
 ],
 "qtyDetails": [
 {
 "prodSku": "07040001501",
 "pColor": "Grey",
 "pQty": "50"
 },
 {
 "prodSku": "07040001502",
 "pColor": "Blue",
 "pQty": "50"
 },
 {
 "prodSku": "07040001503",
 "pColor": "Black",
 "pQty": "50"
 }
 ],
 "additionalProperties": {},
 "whAvailability": [
 {
 "prodSkuId": "07040001502",
 "availQty": 17,
 "wareHouse": [
 {
 "wPinCode": "560048",
 "wName": "Bangalore",
 "warehouseId": 3
 }
 ]
 },
 {
 "prodSkuId": "07040001503",
 "availQty": 20,
 "wareHouse": [
 {
 "wPinCode": "560048",
 "wName": "Bangalore",
 "warehouseId": 3
 }
 ]
 },
 {
 "prodSkuId": "07040001501",
 "availQty": 20,
 "wareHouse": [
 {
 "wPinCode": "560048",
 "wName": "Bangalore",
 "warehouseId": 3
 }
 ]
 }
 ],
 "cDetails": []
 }
 ],
 "error": null,
 "status": true,
 "additionalProperties": {},
 "scode": 200
 },
 "additionalProperties": {}
 }
 
 */
