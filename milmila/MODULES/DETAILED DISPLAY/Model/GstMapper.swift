//
//  GstMapper.swift
//  milmila
//
//  Created by prasun sarkar on 1/24/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class gstMapper: Mappable {
   
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var dataS: dataZI!
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {        
        sCode <- map["sCode"]
        error <- map["error"]
        //msg <- map["data"]
        status <- map["status"]
        dataS <- map["data"]
    }
}

class dataZI: gstMapper {
   
    var baseData: baseData!
    var taxId: Int?
    var gst: Int?
    var sgst: Int?
    var igst: Int?
    var isActive: String?
    var isDefault: String?
    
    override func mapping(map: Map) {
        
        baseData <- map["baseData"]
        taxId <- map["taxId"]
        gst <- map["gst"]
        sgst <- map["sgst"]
        igst <- map["igst"]
        isActive <- map["isActive"]
        isDefault <- map["isDefault"]
    }
}
/*
 {
 "msg": "Successfull",
 "sCode": 200,
 "data": {
 "baseData": {},
 "taxId": 1,
 "gst": 6,
 "sgst": 6,
 "igst": 12,
 "isActive": "A",
 "isDefault": "Y"
 },
 "error": null,
 "status": true
 }
 
 */

