//
//  buynow.swift
//  milmila
//
//  Created by prasun sarkar on 1/11/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class buynowmapper: Mappable {
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var data: Int?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        data <- map["data"]
    }
}

/*
 
 {"msg":"Product Information Added Successfully..","sCode":200,"data":98,"error":false,"status":true}
 
 */
