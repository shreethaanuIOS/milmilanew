//
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//


import UIKit
import ObjectMapper


class productsDisplayViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var TitleOFpage: UILabel!
    @IBOutlet weak var productsdisplay: UICollectionView!
    var gridLayout: GridLayout!
   var homeProductsListingArray = [homeprodList]()
    var httpRequest = CSSwiftApiHttpRequest()
    var categoryId = ""
    var categoryName = ""
    var fromtype = ""
    var productListArray = [mapz]()
    var productslist = [prodObj]()
    var catid : Int!
    
    @IBOutlet weak var bottomBar: UIView!
    @IBOutlet weak var detailImg: UIImageView!
    @IBOutlet weak var detailedDescriptionView: UIView!
    @IBOutlet weak var detailedWholeView: UIView!
    @IBOutlet weak var Detaildetails: UILabel!
    @IBOutlet weak var DetailPrice: UILabel!
    @IBOutlet weak var detailName: UILabel!
    
    
    
    
    private let leftAndRightPaddings: CGFloat = 15.0
    private let numberOfItemsPerRow: CGFloat = 3.0
    
    var imgname = ""
    var pname = ""
    var pprice = ""
    var desc = ""
    var prodId = ""
    var myprice = ""
    var prodQty = ""
    var imgsname = ""
    
    @IBOutlet weak var shareIcon: UIButton!
    @IBOutlet weak var cartIcon: UIButton!
    @IBOutlet weak var favIcon: UIButton!
    
    @IBOutlet weak var LeftmenuTable: UITableView!
    @IBOutlet weak var rightTable: UITableView!
    @IBOutlet weak var filterClear: UIButton!
    @IBOutlet weak var filterApply: UIButton!
    @IBOutlet weak var closeFlter: UIButton!
    
    @IBOutlet weak var filterView: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
       filterView.isHidden = true
        rightTable.isHidden = true
       
        productsdisplay.dataSource = self
        productsdisplay.backgroundColor = UIColor.myLightGrayColour
        TitleOFpage.text = categoryName
        print(categoryId)
        ProductsDisplay()
        
        // GRID LAYOUT
        
        gridLayout = GridLayout()
        productsdisplay.setCollectionViewLayout(gridLayout, animated: true)
        // homeproductslisting()
        
        detailedWholeView.isHidden = true
        detailedDescriptionView.isHidden = true
        
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
        }
        else{
         //   fabIcon()
            // Do any additional setup after loading the view.
        }
    }

    /// MARK:- Button Action
    
    @IBAction func closeDetailView(_ sender: Any) {
     detailedWholeView.isHidden = true
        detailedDescriptionView.isHidden = true
       detailedDescriptionView.closeview()
        
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailProductDisplayViewController") as! DetailProductDisplayViewController
        vc.IMGPASSto = imgname
        vc.NAMEPASSto = desc
        vc.PRICEPASSto = pprice
        vc.DESCRIPTIONPASSto = desc
        vc.IMAGEARRAYTOPASS = imgsname
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func favAction(_ sender: Any) {
    favIcon.setImage(#imageLiteral(resourceName: "like_inactive"), for: .normal)
    }
    
    /// Mark:- Functions
    func detailViewActions(){
    detailedWholeView.isHidden = false
    detailedDescriptionView.isHidden = false
    detailedDescriptionView.springAnimation()
    }
    
    @IBAction func cartAction(_ sender: Any) {
        if(GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
            let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        } else {
            addToCart()
        }
        }
    
    /// MARK :- API CALL
    
    func ProductsDisplay()
    {
        let url:String = BASE_URL + "products/list?c=" + categoryId 
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in

            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<prductsmapper>().map(JSONString: content!)
            if(responseData?.maping.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
            
                self.productslist = (responseData?.maping.respdata.prodObj)!
              //  self.homeProductsListingArray = (responseData?.maping.respdata.cateObj)!
             //   self.TitleOFpage.text = (responseData?.maping.respdata.cateObj.cate_name)
                self.productsdisplay.reloadData()
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")

               print("------------ failed --------------")
            }
        })
    }
    
    @IBAction func closeFilterAction(_ sender: Any) {
        filterView.isHidden = true
        filterView.springAnimation()
        bottomBar.isHidden = false
        
    }
    
    @IBAction func filterAction(_ sender: Any) {
        filterView.isHidden = false
        filterView.springAnimation()
        bottomBar.isHidden = true
    }
    
    
    func addToCart() {
        let url:String = BASE_URL + "/user/addToCart"
        //
        //        let categoryId:Int! = GlobalConstants.General.USERDEFAULTS.value(forKey: GlobalConstants.Login.CSTeamId) as! Int
        
        let params:[String:String] = [
            "userId":GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken)! as! String,
            "prodId": "1",
            "pname": desc,
            "pimage":   imgname,
            "qty": "500",
            "size"  : "NA",
            "color" : "NA",
            "price": pprice,
            "orderVal" : "356500"
        ]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            let responseData = Mapper<myCartModel>().map(JSONString: content!)
            self.showSuccessToastMessage(message: "Item added to cart")
            print("Item added to cart")
        })
    }
    
    
    ///MARK:-  CollectionView Delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return productslist.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productsdisplay", for: indexPath) as! productsdisplayCollectionViewCell
        var productsimageURL = productslist[indexPath.row].imgURL
        if productsimageURL == nil
        {
            cell.productsimage.image = #imageLiteral(resourceName: "placeholderImage")
        }
        else {
        let fullNameArr = productsimageURL?.components(separatedBy: ",")
        let count = fullNameArr?.count
        print(count)
        let thumbImg = fullNameArr![0]
        let  productURL  = img_URL + thumbImg
        print(productURL)
        cell.productsimage.setImageWithUrl(imageURl: productURL)
        }
        if productslist[indexPath.row].pname == nil {
            cell.productlistname.text = "Product name"
        }
        else {
        cell.productlistname.text = productslist[indexPath.row].pname
        }
        if productslist[indexPath.row].price == nil {
            cell.productPrice.text = "Price"
        }
        else{
        let priceProduct = "\(String(describing: productslist[indexPath.row].price!))"
        cell.productPrice.text = "₹ " + priceProduct
        }
        cell.backgroundColor = UIColor.white
        cell.layer.shadowOpacity = 0.7
        cell.layer.shadowRadius = 5.0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell:productsdisplayCollectionViewCell = cell as! productsdisplayCollectionViewCell
            cell.collectionViewDisplayAnimation()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if productslist[indexPath.row].price == nil {
        
            pprice = "price"
       
        }
        
        else {
          
            pprice = String(Int(Double(productslist[indexPath.row].price!)))
        }
      //  pprice = String(Int(Double(productslist[indexPath.row].price!)))
      
        if productslist[indexPath.row].imgURL == nil {
          detailImg.image = #imageLiteral(resourceName: "placeholderImage")
        }
            
        else {
        
        imgsname = productslist[indexPath.row].imgURL
        let fullNameArr = imgsname.components(separatedBy: ",")
        let count = fullNameArr.count
        print(count)
        imgname = fullNameArr[0]
        let  productURL  = img_URL + imgname
        detailImg.setImageWithUrl(imageURl: productURL)
        
        }
        
        if productslist[indexPath.row].pname == nil {
            desc = "pname"
        }
            
        else{
        
            desc = productslist[indexPath.row].pname

        }
        
        DetailPrice.text = pprice
        detailName.text = desc
        Detaildetails.text = desc
       
        if productslist[indexPath.row].pid == nil {
          prodId = "12"
        }
        else {
        prodId = String(Int(Double(productslist[indexPath.row].pid)))
        }
        detailViewActions()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        let padding: CGFloat =  50
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width:collectionViewSize/2, height:collectionViewSize/2)
        
    }
    
    ///MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == LeftmenuTable {
            return 6
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == LeftmenuTable
        {
         let cell = tableView.dequeueReusableCell(withIdentifier:"filterLeftTableViewCell", for: indexPath) as! filterLeftTableViewCell
            cell.filterItemsView.isHidden = false
            rightTable.isHidden = false
            rightTable.reloadData()
        }
        else {
            let cells = tableView.dequeueReusableCell(withIdentifier:"filterrightTableViewCell", for: indexPath) as! filterrightTableViewCell
                cells.filercontent.textColor = UIColor.myBlueColour
                cells.filterSelection.image = #imageLiteral(resourceName: "like_active")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == LeftmenuTable {
            let cell = tableView.dequeueReusableCell(withIdentifier:"filterLeftTableViewCell", for: indexPath) as! filterLeftTableViewCell
            cell.filterItems.text = "my filter contents"
            cell.filterItemsView.isHidden = true
            return cell
        }
        else {
           let cells = tableView.dequeueReusableCell(withIdentifier:"filterrightTableViewCell", for: indexPath) as! filterrightTableViewCell
            cells.filercontent.text = "Name"
            return cells
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension UIViewController{
   
    
}
extension UIView {
    func springAnimation() {
        self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 1.0,
                       delay: 0,
                       usingSpringWithDamping: 0.2,
                       initialSpringVelocity: 6.0,
                       options: .allowUserInteraction,
                       animations: { [weak self] in
                        self?.transform = .identity
            },
                       completion: nil)
    }
   
    func closeview(){
        UIView.animate(withDuration: 1.0,
                       animations: {
                        self.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
        },
                       completion: { _ in
                        UIView.animate(withDuration: 1.6) {
                            self.transform = CGAffineTransform.identity
                        }
        })
    }
}
