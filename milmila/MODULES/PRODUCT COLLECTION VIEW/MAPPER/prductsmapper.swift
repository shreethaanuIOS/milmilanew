//
//  prductsmapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class prductsmapper: Mappable {
    var maping : mapz!
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        maping <- map["map"]
        
    }
}

class mapz : prductsmapper{
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var respdata :  objects!
    

override func mapping( map: Map) {
sCode <- map["scode"]
error <- map["error"]
respdata <- map["data"]
msg <- map["msg"]
}
}

class  objects: mapz {
  var  cateObj:  cateObj!
    var prodObj :  [prodObj]!
    override func mapping( map: Map) {
        prodObj <- map["cateObj"]
        prodObj <- map["prodObj"]
    
}
}
class  cateObj: objects {

    var cateId: String!
    var parent_cate_id:String!
    var pcateName:String!
    var cate_code:String!
    var cate_name:String!
    var cate_desc : String!
    var cate_image:String!
    var image_width:String!
    var image_height:String!
    var show_in : String!
    var sort_order:String!
    var is_active:String!
    var status:String!
    var create_date : String!
    var create_user:String!
    var modi_date:String!
    var modi_user:String!
    
    override func mapping( map: Map) {
        // direct conversion
        cateId     <- map["cateId"]
        parent_cate_id   <- map["parent_cate_id"]
        pcateName   <- map["pcateName"]
        cate_code      <- map["cate_code"]
        cate_name     <- map["cate_name"]
        cate_desc   <- map["cate_desc"]
        cate_image       <- map["cate_image"]
        image_width          <- map["image_width"]
        image_height      <- map["image_height"]
        show_in          <- map["show_in"]
        sort_order          <- map["sort_order"]
        is_active          <- map["is_active"]
        status          <- map["status"]
        create_date          <- map["create_date"]
        create_user          <- map["create_user"]
        modi_date          <- map["modi_date"]
        modi_user          <- map["modi_user"]
    }
}

class prodObj: objects{
    var pid: Int!
    var pname:String!
    var model:String!
    var price:Int?
    var sku : String!
    var colorAvail:String!
    var colors:String!
    var sizeAvail:String!
    var size : String!
    var weight:String!
    var height:String!
    var length:String!
    var imgURL : String!
    
    override func mapping( map: Map) {
        // direct conversion
   
        pid     <- map["pid"]
        pname   <- map["pname"]
        model      <- map["model"]
        sku     <- map["sku"]
        price   <- map["price"]
        colorAvail       <- map["colorAvail"]
        colors          <- map["colors"]
        sizeAvail      <- map["sizeAvail"]
        size          <- map["size"]
        weight          <- map["weight"]
        height          <- map["height"]
        length          <- map["length"]
        imgURL          <- map["imgURL"]
    }
}

/*
 
 {
 "map": {
 "msg": "Found 164 Product(s) List.",
 "sCode": 200,
 "data": {
 "cateObj": {
 "cateId": 1,
 "parent_cate_id": 0,
 "pcateName": null,
 "cate_code": "A1001",
 "cate_name": "Fashion",
 "cate_desc": "Fashion",
 "cate_image": "",
 "image_width": 200,
 "image_height": 200,
 "show_in": "BOTH",
 "sort_order": 0,
 "is_active": "A",
 "status": 0,
 "create_date": null,
 "create_user": null,
 "modi_date": null,
 "modi_user": null
 },
 "prodObj": [
 {
 "pid": 1,
 "pname": "Sexy cocktail mini-skirt",
 "model": "10001",
 "sku": "10001",
 "price": 380,
 "colorAvail": null,
 "colors": "Red,BLue",
 "sizeAvail": null,
 "size": "M,L",
 "weight": 0,
 "height": 0,
 "length": 0,
 "imgURL": "A1001/10001a.jpg,A1001/10001b.jpg,A1001/10001c.jpg"
 },
 
 */
