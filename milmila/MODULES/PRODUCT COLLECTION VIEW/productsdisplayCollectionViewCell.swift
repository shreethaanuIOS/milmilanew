////
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//


import UIKit

class productsdisplayCollectionViewCell: CSBaseCollectionViewCell {
    
    @IBOutlet weak var productPrice: UILabel!

    @IBOutlet weak var productlistname: UILabel!
    @IBOutlet weak var productsimage: UIImageView!
}
