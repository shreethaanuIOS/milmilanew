//
//  searchMaper.swift
//  milmila
//
//  Created by prasun sarkar on 11/30/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
class searchMaper: Mappable {

    var mapin : mapi!
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        mapin  <- map["map"]
}
}

class mapi : searchMaper {
    var error: Bool?
    var statusCode : Int?
    var message : String?
    var response: mmdata!

    override func mapping(map: Map) {
    error <- map["error"]
    statusCode <- map["scode"]
    response <- map["data"]
    message <- map["msg"]
}
}
class mmdata : mapi {
   var cateObj : String?
    var prodObj : [productsList]!
    
override func mapping(map: Map) {
    cateObj <- map["cateObj"]
    prodObj <- map["prodObj"]
    
    }
}

class productsList : mmdata {
    var prod_id: Int?
    var prod_name: String?
    var prod_image: String?
    var prod_qty: String?
    var prod_color: String?
    var prod_size: String?
    var prod_price: Int?
    var total_val: Int?
    var cart_json: String?
    var prod_model: String?
    var create_date: Int?
    
    
   override func mapping(map: Map) {
        prod_id <- map["pid"]
        prod_name <- map["pname"]
        prod_image <- map["imgURL"]
        prod_color <- map["colorAvail"]
        prod_size <- map["sizeAvail"]
        prod_price <- map["price"]
        prod_model <- map["model"]
}
}
/*
 
 {
 "map": {
 "msg": "Successfull",
 "data": {
 "cateObj": {
 "pCateId": null,
 "cateId": null,
 "sortOrder": null,
 "pCateName": null,
 "showIn": null,
 "createUser": null,
 "isActive": null,
 "cateName": "",
 "cateCode": null,
 "createDate": null,
 "subCate": null,
 "additionalProperties": {},
 "pcateId": null,
 "pcateName": null
 },
 "prodObj": [
 {
 "pid": 2617,
 "pname": "Baby learning Mat One Side Printed",
 "model": "120700068",
 "sku": "12070006801",
 "price": 455,
 "minPrice": 400,
 "colorAvail": null,
 "colors": null,
 "sizeAvail": null,
 "size": null,
 "weight": 0,
 "height": null,
 "length": null,
 "imgURL": "https://s3.ap-south-1.amazonaws.com/mmimage/products/0-5cm-Wat/0-5cm-Waterproof-Rug-Toys-With-Bag-Kids-Baby-Play-Mat-Mat-for-Children-Carpet-EVA.jpg",
 "seoLink": null,
 "additionalProperties": {}
 },
 */
