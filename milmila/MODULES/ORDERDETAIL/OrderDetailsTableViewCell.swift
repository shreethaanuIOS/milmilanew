//
//  OrderDetailsTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 3/6/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit

class OrderDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var prodTot: UILabel!
    
    @IBOutlet weak var prodImh: UIImageView!
    
    @IBOutlet weak var prodName: UILabel!
    
    @IBOutlet weak var prodPrice: UILabel!
    
    @IBOutlet weak var prodQTY: UILabel!
    
    @IBOutlet weak var orderId: UILabel!
    
    @IBOutlet weak var deliveryStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
