//
//  OrderDetailViewController.swift
//  milmila
//  Created by prasun sarkar on 11/15/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var subTotatl: UILabel!
    @IBOutlet weak var tableheight: NSLayoutConstraint!
    @IBOutlet weak var orderSummary: UITableView!
    @IBOutlet weak var shippiAddress: UILabel!
    @IBOutlet weak var orderTotal: UILabel!
    @IBOutlet weak var orederData: UILabel!
    @IBOutlet weak var totalValue: UILabel!
    @IBOutlet weak var taxi: UILabel!
    @IBOutlet weak var shippi: UILabel!
    var oredrDetailArray = [orderDetail]()
  
    var ordersTotal: Int!
    var orderDate: Int!
    var shippingCost: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        orderSummary.delegate = self
        orderSummary.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        tableheight.constant = CGFloat(oredrDetailArray.count * 150)
    }
    
    // TABLEVIEW DELEGATES
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"OrderDetailsTableViewCell", for: indexPath) as! OrderDetailsTableViewCell
        
        cell.prodName.text = oredrDetailArray[indexPath.row].prodName
        cell.prodPrice.text = "₹  " + "\(oredrDetailArray[indexPath.row].prodPrice!)"
        cell.prodQTY.text =   "\(oredrDetailArray[indexPath.row].prodQty!)"
        cell.prodTot.text = "₹  " + "\(oredrDetailArray[indexPath.row].totalVal!)"
        cell.orderId.text = "# " + "\(oredrDetailArray[indexPath.row].ordersId!)"
        
        
        let myImgProd = oredrDetailArray[indexPath.row].prodImage
        cell.prodImh.setImageWithUrl(imageURl: img_URL + myImgProd!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return oredrDetailArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 135
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
