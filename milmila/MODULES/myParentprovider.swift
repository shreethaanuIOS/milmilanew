//
//  myParentprovider.swift
//  milmila
//
//  Created by prasun sarkar on 12/1/17.
//  Copyright © 2017 Milmila. All rights reserved.
//
import UIKit
import ESTabBarController_swift

enum ExampleProvider {
    
    static func customBackgroundColorStyle(implies: Bool) -> MyNavigationController {
        let tabBarController = ESTabBarController()
        if let tabBar = tabBarController.tabBar as? ESTabBar {
            tabBar.itemCustomPositioning = .fillIncludeSeparator
        }
        let v1 = CartViewController()
        let v2 = ViewController()
        let v3 = ProfileViewController()
        let v4 = myaccountViewController()
        let v5 = RecentlyViewedViewController()
        
        v1.tabBarItem = ESTabBarItem.init(MyBackgroundContentView(), title: nil, image: UIImage(named: "like_active"), selectedImage: UIImage(named: "like_active"))
        v2.tabBarItem = ESTabBarItem.init(MyBackgroundContentView(), title: nil, image: UIImage(named: "like_active"), selectedImage: UIImage(named: "like_active"))
        v3.tabBarItem = ESTabBarItem.init(MyBackgroundContentView.init(specialWithAutoImplies: implies), title: nil, image: UIImage(named: "like_active"), selectedImage: UIImage(named: "like_active"))
        v4.tabBarItem = ESTabBarItem.init(MyBackgroundContentView(), title: nil, image: UIImage(named: "like_active"), selectedImage: UIImage(named: "like_active"))
        v5.tabBarItem = ESTabBarItem.init(MyBackgroundContentView(), title: nil, image: UIImage(named: "like_active"), selectedImage: UIImage(named: "like_active"))
        
        tabBarController.viewControllers = [v1, v2, v3, v4, v5]
        
        let navigationController = MyNavigationController.init(rootViewController: tabBarController)
        tabBarController.title = "Example"
        return navigationController
    }
}
