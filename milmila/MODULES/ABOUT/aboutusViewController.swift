//
//  aboutusViewController.swift
//  milmila
//
//  Created by prasun sarkar on 11/6/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class aboutusViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet weak var aboutusWeb: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        aboutusWeb.delegate = self
        if let url = URL(string: "http://www.milmila.com/about-us.html"){
            let request = URLRequest(url: url)
            aboutusWeb.loadRequest(request)
        // Do any additional setup after loading the view.
    }
    }
    
    
    @IBAction func aboutUs(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "myaccountViewController") as! myaccountViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
