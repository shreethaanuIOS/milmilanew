//
//  RecentlyviewedTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/27/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class RecentlyviewedTableViewCell: UITableViewCell {

    @IBOutlet weak var productSize: UILabel!
    @IBOutlet weak var productrate: UILabel!
    @IBOutlet weak var productname: UILabel!
    @IBOutlet weak var productimage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
