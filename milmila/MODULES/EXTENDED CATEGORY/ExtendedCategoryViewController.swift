//
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//


import UIKit

class ExtendedCategoryViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var extendedCategory: UITableView!
    @IBOutlet weak var PageTitle: UILabel!
    let ExtCategorydata = ["Formal shirt","casual shirt","Party wear","fUll sleve","half sleeve"]
    let ExtcategoryImageData = ["shirt","pant","tshirt","watch","shirt"]
    override func viewDidLoad() {
        super.viewDidLoad()
       PageTitle.text = ""
        // Do any additional setup after loading the view.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ExtCategorydata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"ExtendedCategory", for: indexPath) as! ExtendedCategoryTableViewCell
        cell.extCategoryLabel.text = ExtCategorydata[indexPath.row]
        cell.extCategoryImage.image = UIImage(named: ExtcategoryImageData[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "productsdisplayviewcontroller") as! productsDisplayViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: CGFloat(10), height: CGFloat(10))
        cell.layer.transform = CATransform3DMakeScale(0.85, 0.82, 0.82)
        UIView.beginAnimations("scaleTableViewCellAnimationID", context: nil)
        UIView.setAnimationDuration(0.5)
        cell.layer.shadowOffset = CGSize(width: CGFloat(0), height: CGFloat(0))
        cell.alpha = 1
        cell.layer.transform = CATransform3DIdentity
        UIView.commitAnimations()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
