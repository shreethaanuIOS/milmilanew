//
//  mycartViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/26/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class myordersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var myOrders: UITableView!
    
    var httpRequest = CSSwiftApiHttpRequest()
    var rideListArray = [myOrderList]()
    var myorderlI = [orderDetail]()
    var myImgrl : String!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
 
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
            let vc =  UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }
        else{
       myOrdersList()
        }
    }

    @IBAction func back(_ sender: Any) {
        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func myOrdersList() {
        let UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String = BASE_URL + "/customerB2B/myOrderList?customerId=" + myuserid
        let params = [String : String]()
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(content ?? String())
            let response = Mapper<myOrderMapper>().map(JSONString: content!)
            
            if (response?.sCode == 200) {
                print("+++++++++ success ++++++++++")
                self.rideListArray = (response?.responsiidata)!
                self.myorderlI = (response?.responsiidata[0].orderDetails)!
                self.myOrders.reloadData()
            }
            else
            {
                self.showToastMessageTop(message: "Internal server error")
                print("+++++++++ vada pocheaaa  ++++++++++")
            }
        })
    }
    
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"myordersTableViewCell", for: indexPath) as! myordersTableViewCell
        
        
           if rideListArray[indexPath.row].orderDetails.count == 0 {
                cell.myorderimage.image = #imageLiteral(resourceName: "placeholderImage")
                cell.myorderName.text =  rideListArray[indexPath.row].refOrderNo
                cell.myorderRate.text =  "₹  " + "\(rideListArray[indexPath.row].orderValue!)"
                cell.myOrderMethod.text = rideListArray[indexPath.row].orderGeneratedBy
        }
        else {
            if rideListArray[indexPath.row].orderDetails[0].prodImage == nil {
                cell.myorderimage.image = #imageLiteral(resourceName: "placeholderImage")
                cell.myorderName.text =  rideListArray[indexPath.row].refOrderNo
                cell.myorderRate.text =  "₹  " + "\(rideListArray[indexPath.row].orderValue!)"
                cell.myOrderMethod.text = rideListArray[indexPath.row].orderGeneratedBy
            }
                myImgrl = rideListArray[indexPath.row].orderDetails[0].prodImage
                cell.myorderimage.setImageWithUrl(imageURl: img_URL + myImgrl)
                cell.myorderName.text =  rideListArray[indexPath.row].refOrderNo
                cell.myorderRate.text =  "₹  " + "\(rideListArray[indexPath.row].orderValue!)"
                cell.myOrderMethod.text = rideListArray[indexPath.row].orderGeneratedBy
        
    }
        return cell
    }
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rideListArray.count
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! myordersTableViewCell
        let myArrayValue = rideListArray[indexPath.row].orderDetails
        if rideListArray[indexPath.row].orderDetails.count == 0 {
        cell.selectionStyle = UITableViewCellSelectionStyle.none
            print("nonclickable")
        }
        else {
            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
            vc.oredrDetailArray = myArrayValue!
            vc.ordersTotal = rideListArray[indexPath.row].orderValue!
            vc.orderDate = rideListArray[indexPath.row].create_date!
            vc.shippingCost = rideListArray[indexPath.row].shippingCost!
            
            self.present(vc, animated: true, completion: nil)
        }
    }
}
