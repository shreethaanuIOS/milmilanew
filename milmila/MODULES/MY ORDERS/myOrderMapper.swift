//
//  myOrderMapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class myOrderMapper: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var responsiidata:  [myOrderList]!
   
    required init?(map: Map) {
    }
   
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        responsiidata <- map["data"]
    }
}

class myOrderList : myOrderMapper {
    var orderId : Int?
    var orderDetails : [orderDetail]!
    var paymentMode : String?
    var orderGeneratedBy : String?
    var channel_type : String?
    var create_date : Int?
    var orderValue : Int?
    var refOrderNo : String?
    var shippingCost : Int?
    
    override func mapping( map: Map) {
        // direct conversion
        orderId  <- map["orderId"]
        orderDetails <- map["orderDetailB2BEntityList"]
        paymentMode <- map["paymentMode"]
        orderGeneratedBy  <- map["orderGeneratedBy"]
        channel_type <- map["channel_type"]
        create_date <- map["createDate"]
        orderValue <- map["orderValue"]
        refOrderNo <- map["refOrderNo"]
        shippingCost <- map["shippingCost"]
    }
}

class orderDetail: myOrderList{
    var orderDetailId: Int?
    var prodMoq: Int?
    var prodSize: String?
    var prodPrice: Int?
    var totalVal:  Int?
    var prodImage:  String?
    var prod_color: String?
    var prodQty: Int?
    var ordersId:  Int?
    var prodId: Int?
    var prodName: String?
    
override func mapping( map: Map) {
        // direct conversion
        orderDetailId  <- map["orderDetailId"]
        prodMoq <- map["prodMoq"]
        prodSize  <- map["prodSize"]
        prodPrice <- map["prodPrice"]
        totalVal  <- map["totalVal"]
        prodImage <- map["prodImage"]
        prod_color  <- map["prodColor"]
        prodQty <- map["prodQty"]
        ordersId <- map["orderId"]
        prodId  <- map["prodId"]
        prodName <- map["prodName"]
    }
}

