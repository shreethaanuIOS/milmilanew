//
//  myordersTableViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 10/26/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class myordersTableViewCell: UITableViewCell {

    @IBOutlet weak var myorderRate: UILabel!
    @IBOutlet weak var myorderName: UILabel!
    @IBOutlet weak var myorderimage: UIImageView!
    
    @IBOutlet weak var myorderType: UILabel!
    
    @IBOutlet weak var myOrderMethod: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
