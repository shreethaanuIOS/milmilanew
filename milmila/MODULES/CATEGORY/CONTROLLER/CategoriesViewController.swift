//
//  CategoriesViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class CategoriesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var sortButton: UIButton!
    
    @IBOutlet weak var filterButton: UIButton!
    
    @IBOutlet weak var sortingWholeView: UIView!
    
    @IBOutlet weak var sortingView: UIView!
    @IBOutlet weak var filterBar: UIView!
    @IBOutlet weak var productsMenu: UICollectionView!
    @IBOutlet weak var productsCollectionView: UICollectionView!
   
    @IBOutlet weak var filterbarHeight: NSLayoutConstraint!
    var gridLayout: GridLayout!
    var httpRequest = CSSwiftApiHttpRequest()
    var categoryIdPass:String!
    
    var catSubCatName = [categoryList]()

    var fromtype = ""
    var categoryId = ""
    
    var categoryName = ""
    var productslist = [prodObj]()
    
    var subCateId = ""
 
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var cateName: UILabel!
    
    @IBOutlet weak var clearall: UIButton!
    @IBOutlet weak var applyfilter: UIButton!
    @IBOutlet weak var rightmenu: UITableView!
    @IBOutlet weak var leftMenu: UITableView!
    var imgname = ""
    var pname = ""
    var pprice = ""
    var desc = ""
    var prodId = ""
    var myprice = ""
    var prodQty = ""
    var imgsname = ""
    var sortOrder = ""
    var MysortingId = ""
    var rideListArray=[categoryList]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
          let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        sortingWholeView.addGestureRecognizer(tapGesture)
        filterBar.isHidden = true
        filterbarHeight.constant = 0
        sortingView.isHidden = true
        sortingWholeView.isHidden = true
        filterView.isHidden = true
        gridLayout = GridLayout()
        ProductsDisplay()
        productsCollectionView.setCollectionViewLayout(gridLayout, animated: true)
        cateName.text = categoryName
        MysortingId = categoryId
       // RideList()
    }

    func handleTap(sender: UITapGestureRecognizer) {
       sortingWholeView.isHidden = true
        sortingView.isHidden = true
    }
    
    @IBAction func FilterAction(_ sender: Any) {
        filterView.isHidden = false
        filterView.springAnimation()
       // bottomBar.isHidden = true
    
    }
    
    @IBAction func closeFilterAction(_ sender: Any) {
        filterView.isHidden = true
        filterView.springAnimation()
       // bottomBar.isHidden = false
        
    }
    
    @IBAction func sortingAction(_ sender: Any) {
        sortingView.isHidden = false
        sortingWholeView.isHidden = false
    }
    
    @IBAction func lowtohigh(_ sender: Any) {
       sortOrder = "ASC"
      //  sorting()
         sortingView.isHidden = true
    }
    
    @IBAction func hightolow(_ sender: Any) {
        sortOrder = "DEC"
     //   sorting()
        sortingView.isHidden = true
    }
    
    // COLLECTION VIEW DELEGATES
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == productsMenu {
        
            return catSubCatName.count
        
        }
        else
        
        {
          return productslist.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == productsCollectionView {
            if productslist[indexPath.row].price == nil {
                pprice = "price"
            }
                
            else {
                pprice = String(Int(Double(productslist[indexPath.row].price!)))
            }
            if productslist[indexPath.row].imgURL == nil {
            }
            else {
                
                imgsname = productslist[indexPath.row].imgURL
                let fullNameArr = imgsname.components(separatedBy: ",")
                let count = fullNameArr.count
                print(count)
                imgname = fullNameArr[0]
                let  productURL  = img_URL + imgname
            }
            
            if productslist[indexPath.row].pname == nil {
                desc = "pname"
            }
                
            else{
                
                desc = productslist[indexPath.row].pname
                
            }
            
            
            if productslist[indexPath.row].pid == nil {
                prodId = ""
            }
            else {
                prodId = String(Int(Double(productslist[indexPath.row].pid)))
            }
            let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailProductDisplayViewController") as! DetailProductDisplayViewController
            vc.IMGPASSto = imgname
            vc.NAMEPASSto = desc
            vc.PRICEPASSto = pprice
            vc.DESCRIPTIONPASSto = desc
            vc.IMAGEARRAYTOPASS = imgsname
            vc.PRODID = prodId
           // vc.cateid = categoryId
            vc.categname = categoryName
            self.present(vc, animated: true, completion: nil)
        }
        else {
            subCateId = catSubCatName[indexPath.row].cateId
            subCateDisplay()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if collectionView == productsMenu {
            let cells = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductsMenuCollectionViewCell", for: indexPath) as! ProductsMenuCollectionViewCell
             cells.menuCategory.text = catSubCatName[indexPath.row].cateName
            
        return cells
        }
         else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productsDisplayCategoryCollectionViewCell", for: indexPath) as! productsDisplayCategoryCollectionViewCell
            var productsimageURL = productslist[indexPath.row].imgURL
            if productsimageURL == nil
            {
                cell.productsImage.image = #imageLiteral(resourceName: "placeholderImage")
            }
            else {
                let fullNameArr = productsimageURL?.components(separatedBy: ",")
                let count = fullNameArr?.count
                print(count)
                let thumbImg = fullNameArr![0]
                let  productURL  = img_URL + thumbImg
                print(productURL)
                cell.productsImage.setImageWithUrl(imageURl: productURL)
            }
            if productslist[indexPath.row].pname == nil {
                cell.productsName.text = "Product name"
            }
            else {
                cell.productsName.text = productslist[indexPath.row].pname
            }
            if productslist[indexPath.row].price == nil {
                cell.productsPrice.text = "Price"
            }
            else{
                let priceProduct = "\(String(describing: productslist[indexPath.row].price!))"
                cell.productsPrice.text = "₹ " + priceProduct
            }
            cell.backgroundColor = UIColor.white
            cell.layer.shadowOpacity = 0.7
            cell.layer.shadowRadius = 5.0
            return cell
            
        }
    }
    
    // API CALL
    
    func ProductsDisplay()
    {
        let url:String = BASE_URL + "product/List?c=" + categoryId
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<prductsmapper>().map(JSONString: content!)
            if(responseData?.maping.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                
                self.productslist = (responseData?.maping.respdata.prodObj)!
                //  self.homeProductsListingArray = (responseData?.maping.respdata.cateObj)!
                //   self.TitleOFpage.text = (responseData?.maping.respdata.cateObj.cate_name)
                self.productsCollectionView.reloadData()
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
    }
    
    func subCateDisplay()
   
    {
        let url:String = BASE_URL + "product/List?c=" + subCateId
        let params = [String:String]()
        
        print(params)
        print(url)
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
        
            print(url)
            
            let responseData = Mapper<prductsmapper>().map(JSONString: content!)
            if(responseData?.maping.sCode == 200)
            
            {
                print("++++++++++++++ succes +++++++++++++++")
                self.MysortingId = self.subCateId
                self.productslist = (responseData?.maping.respdata.prodObj)!
                self.productsCollectionView.reloadData()
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
    }
    
    func RideList() {
        
        let url:String = BASE_URL + "category/list"
        let params = [String : String]()
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(content ?? String())
            /*
             * call the the object mapper
             */
            
            let response = Mapper<categorylistModel>().map(JSONString: content!)
            
            if (response?.sCode == 200) {
            
                print("+++++++++ success ++++++++++")
                
                self.catSubCatName = (response?.respdata)!
                print(self.rideListArray)
                self.productsMenu.reloadData()
            }
            
            else
            
            {
                self.showToastMessageTop(message: "Internal server error")
                print("+++++++++ vada pocheaaa  ++++++++++")
            }
        })
    }
    
    func sorting(){
        
        let url:String = BASE_URL + "products/sort?c=" + MysortingId + "&min=200&max=2000"
        let params = [String:String]()
        
        print(params)
        print(url)
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            print(url)
        
            let responseData = Mapper<prductsmapper>().map(JSONString: content!)
            if(responseData?.maping.sCode == 200)
            
            {
            
                print("++++++++++++++ succes +++++++++++++++")
                self.productslist = (responseData?.maping.respdata.prodObj)!
                self.productsCollectionView.reloadData()
           
            }
                
            else {
                 self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
        
    }
    
    ///MARK:- TABLEVIEW DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == leftMenu {
            return 6
        }
        else {
            return 5
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == leftMenu
        {
            let cell = tableView.dequeueReusableCell(withIdentifier:"filterLeftTableViewCell", for: indexPath) as! filterLeftTableViewCell
            cell.filterItemsView.isHidden = false
            rightmenu.isHidden = false
            rightmenu.reloadData()
        }
        else {
            let cells = tableView.dequeueReusableCell(withIdentifier:"filterrightTableViewCell", for: indexPath) as! filterrightTableViewCell
            cells.filercontent.textColor = UIColor.myBlueColour
            cells.filterSelection.image = #imageLiteral(resourceName: "like_active")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == leftMenu {
            let cell = tableView.dequeueReusableCell(withIdentifier:"filterLeftTableViewCell", for: indexPath) as! filterLeftTableViewCell
            cell.filterItems.text = "my filter contents"
            cell.filterItemsView.isHidden = true
            return cell
        }
        else {
            let cells = tableView.dequeueReusableCell(withIdentifier:"filterrightTableViewCell", for: indexPath) as! filterrightTableViewCell
            cells.filercontent.text = "Name"
            return cells
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
