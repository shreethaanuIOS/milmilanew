//
//  productsDisplayCategoryCollectionViewCell.swift
//  milmila
//
//  Created by prasun sarkar on 12/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit

class productsDisplayCategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productsFav: UIImageView!
    @IBOutlet weak var productsImage: UIImageView!
    @IBOutlet weak var productsPrice: UILabel!
    @IBOutlet weak var productsName: UILabel!
    
}
