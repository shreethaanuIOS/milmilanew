//
//  categorylistModel.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class categorylistModel: Mappable {
    
    var pid: Int?
    var model: Int?
    var pname: String?
    var price: Int?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        pid <- map["pid"]
        model <- map["model"]
        pname <- map["pname"]
        price <- map["price"]
    }
}






/*
 {
 "msg": "Found 6 Categorie(s).",
 "sCode": 200,
 "data": [
 {
 "pCateId": "",
 "cateId": "1",
 "sortOrder": "0",
 "pCateName": "",
 "showIn": "BOTH",
 "createUser": "",
 "isActive": "A",
 "cateName": "Fashion",
 "cateCode": "A1001",
 "createDate": ""
 },
 {
 "pCateId": "",
 "cateId": "2",
 "sortOrder": "0",
 "pCateName": "",
 "showIn": "BOTH",
 "createUser": "",
 "isActive": "A",
 "cateName": "Bags & Shoes",
 "cateCode": "B1001",
 "createDate": ""
 },
 {
 "pCateId": "",
 "cateId": "3",
 "pCateName": "",
 "showIn": "BOTH",
 "createUser": "",
 "isActive": "A",
 "cateName": "Kids & Mom",
 "cateCode": "C1001",
 "createDate": ""
 },
 {
 "pCateId": "",
 "cateId": "4",
 "sortOrder": "0",
 "pCateName": "",
 "showIn": "BOTH",
 "createUser": "",
 "isActive": "A",
 "cateName": "Accessories",
 "cateCode": "D1001",
 "createDate": ""
 },
 {
 "pCateId": "",
 "cateId": "5",
 "sortOrder": "0",
 "pCateName": "",
 "showIn": "BOTH",
 "createUser": "",
 "isActive": "A",
 "cateName": "Home Living",
 "cateCode": "E1001",
 "createDate": ""
 },
 {
 "pCateId": "",
 "cateId": "6",
 "sortOrder": "0",
 "pCateName": "",
 "showIn": "WEB",
 "createUser": "",
 "isActive": "D",
 "cateName": "fashion",
 "cateCode": "3232",
 "createDate": ""
 }
 ],
 "error": false,
 "status": true
 }
 
 
 */
