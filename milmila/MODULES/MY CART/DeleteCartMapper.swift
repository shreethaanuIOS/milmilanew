//
//  DeleteCartMapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/23/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class DeleteCartMapper: Mappable {
    
    var msg: String?
    var sCode: Int?
    var data: String?
    var error: Bool?
    var status: Bool?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        msg <- map["msg"]
        sCode <- map["sCode"]
        data <- map["data"]
        status <- map["status"]
         error <- map["error"]
    }
}

/*
 
 {
 "msg": "Successfully Deleted",
 "sCode": 200,
 "data": "java.lang.Object@391f6625",
 "error": false,
 "status": true
 }
 */

