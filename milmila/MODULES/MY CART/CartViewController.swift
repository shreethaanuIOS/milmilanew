//
//  CartViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/26/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper
import KRProgressHUD

class CartViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var detailImg: UIImageView!
    @IBOutlet weak var detailName: UILabel!
    @IBOutlet weak var detailType: UILabel!
    @IBOutlet weak var qty: UILabel!
    @IBOutlet weak var sizeof: UILabel!
    @IBOutlet weak var priceof: UILabel!
    @IBOutlet weak var backbuttonAction: UIButton!
    @IBOutlet weak var qtyTextField: UITextField!
    @IBOutlet weak var carteditwholeview: UIView!
    @IBOutlet weak var cartedit: SpringView!
    @IBOutlet weak var cartTable: UITableView!
    
    var cartListing = [cartListMapper]()
    var deleteCart = [DeleteCartMapper]()
    var imgname : String!
    var UID : Int!
    var httpRequest = CSSwiftApiHttpRequest()
    
    @IBOutlet weak var noCartItem: UIView!
    
    var imgnames = ""
    var pname = ""
    var pprice = ""
    var desc = ""
    var prodId = ""
    var myprice = ""
    
    var prodQty: String!
    var pnames : String!
    var pprices : Int!
    var pImages : String!
    var pcolor: String!
    var psize: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cartedit.isHidden = true
        carteditwholeview.isHidden = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(sender:)))
        carteditwholeview.addGestureRecognizer(tapGesture)
        noCartItem.isHidden = true
        cartList()
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
        }
        else{
           // fabIcon()
            // Do any additional setup after loading the view.
        }

    }
    @IBAction func CheckOutAction(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Order", bundle: nil).instantiateViewController(withIdentifier: "ShippingViewController") as! ShippingViewController
        vc.fromWay = "AddtoCart"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func closeNocart(_ sender: Any) {
  
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    
    }
    
    func notPrettyString(from object: Any) -> String? {
        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func parserJSON(data: Data){
        var names = [String]()
        do {
            if let ipString = NSString(data:data as Data, encoding: String.Encoding.utf8.rawValue) {
                let json = try JSONSerialization.jsonObject(with: data as Data, options: .mutableContainers) as! [AnyObject]
               
                print(json)
                print(json.first?.object)
                var data = [cartListMapper]()
                    for i in 0..<json.count {
                    let jsonString  = notPrettyString(from: json[i])
                    let responseData = Mapper<cartListMapper>().map(JSONString: jsonString!)
                    cartListing.append(responseData!)
                    print(responseData?.id)
                }
                
                if  cartListing.count == 0 {
                    noCartItem.isHidden = false
                    print("Empty")
                    cartTable.reloadData()
                }
                    
                else{
                    cartTable.reloadData()
                }
            }
        } catch {
            ///alert
            print(error)
        }
    }
    
    @IBAction func CartActionToDo(_ sender: Any) {
    }
    
    @IBAction func backbutton(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        cartedit.isHidden = true
        carteditwholeview.isHidden = true
    }
    
    @IBAction func increment(_ sender: Any) {
        
        if (Int(qtyTextField.text!)! > 0){
            qtyTextField.text = String(Int(qtyTextField.text!)! + 1)
        } else {
            qtyTextField.text = "1"
        }
    }
    
    @IBAction func decrement(_ sender: Any) {
        
        if (Int(qtyTextField.text!)! > 1){
            qtyTextField.text = String(Int(qtyTextField.text!)! - 1)
        } else {
            qtyTextField.text = "1"
        }
        }

    @IBAction func RemoveAction(_ sender: Any) {
        deleteCartContent()
        cartedit.isHidden = true
        carteditwholeview.isHidden = true
    }
    
    @IBAction func changeAction(_ sender: Any) {
        
    }
    
    // API
    func cartList()
    {
        UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        let myuserid = String(UID)
        let url:String = BASE_URL + "customerB2B/cartList?userId=" + myuserid
        let params = [String:String]()
        print(params)
        print(url)
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
         
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            self.parserJSON(data: response as! Data)
            KRProgressHUD.dismiss()
            self.cartTable.reloadData()
        })
    }
    
    // Delete Cart API
    
    func deleteCartContent()
    {
        UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        
        let myuserid = String(UID)
        let url:String = BASE_URL + "customerB2B/deleteCartListProduct?id=" + prodId + "&uid=" + "\(myuserid)"
        let params = [String:String]()
        
        print(params)
        print(url)
       
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
        
            let responseData = Mapper<DeleteCartMapper>().map(JSONString: content!)
            
            if(responseData?.sCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
            }
            else {
                print("------------ failed --------------")
            }
        })
    }
    
    func checkout(){
        
        let url = URL(string: "http:www.milmila.com/user/order?")!
        var request = URLRequest(url: url)
        
        UID = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) as! Int
        
        let myuserid = String(UID)
        
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        
        let postString = "user=" + "\(UID)"
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
        guard let data = data, error == nil else {
                // check for fundamental networking error
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {        // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(responseString)")
        }
        self.showSuccessToastMessage(message: "CheckOut done successfully")
         self.noCartItem.isHidden = false
        task.resume()
    }
    
    /// MARK :-
    // Tableview Delegates
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cartListing.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            prodId = String(Int(Double(cartListing[indexPath.row].id!)))
            deleteCartContent()
              cartListing.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
        if cartListing.count == 0 {
            noCartItem.isHidden = false
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"cartTableViewCell", for: indexPath) as! cartTableViewCell
        cell.cartProductName.text = cartListing[indexPath.row].prod_name
        cell.cartProductPrice.text = "₹  " + String(Int(Double(cartListing[indexPath.row].total_val!)))
        cell.cartProductColour.text = cartListing[indexPath.row].prod_color
        cell.productSize.text = cartListing[indexPath.row].prod_size
        imgname = cartListing[indexPath.row].prod_image
        let  productURL  = img_URL + imgname
        cell.cartimage.setImageWithUrl(imageURl: productURL)       
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        pprice = String(Int(Double(cartListing[indexPath.row].prod_price!)))
//        imgnames = cartListing[indexPath.row].prod_image!
//
//        let  productURL  = img_URL + imgname
//        prodId = String(Int(Double(cartListing[indexPath.row].id!)))
//        actionforedit()
//        pnames = cartListing[indexPath.row].prod_name
//        psize = cartListing[indexPath.row].prod_size
//        pcolor = cartListing[indexPath.row].prod_color
//        prodQty = cartListing[indexPath.row].prod_qty
    }
    
    func actionforedit() {
        cartedit.isHidden = false
        carteditwholeview.isHidden = false
        cartTable.reloadData()
        detailImg.setImageWithUrl(imageURl: img_URL + imgnames)
        detailName.text = pnames
        detailType.text = "₹ " + pprice
        priceof.text = "₹ " + pprice
        qty.text = prodQty
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
