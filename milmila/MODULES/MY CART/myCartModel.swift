//
//  myCartModel.swift
//  milmila
//
//  Created by prasun sarkar on 11/20/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class myCartModel: Mappable {
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var data: buyNowProductRelateIdsD!
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        data <- map["data"]
    }
}

class buyNowProductRelateIdsD : myCartModel {
    
    var buyNowProductRelateId: Int?
    var bnId : Int?
    
    override func mapping(map: Map) {
        buyNowProductRelateId <- map["buyNowProductRelateId"]
        bnId <- map["bnId"]
}
}

/*
 
 
 {
 "msg": "Successfull",
 "sCode": 200,
 "data": {
 "buyNowProductRelateId": 484,
 "bnId": 484
 },
 "error": null,
 "status": true
 }
 
 */
