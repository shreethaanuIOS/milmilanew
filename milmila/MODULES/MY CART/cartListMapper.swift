//
//  cartListMapper.swift
//  milmila
//
//  Created by prasun sarkar on 11/20/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class cartListMapper: Mappable {
    
    var id: Int?
    var user_id: Int?
    var uuid: Int?
    var prod_id: Int?
    var prod_name: String?
    var prod_image: String?
    var prod_qty: String?
    var prod_color: String?
    var prod_size: String?
    var prod_price: Int?
    var total_val: Int?
    var cart_json: String?
    var create_time: Int?
    var create_date: Int?
    var shipping_cost: Int?
    var warehouse_detail: String?
    var grandTotal: Int?
   // var basesData: baseData!
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        user_id <- map["customerId"]
        uuid <- map["uuid"]
        prod_id <- map["prod_id"]
        prod_name <- map["prod_name"]
        prod_image <- map["prod_image"]
        prod_qty <- map["prod_qty"]
        prod_color <- map["prod_color"]
        prod_size <- map["prod_size"]
        prod_price <- map["prod_price"]
        total_val <- map["total_val"]
        cart_json <- map["cart_json"]
        create_time <- map["create_time"]
        create_date <- map["create_date"]
        shipping_cost <- map["shipping_cost"]
        warehouse_detail <- map["warehouse_detail"]
        grandTotal <- map["grand_total"]
        
       // basesData <- map["baseData"]
    }
}

//class baseData: cartListMapper {
//
//}

/*
 
 [
 {
 "id": 77,
 "user_id": 31,
 "uuid": "",
 "prod_id": 223,
 "prod_name": "Hot Man's Sports Shoes",
 "prod_image": "B1001/20059a.jpg",
 "prod_qty": 2,
 "prod_color": "NA",
 "prod_size": "NA",
 "prod_price": 507,
 "total_val": 1014,
 "cart_json": "com.mm.b2b.model.CartJson@a65e885",
 "create_time": 1511154501429,
 "create_date": 1511136000000
 }
 ]
 
 
 
 */
