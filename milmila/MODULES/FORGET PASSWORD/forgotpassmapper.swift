//
//  forgotpassmapper.swift
//  milmila
//
//  Created by prasun sarkar on 1/4/18.
//  Copyright © 2018 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class forgotpassmapper: Mappable {
var sCode: Int?
var error: Bool?
var msg: String?
var status: Bool?
required init?(map: Map) {
    
}
func mapping(map: Map) {
    sCode <- map["sCode"]
    error <- map["error"]
    msg <- map["msg"]
    status <- map["status"]
}
}
