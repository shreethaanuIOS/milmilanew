//
//  forgotPasswordViewController.swift
//  milmila
//
//  Created by prasun sarkar on 11/15/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class forgots: UIViewController {
    
    @IBOutlet weak var successview: UIView!
    @IBOutlet weak var respmsg: UILabel!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var resetPassword: UILabel!
    
    var httpRequest = CSSwiftApiHttpRequest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        successview.isHidden = true
        // Do any additional setup after loading the view.
    }
    @IBAction func okaction(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    @IBAction func resetPasswordButtonAction(_ sender: Any) {
        
        reset()
        
    }
    
    func reset()
    {
        let uid = email.text
        let url:String = BASE_URL + "customerB2B/forgetPassword?emailid=" + uid!
        
        //
        //        let categoryId:Int! = GlobalConstants.General.USERDEFAULTS.value(forKey: GlobalConstants.Login.CSTeamId) as! Int
        let params:[String:String] = [:]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<forgotpassmapper>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
                print("+++++++++ success ++++++++++")
                
                self.successview.isHidden = false
                self.respmsg.text = responseData?.msg
            }
            else {
                 self.showToastMessageTop(message: "Internal server error")
                print("+++++++++ vada pocheaaa  ++++++++++")
            }
        })
    }
    
    
    
    @IBAction func signInAction(_ sender: Any) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

