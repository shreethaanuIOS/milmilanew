//
//  ShippingInfoViewController.swift
//  milmila
//
//  Created by prasun sarkar on 11/10/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown

class ShippingInfoViewController: UIViewController {

    @IBOutlet var nameTextField: SkyFloatingLabelTextField!
    /// address One text field
    @IBOutlet var addressTextField: SkyFloatingLabelTextField!
    /// address One text field
    @IBOutlet var addressLineTwoTextField: SkyFloatingLabelTextField!
    /// country text field
    @IBOutlet var countryTextField: SkyFloatingLabelTextField!
    /// state text field
    @IBOutlet var stateTextField: SkyFloatingLabelTextField!
    /// city text field
    @IBOutlet var cityTextField: SkyFloatingLabelTextField!
    /// zipcode text field
    @IBOutlet var zipcodeTextField: SkyFloatingLabelTextField!
    /// fist billing name text field
    @IBOutlet var billingNameTextField: SkyFloatingLabelTextField!
    /// billing address One text field
    @IBOutlet var billingAddressTextField: SkyFloatingLabelTextField!
    /// billing address One text field
    @IBOutlet var billingAddressLineTwoTextField: SkyFloatingLabelTextField!
    /// billing country text field
    @IBOutlet var billingCountryTextField: SkyFloatingLabelTextField!
    /// billing state text field
    @IBOutlet var billingStateTextField: SkyFloatingLabelTextField!
    /// billing city text field
    @IBOutlet var billingCityTextField: SkyFloatingLabelTextField!
    /// billing zipcode text field
    @IBOutlet var billingZipcodeTextField: SkyFloatingLabelTextField!
    /// order cost
    @IBOutlet var billingView: UIView!
    /// Scroll View
    @IBOutlet var shippingScrollView: UIScrollView!
    /// Scroll Content View
    @IBOutlet var shippingContentView: UIView!
    /// order cost
    @IBOutlet var addressView: UIView!
    /// order cost
    @IBOutlet var switchView: UIView!
    /// shipping methods dropdown
    //----------- Drop Down Declaring -----------------//
    /// city dropdown
    var stateDropDown: DropDown!
    /// city dropdown
    var cityDropDown: DropDown!
    /// country dropdown
    var countryDropDown: DropDown!
    /// Country Name list
    //--------- Get Complete Country,State And City --------------//
//    var countryList = [CSCountryStates]()
    var countryNameList = [String]()
    // order Address Detail dict
    var orderAddressDetail = [String: String]()
    /// order list
    var orderList: String!
    /// order cost
    var totalCost: String!
    /// Shipping Address
 //   var shippingAddress: CSAddress!
    /// Billing Address
//    var billingAddress: CSAddress!
    var isFetchAddress = false
    var isFromCart = true
//    var orderInfo: CSOrderInfo!
    /// Cart Content
   // var cartContents: CSResponceCart! = nil
    /// CardId
    var cardId = String()
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: Any) {
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
