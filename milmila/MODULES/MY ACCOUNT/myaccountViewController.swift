//
//  myaccountViewController.swift
//  milmila
//
//  Created by prasun sarkar on 10/25/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class myaccountViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    var httpRequest = CSSwiftApiHttpRequest()
    var rideListArray=[categoryList]()
   
    var accountData: [String]   = ["Recently viewed","Balance","Coupons","Enter invite code","Shipping","Return policy","FAQ","Invite friends","About Us"]
    
    let accountImages = ["viewed","balance","coupon","invitecode","shipping","policy","faq","invitation","about"]
    
    var tableViewHeight:CGFloat = 0;
  
    let mySection: [String] = ["AccountDetils", "Categories"]
   
    @IBOutlet weak var myAccountTable: UITableView!
   
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var useremail: UILabel!
    
    @IBOutlet weak var userprofilepic: UIImageView!
    
    @IBOutlet weak var mylowerAcc: UITableView!
    var cellSpacingHeight: CGFloat = 10
    
    var sectionData: [Int: [String]] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        RideList()
        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName) == nil){
        username.text = "Hello User"
        userprofilepic.image = #imageLiteral(resourceName: "accountimage")
        }
            
        else {
            
            username.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserName)! as? String
        
            
            useremail.text = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSEmail)! as? String
        
            
            let myImg = GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSProfilePicture)! as? String
            
            if (myImg == "")
                        {
                            userprofilepic.image = #imageLiteral(resourceName: "accountimage")
                        }
                        else if (myImg == nil){
                           userprofilepic.image = #imageLiteral(resourceName: "accountimage")
                        }
                        else{
                            userprofilepic.setImageWithUrl(imageURl: myImg)
                    }
                    }
    
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func profileAction(_ sender: Any) {
    
                        if (GlobalConstants.General.USERDEFAULTS.object(forKey: GlobalConstants.Login.CSUserToken) == nil ){
                      
                        }
                         else{
                            let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        
                            self.present(vc, animated: true, completion: nil)
                        }
        
    
    }

    
    func RideList() {
        
        let url:String = BASE_URL + "category/list"
        let params = [String : String]()
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(content ?? String())
            /*
             * call the the object mapper
             */
            
            let response = Mapper<categorylistModel>().map(JSONString: content!)
            
            if (response?.sCode == 200) {
                print("+++++++++ success ++++++++++")
                self.rideListArray = (response?.respdata)!
                print(self.rideListArray)
                self.myAccountTable.reloadData()
                
            }else{
                self.showToastMessageTop(message: "Internal server error")
                print("+++++++++ vada pocheaaa  ++++++++++")
            }
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        

        return rideListArray.count
       
   
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == myAccountTable {
//     if indexPath.row == 0 {
//
//        }
//     else if indexPath.row == 1 {
//     //   let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "RecentlyViewedViewController") as! RecentlyViewedViewController
//     //   self.present(vc, animated: true, completion: nil)
//        }
//     else if indexPath.row == 2 {
//
//        }
//     else if indexPath.row == 3 {
//
//     }
//     else if indexPath.row == 4 {
//        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "inviteViewController") as! inviteViewController
//        self.present(vc, animated: true, completion: nil)
//     }
//     else if indexPath.row == 5 {
//
//        }
//     else if indexPath.row == 6 {
//        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "returnViewController") as! returnViewController
//        self.present(vc, animated: true, completion: nil)
//
//        }
//     else if indexPath.row == 7 {
//
//        }
//     else if indexPath.row == 8 {
//
//
//        }
//     else  {
//        let vc =  UIStoryboard(name: "account", bundle: nil).instantiateViewController(withIdentifier: "aboutusViewController") as! aboutusViewController
//        self.present(vc, animated: true, completion: nil)
//        }
//    }
    
  
}
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cells = tableView.dequeueReusableCell(withIdentifier:"account", for: indexPath) as! myaccountTableViewCell
            cells.accountlabel.text = rideListArray[indexPath.row].cateName
            if (cells.accountlabel.text) == "my orders"{
                cells.viewallButton.isHidden = false
            }else{
                cells.viewallButton.isHidden = true
            }
            return cells
}
//    else {
    
//    let cellst = tableView.dequeueReusableCell(withIdentifier:"accounttablecell", for: indexPath) as! myAccountHeaderTableViewCell
//
//    cellst.accountame.text = accountData[indexPath.row]
//    cellst.accountImgae.image = UIImage(named: accountImages[indexPath.row])
//    //cellst.accountImgae.image = UIImage(accountImages[indexPath.row])
//    return cellst
//        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
            return 113;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

    }

