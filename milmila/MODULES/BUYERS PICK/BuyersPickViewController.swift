//
//  milmila
//
//  Created by ShreeThaanu on 10/24/17.
//  Copyright © 2017 Milmila. All rights reserved.
//


import UIKit
import ObjectMapper

class BuyersPickViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var searchresult: UILabel!
    
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var searchDetailImage: UIImageView!
    @IBOutlet weak var buyersTableview: UITableView!
    var CategoryData      = ["Shirt","Pant","Tshirt","watch","Pant","Tshirt","watch","Pant","Tshirt","watch"]
    let categoryImageData = ["shirt","pant","tshirt","watch","pant","tshirt","watch","pant","tshirt","watch"]
    var searchListing = [productsList]()
    var httpRequest = CSSwiftApiHttpRequest()
    
    @IBOutlet weak var searchText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func searchAction(_ sender: Any) {
        searchResultView.isHidden = true
        
        searchApi()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // API CALL
    
    func searchApi(){
        
        let keyword = searchText.text
        let newString = keyword?.replacingOccurrences(of: " ", with: "+", options: .literal, range: nil)
        if (keyword == ""){
            self.searchResultView.isHidden = false
            self.searchDetailImage.image = #imageLiteral(resourceName: "about")
            self.searchresult.text = "Empty values cannot be searched"
        }
        else{

        let url:String = BASE_URL +  "api/product/search?text=" + newString!
        let params = [String:String]()
        print(params)
        print(url)
        
        httpRequest.getMethodWithHeader(url: url, view: self.view, parameter: params, ResponseBlock: { (response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            print(url)
            let responseData = Mapper<searchMaper>().map(JSONString: content!)
            self.view.endEditing(true)
            if(responseData?.mapin.statusCode == 200)
            {
                print("++++++++++++++ succes +++++++++++++++")
                
                self.searchListing = (responseData?.mapin.response.prodObj)!
                
                if(self.searchListing.count == 0){
                    self.searchResultView.isHidden = false
                    self.searchDetailImage.image = #imageLiteral(resourceName: "No_Records_image")
                    self.searchresult.text = "No items found "
            }
                else{
                    self.buyersTableview.reloadData()
                }
            }
                
            else {
                self.showToastMessageTop(message: "Internal server error")
                print("------------ failed --------------")
            }
        })
        }
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListing.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailProductDisplayViewController") as! DetailProductDisplayViewController
        vc.IMGPASSto = searchListing[indexPath.row].prod_image!
        vc.NAMEPASSto = searchListing[indexPath.row].prod_name!
        vc.PRODID = String(Int(Double(searchListing[indexPath.row].prod_id!)))
        vc.fromtype = "search"
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"buyerspiccell", for: indexPath) as! BuyerspickTableViewCell
        cell.productname.text = searchListing[indexPath.row].prod_name
        var cellImg = searchListing[indexPath.row].prod_image
        if (((cellImg?.components(separatedBy: ","))) != nil) {
            let fullNameArr = cellImg?.components(separatedBy: ",")
            let thumbImg = fullNameArr![0]
            let  productURL  = img_URL + thumbImg
            print(productURL)
            cellImg = img_URL + productURL
            cell.productimage.setImageWithUrl(imageURl: cellImg)
            cell.hashtag.text = searchListing[indexPath.row].prod_name
            cell.hashtag1.text = "Price :  ₹" +  String(Int(Double(searchListing[indexPath.row].prod_price!)))
            cell.hashtag2.text = "Model :  " +  searchListing[indexPath.row].prod_model!
        }
        else {
            let prodImg =  img_URL + cellImg!
            cell.productimage.setImageWithUrl(imageURl: prodImg)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 140;   //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath){
        let cell:BuyerspickTableViewCell = cell as! BuyerspickTableViewCell
        cell.cellDisplayAnimation()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
