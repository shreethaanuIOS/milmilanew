//
//  SignupViewController.swift
//  milmila
//
//  Created by prasun sarkar on 11/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class SignupViewController: UIViewController {

    @IBOutlet weak var Login: UIButton!
    @IBOutlet weak var google: UIButton!
    @IBOutlet weak var twitterButton: UIButton!
    @IBOutlet weak var fb: UIButton!
    @IBOutlet weak var signup: UIButton!
    @IBOutlet weak var cPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var name: UITextField!
    var httpRequest = CSSwiftApiHttpRequest()
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    
  ///  MARK :-
    // Button Actions :
    
    @IBAction func googleAction(_ sender: Any) {
    
    }
    
    @IBAction func twitterAction(_ sender: Any) {
    
    }
    
    @IBAction func fbAction(_ sender: Any) {
    
    }
    
    func isValidationSuccess() -> Bool {
        
        if (name.text == "") {
            self.showToastMessageTop(message: "Please enter the Name")
            return false
        }
        else if (name.text?.isEmpty)! {
            self.showToastMessageTop(message: "Please enter the Name")
            return false
        }
        else if (trimString(self.cPassword.text!).count < 6) {
            self.showToastMessageTop(message: "Password must be of minimum 6 digits")
            return false
        }
        else if (email.text == "") {
            self.view.makeToast("Please enter the email", duration: 3.0, position: .bottom)
            return false
        }
        if !((email?.text?.validateEmail(true))!) {
            self.showToastMessageTop(message:
                NSLocalizedString("Please enter a vaild E-mail id",
                                  comment: "Email"))
            return false
        }
        
        return true
    }
    
    func trimString(_ value: String) -> String {
        return value.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    @IBAction func SignupAction(_ sender: Any) {
        if isValidationSuccess() {
         signUp()
        }
        else {
   
    }
    }
    @IBAction func closeAction(_ sender: Any) {
        let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func AlreadyLogin(_ sender: Any) {
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /// MARK :- API CALL
    
    func signUp()
    {
        let url:String = BASE_URL + "customerB2B/signUp"
        //
        //        let categoryId:Int! = GlobalConstants.General.USERDEFAULTS.value(forKey: GlobalConstants.Login.CSTeamId) as! Int
        
        let params:[String:String] = [
            "firstName":       name.text!,
            "lastName":       name.text!,
            "loginName":       email.text!,
            "mobile":         cPassword.text!,
            "deviceId":    deviceId,
            "deviceType": "IOS",
            "signedFrm": "MOBILE",
            "socialSignUp": "N",
            "socialNwName": "",
            "isActive": "N",
            "isVerified": "N",
            "pwd" : password.text!,
            "verifiedDate": "",
            "createDate": "",
            "createUser": "MM_API",
            "gstNumber": "",
            "gstName": "",
            "gstZipcode": "",
            "panNumber": "",
            "isGstVerified": "",
            "gstVerifiedDate": "",
            "verifyLink": "",
            "gstAddr": "",
            "isGstOptional": ""
            
        ]
        print(params)
        httpRequest.PostMethodWithOtherHeader(url: url, view: self.view, Parameter: params as [String : AnyObject], ResponseBlock: {(response) -> Void in
            
            let content = String(data: response as! Data, encoding: String.Encoding.utf8)
            
            let responseData = Mapper<signupModel>().map(JSONString: content!)
            if(responseData?.sCode == 200)
            {
               self.showToastMessageTop(message: responseData?.msg)
                print(responseData?.msg)
                let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewController") as! ViewController
                self.present(vc, animated: true, completion: nil)
            }
            else {
              self.showToastMessageTop(message: responseData?.msg)
                print(responseData?.msg)
            }
        })
    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    extension SignupViewController : UITextFieldDelegate {
        /// Scrollview's ContentInset Change on Keyboard hide
     
        /// UnRegistering Notifications
        func unregisterKeyboardNotifications() {
            NotificationCenter.default.removeObserver(self)
        }
        /// register Keyboard
        func registerKeyboardNotifications() {
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(ChangePasswordViewController.keyboardDidShow(notification:)),
                                                   name: NSNotification.Name.UIKeyboardDidShow,
                                                   object: nil)
            NotificationCenter.default.addObserver(self,
                                                   selector: #selector(ChangePasswordViewController.keyboardWillHide(notification:)),
                                                   name: NSNotification.Name.UIKeyboardWillHide,
                                                   object: nil)
        }
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            return true
        }
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            if textField == name {
                _ = email.becomeFirstResponder()
            } else if textField == email {
                _ = password.becomeFirstResponder()
            } else if textField == password {
                _ = cPassword.becomeFirstResponder()
            }
            else if textField == cPassword {
                dismissKeyboard()
            }
            return true
        }
    }

