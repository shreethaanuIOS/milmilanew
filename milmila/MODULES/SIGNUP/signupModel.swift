//
//  signupModel.swift
//  milmila
//
//  Created by prasun sarkar on 11/14/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import ObjectMapper

class signupModel: Mappable {
    
    var sCode: Int?
    var error: Bool?
    var msg: String?
    var status: Bool?
    var respdata:  String?
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        sCode <- map["sCode"]
        error <- map["error"]
        msg <- map["msg"]
        status <- map["status"]
        respdata <- map["data"]
    }
}


//class SignUp : signupModel{
//
//    var user_id: String?
//    var first_name: String?
//    var last_name:String!
//    var login_name:String?
//    var addr1:String!
//    var addr2:String!
//    var addr3 : String!
//    var city_id:String!
//    var city_name:String!
//    var state_id:String!
//    var state_name:String!
//    var zip_code : String!
//    var mobile:String!
//    var dob:String!
//    var gender:String!
//    var profile_image : String!
//    var is_active:String!
//    var is_verified:String!
//    var verified_date:String!
//    var create_date:String!
//    var create_user:String!
//
//    override func mapping( map: Map) {
//        // direct conversion
//        user_id    <- map["user_id"]
//        first_name     <- map["first_name"]
//        last_name   <- map["last_name"]
//        login_name   <- map["login_name"]
//        addr1      <- map["addr1"]
//        addr2     <- map["addr2"]
//        addr3       <- map["addr3"]
//        city_id          <- map["city_id"]
//        city_name          <- map["city_name"]
//        state_id          <- map["state_id"]
//        state_name     <- map["state_name"]
//        zip_code       <- map["zip_code"]
//        mobile          <- map["mobile"]
//        dob          <- map["dob"]
//        gender          <- map["gender"]
//        profile_image <- map["profile_image"]
//        is_active          <- map["is_active"]
//        is_verified          <- map["is_verified"]
//        verified_date          <- map["verified_date"]
//        create_date <- map["create_date"]
//        create_user <- map["create_user"]
//    }
//}



/*
 
 {
 "error": false,
 "sCode": 200,
 "msg": "Succesfully Logged-In",
 "status": true,
 "data": {
 "user_id": "7",
 "first_name": "TEst",
 "last_name": "",
 "login_name": "test@milmila.com",
 "addr1": null,
 "addr2": null,
 "addr3": null,
 "city_id": null,
 "city_name": null,
 "state_id": null,
 "state_name": null,
 "zip_code": null,
 "mobile": "1234567890",
 "dob": null,
 "gender": null,
 "profile_image": null,
 "is_active": "Y",
 "is_verified": "Y",
 "verified_date": null,
 "create_date": 1510140420000,
 "create_user": "MM_USER"
 }
 }
 
 */
