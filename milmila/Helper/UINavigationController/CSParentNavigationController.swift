

import UIKit
class CSParentNavigationController: UINavigationController,UINavigationControllerDelegate,UIViewControllerTransitioningDelegate {
    
    override func viewDidLoad() {
        self.navigationBar.isHidden = false
        self.delegate = self
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
    }
    
  
    func deviceDidRotate() {
        if UIDevice.current.orientation.isLandscape {
            self.navigationBar.isHidden = true
        } else {
            self.navigationBar.isHidden = false
        }
    }
    

    
    //MARK- Animation
//    func navigationController(
//        _ navigationController: UINavigationController,
//        animationControllerFor operation:
//        UINavigationControllerOperation,
//        from fromVC: UIViewController,
//        to toVC: UIViewController
//        ) -> UIViewControllerAnimatedTransitioning? {
////        let transition = TransitionZoom();
////        if( operation == .push ){
////            transition.isPresenting = false;
////        }else{
////            transition.isPresenting = true;
////        }
//        return transition
//    }
    
    /// navigation controller delegate
    ///
    /// - Parameters:
    ///   - navigationController: navigation controller object
    ///   - viewController: viewcontroller object
    ///   - animated: animate bool
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool){
        if(viewController is ViewController){
            self.navigationBar.isHidden = true
        } else {
            self.navigationBar.isHidden = false
        }
    }
   

    
    
    /// remove child View
    ///
    /// - Parameter controller: remove the child view
    func removeLoginPopUp(controller: UIViewController) {
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    
    
    // MARK:Private Methods
    

    /// Create right cart button in bar button
    ///
    /// - Returns: Bar button
    private  func setRightProfileButton()->UIBarButtonItem{
        //Creating a right Search Icon
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "about"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.rightProfileButtonAction(sender:)), for: .touchUpInside)
        let rightSearchBarButtonItem = UIBarButtonItem(customView: btn1)
        rightSearchBarButtonItem.tintColor=UIColor.barButtonItemColor
        return rightSearchBarButtonItem
    }
    
    
    /// Button Action for Search
    ///
    /// - Parameter sender: Any
    func rightProfileButtonAction(sender:UIButton) {
        /// navigate to ask alecia page
        let vc = CSApplicationConstant.mainStoryboard.instantiateViewController(withIdentifier: "ProfileViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
