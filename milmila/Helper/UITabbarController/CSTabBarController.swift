
import UIKit

class CSTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let nc = NotificationCenter.default
        nc.removeObserver(self)
        nc.addObserver(self, selector: #selector(UserLoggedIn), name: Notification.Name("UserLoggedIn"), object: nil)
    }
    
    func deviceDidRotate() {
        if UIDevice.current.orientation.isLandscape {
            self.tabBar.isHidden = true
        } else {
            self.tabBar.isHidden = false
        }
    }
    
    /// User Login Successfull remove the current view
    ///
    /// - Parameter notification: nil
    func UserLoggedIn(notification: NSNotification){
        /// takes tap bar current view controller At that index
        let viewController: CSParentViewController! = self.childViewControllers[self.selectedIndex].childViewControllers.first as! CSParentViewController
        /// remove child view at that index
        if(viewController.childViewControllers.first is LoginViewController) {
          viewController.removeChildView(controller: (viewController.childViewControllers.first)!)
          viewController.callApi()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
