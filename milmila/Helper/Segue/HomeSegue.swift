/*
 * HomeSegue
 * This class  is used as Custom segue for nagvate to home viewController
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */
import UIKit
class HomeSegue: UIStoryboardSegue {
    override func perform() {
        if let navigation = source.navigationController {
           // let vc = mainStoryboard.instantiateViewController(withIdentifier: "home")
            navigation.popToRootViewController(animated: true)
        }
    }
}
