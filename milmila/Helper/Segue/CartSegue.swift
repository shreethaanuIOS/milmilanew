/*
 * CartSegue
 * This class  is used as Custom segue for nagvate to cart viewController
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit
class CartSegue: UIStoryboardSegue {
    override func perform() {
        if let navigation = source.navigationController {
            /// Navigate to cart page
            let vc = CSApplicationConstant.shopStoryboard.instantiateViewController(withIdentifier: "CSCartViewController")
            navigation.pushViewController(vc, animated: true)
        }
    }
}
