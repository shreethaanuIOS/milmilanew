/*
 * Validation constant
 * This class  is used to validataion class for sting
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit
let ADDRESS_ACCEPTABLE_CHARACTERS = "0123456789"

extension String {
    /**
     This method is used Validate a Non Empty Condition
     
     @return bool Value '0' if it is empty  '1' if it is non empty
     */
    
    func validateNotEmpty() -> Bool {
        var string: String = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.isEqual(NSNull()){
            string = ""
        }
        return ((string.characters.count ) != 0)
    }
    
    /**
     This Method Validates a Numerical Digits
     
     @return Bool value
     */
    func numberValidation() -> Bool {
        let regex: String = "^([0-9]*|[0-9]*[.][0-9]*)$"
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        let isValid: Bool = test.evaluate(with: self)
        return isValid
    }
    
    /**
     This method Validates All International Phone Numbers
     
     @return bool value
     */
    func validatePhone() -> Bool {
        let phoneRegex: String = "^((\\+)|(0)|())[0-9]{5,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let phoneValidates: Bool = phoneTest.evaluate(with: self)
        return phoneValidates
    }
    
    /**
     This method is used to append the prices symbols
     
     @return bool value
     */
    func appendPriceSymbol() -> String{
        let price = PRICESSYMBOLS + self
        return price
    }
    
    
    /**
     This method Validates All International Phone Numbers
     
     @return bool value
     */
    func validateEditPhone() -> Bool {
        let phoneRegex: String = "^((\\+)|(0)|())[0-9]{0,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let phoneValidates: Bool = phoneTest.evaluate(with: self)
        return phoneValidates
    }
    
    /**
     This method used to trim empty spaces from string
     
     @return trimmed string
     
     */
    func trimString() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    //  The converted code is limited by 2 KB.
    //  Upgrade your plan to remove this limitation.
    
    /**
     This method is used Validate a Non Special Characters
     
     @param range range tells the no of characters Present
     @return bool value '0'if no special characters '1'if  special characters
     */
    
    func validateNotSpecialChar(_ range: NSRange) -> Bool {
        let cs = CharacterSet(charactersIn: ADDRESS_ACCEPTABLE_CHARACTERS).inverted
        let filtered: String = (components(separatedBy: cs) as NSArray).componentsJoined(by: "")
        return (!(self == filtered))
    }
    
    /**
     This method is used Validate a Minimum length
     
     @param length Total length of a Available Characters
     @return bool value '0' if lenght is not minimum, '1' if length is minimum
     */
    func validateMinimumLength(_ length: Int) -> Bool {
        return (self.characters.count >= length)
    }
    
    /**
     This method is used Validate a Maximum length
     
     @param length Total length of a Available Characters
     @return bool value '0' if lenght is not maximum, '1' if length is maximum
     */
    func validateMaximumLength(_ length: Int) -> Bool {
        return (self.characters.count <= length)
    }
    
    /**
     This method is used Validate a Matches Confirmation
     
     @param confirmation Validatees Matching Confirmation
     @return bool value '0' if confirmed , '1'if not cofirmed
     */
    func validateMatchesConfirmation(_ confirmation: String) -> Bool {
        return (self == confirmation)
    }
    
    /**
     This method is used Validate a URL
     
     @param confirmation Validates Matching Confirmation
      @return bool value
     */
    func validateUrl() -> Bool {
        let urlRegEx = "((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: self)
    }
    
    /**
     This method Validates InCharacterSet is Found or Not
     
     @param characterSet Validate InCharacterSet
     @return bool value '1' if found particular string  , '0' if not found particular string
     */
    func validate(_ characterSet: CharacterSet) -> Bool {
        return ((self as NSString).rangeOfCharacter(from: characterSet.inverted).location == NSNotFound)
    }
    
    
    func isPasswordValid() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
                                       "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&+=_-])[A-Za-z\\d$@$!%*?&+=_-]{6,}")
        return passwordTest.evaluate(with: self)
    }
    
    /**
     This method Validates Available of letters in Characater Set
     
     @return bool value '1'if character is availabble, '0'if character is not available
     */
    func validateAlpha() -> Bool {
        return validate(CharacterSet.letters)
    }
    //  The converted code is limited by 2 KB.
    //  Upgrade your plan to remove this limitation.
    
    //  Converted with Swiftify v1.0.6341 - https://objectivec2swift.com/
    /**
     This method Validates Available of Alpha Numeric Character in the Characater Set
     
     @return bool value '1'if Alphanumeric character is available,'0'if Alphanumeric character is not available
     */
    func validateAlphanumeric() -> Bool {
        return validate(CharacterSet.alphanumerics)
    }
    
    /**
     This method Validates Available of Decimal Digit in Characater Set
     
     @return bool value '1'if Decimal Digit is available, '0'if Decimal Digit is not available
     */
    func validateNumeric() -> Bool {
        return validate(CharacterSet.decimalDigits)
    }
    
    /**
     This method Validates Available of space in a Characater Set
     
     @return bool value '1'if Space is available in Characters,'0'if Space is not available in Characters
     */
    func validateAlphaSpace() -> Bool {
        var characterSet = CharacterSet.letters
        characterSet.insert(charactersIn: " ")
        return validate(characterSet)
    }
    
    /**
     This method Validates Available of space in a Alpa Numeric Characater Set
     
     @return bool value '1'if Space is available in Alpha numeric Character set,'0'if Space is not available in Alpha numeric Character set
     */
    func validateAlphanumericSpace() -> Bool {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: " ")
        return validate(characterSet)
    }
    
    /**
     This method Validates Available of Special Characters in a Alpha Numeric Character Set
     
     @return bool value '1' if special characters avialable in Alpha Numeric Character Set, '0' if special characters is not avialable in Alpha Numeric Character Set
     */
    // Alphanumeric characters, underscore (_), and period (.)
    func validateUsername() -> Bool {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "'_.")
        return validate(characterSet)
    }
    /**
     This method Validates Email
     
     @return bool value '1'if it is a valid Email, '0' if it is not a valid Email
     */
    
    func validateEmail(_ stricterFilter: Bool) -> Bool {
        let stricterFilterString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString: String = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        let emailRegex: String = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
    }
    
    /**
     This method Validates Phone Number
     
     @return bool value '1'if it is a valid Phone Number, '0' if it is not a Valid Phone Number
     */
    
    func validatePhoneNumber() -> Bool {
        var characterSet = CharacterSet.decimalDigits
        characterSet.insert(charactersIn: "'-*+#,;. ")
        return validate(characterSet)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    
    /**
     This method Validates Pin Code
     
     @return bool value '1'if it is a valid Pin Code, '0' if it is not a Valid Pin Code
     */
    func validatePinCode(_ pincode: String) -> Bool {
        let pinRegex: String = "^[0-9]{6}$"
        let pinTest = NSPredicate(format: "SELF MATCHES %@", pinRegex)
        let pinValidates: Bool = pinTest.evaluate(with: pincode)
        return pinValidates
    }
    
    
    /// This method is used to validate the emoji character
    ///
    /// - Returns: return bool value
    func validateEmojis() -> Bool {
        var containsEmoji: Bool = false
        
        (self as NSString).enumerateSubstrings(in: NSMakeRange(0, (self as NSString).length), options: NSString.EnumerationOptions.byComposedCharacterSequences) { (substring, substringRange, enclosingRange, stop) -> () in
            
            let objCString:NSString = NSString(string:substring!)
            let hs: unichar = objCString.character(at: 0)
            if 0xd800 <= hs && hs <= 0xdbff
            {
                if objCString.length > 1
                {
                    let ls: unichar = objCString.character(at: 1)
                    let step1: Int = Int((hs - 0xd800) * 0x400)
                    let step2: Int = Int(ls - 0xdc00)
                    let uc: Int = Int(step1 + step2 + 0x10000)
                    
                    if 0x1d000 <= uc && uc <= 0x1f77f
                    {
                        containsEmoji = true
                    }
                }
            }
            else if objCString.length > 1
            {
                let ls: unichar = objCString.character(at: 1)
                if ls == 0x20e3
                {
                    containsEmoji = true
                }
            }
            else
            {
                if 0x2100 <= hs && hs <= 0x27ff
                {
                    containsEmoji = true
                }
                else if 0x2b05 <= hs && hs <= 0x2b07
                {
                    containsEmoji = true
                }
                else if 0x2934 <= hs && hs <= 0x2935
                {
                    containsEmoji = true
                }
                else if 0x3297 <= hs && hs <= 0x3299
                {
                    containsEmoji = true
                }
                else if hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50
                {
                    containsEmoji = true
                }
            }
        }
        return containsEmoji;
    }
}
extension Date {
    /// This method is used to convert utc to local time
    ///
    /// - Parameter date: date to convert local time
    /// - Returns: return Converted date in string
    func getDateStringFromUTCTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        // change to a readable time format and change to local time zone
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateStyle = .long
        dateFormatter.dateFormat = "MMM dd, YYYY"
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
}
