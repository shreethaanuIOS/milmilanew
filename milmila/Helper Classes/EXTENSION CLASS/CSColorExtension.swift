/*
 * UIColor Extension
 * This class  is used to global declarion of method to all color give class
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit

// Description - this is used to convert Colour Hex colour to RGB colour
extension UIColor{
    
    /// Hexdecimal to normal color
    ///
    /// - Parameters:
    ///   - hexString: give a hexdemical value
    ///   - alpha: can be the density of color
    /// - Returns: uIcolor
    func HexToColor1(_ hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    /// HexaDemial to normal color
    ///
    /// - Parameter hexStr: give a hexdemical value with #
    /// - Returns: int value
    func intFromHexString1(_ hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
    //MARK - Color Extensions
    
    /// For Navigation bar tint color
    class var navigationColor: UIColor {
        return UIColor.white
    }
    /// For Bar button item tint color
    class var barButtonItemColor: UIColor {
        return UIColor.white
    }
    /// For alert Background Color 
    class var notificationBar: UIColor {
        return UIColor.init(red: 250/255, green: 93/255, blue: 93/255, alpha: 1)
    }
    /// For loader  Color
    class var loaderColor: UIColor {
        return UIColor.init(hex: "#CD3F6D")
    }
    
   
}
