/*
 * UIViewController Extension
 * This class  is used to global declarion of method to all view controllers
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit

extension UIViewController {
    
    /// Setting Space
    ///
    /// - Returns:BarButtonItem
    func setSapcer()->UIBarButtonItem{
        let spacer = UIBarButtonItem(barButtonSystemItem:.fixedSpace , target: nil, action: nil)
        spacer.width = 0
        return spacer
    }
    
    func notificationMessageTop(message: String!){
        let view = MDNotificationCompactLayoutView()
        view.textLabel.text = message
        view.textLabel.textAlignment = .center
        view.textLabel.textColor = UIColor.white
        
        let notificationView = MDNotificationView(view: view, position: .top)
        notificationView.backgroundColor = UIColor.notificationBar
        self.view.addSubview(notificationView)
        notificationView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            notificationView.hide()
            notificationView.removeFromSuperview()
        }
    }
    
    
    /// Show toast message position bottom
    ///
    /// - Parameter message: message to be shown
    func showToastMessageBottom(message:String!){
        self.notificationMessageTop(message: message)
    }
    
    /// Show toast message position Top
    ///
    /// - Parameter message: message to be shown
    func showToastMessageTop(message:String!){
        self.notificationMessageTop(message: message)
    }
    
    /// is validating Image url is empty
    ///
    /// - Parameter string: image url
    /// - Returns: returns true
    func isValidateNillAndEmpty(string:String!)->Bool{
        if(string != nil)&&(string != ""){
            return true
        } else {
            return false
        }
    }
    
}

extension UIViewController {
    
    func nagvationControllerWithBackButton(){
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.barTintColor =  UIColor.navigationColor
        self.navigationController?.navigationBar.barTintColor = UIColor.barButtonItemColor
        //Set right and left bar button in the navigation bar
        self.navigationItem.setLeftBarButtonItems([setLeftBarHomeButton()],animated: true)
        self.navigationItem.setRightBarButtonItems([setRightBarSearchBarButton(),setRightBarCartButton()], animated:true)
    }
    
    
    // MARK:Private Methods
    
    /// Create right side button in bar button
    ///
    /// - Returns: Bar button
    private func setRightBarSearchBarButton()->UIBarButtonItem{
        //Creating a right Search Icon
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "search"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.rightSearchBarButtonAction(sender:)), for: .touchUpInside)
        let rightSearchBarButtonItem = UIBarButtonItem(customView: btn1)
        rightSearchBarButtonItem.tintColor=UIColor.barButtonItemColor
        return rightSearchBarButtonItem
    }
    
    /// Create right cart button in bar button
    ///
    /// - Returns: Bar button
    private  func setRightBarCartButton()->UIBarButtonItem{
        //Creating a right Search Icon
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "cart"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.rightCartBarButtonAction(sender:)), for: .touchUpInside)
        let rightSearchBarButtonItem = UIBarButtonItem(customView: btn1)
        rightSearchBarButtonItem.tintColor=UIColor.barButtonItemColor
        return rightSearchBarButtonItem
    }
    
    /// Create left home button in bar button
    ///
    /// - Returns: Bar button
    private func setLeftBarHomeButton()->UIBarButtonItem{
        //Creating a right Search Icon
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "home"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(self.leftHomeBarButtonAction(sender:)), for: .touchUpInside)
        let rightSearchBarButtonItem = UIBarButtonItem(customView: btn1)
        rightSearchBarButtonItem.tintColor=UIColor.white
        return rightSearchBarButtonItem
    }
    
    
    /// Button Action for Search
    ///
    /// - Parameter sender: Any
    func rightSearchBarButtonAction(sender:UIButton) {
        /// navigate to ask alecia page
        let vc = CSApplicationConstant.mainStoryboard.instantiateViewController(withIdentifier: "CSAskAleciaViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    /// Button Action for Cart
    ///
    /// - Parameter sender: Any
    func rightCartBarButtonAction(sender:UIButton) {
        /// Navigate to cart page
        let vc = CSApplicationConstant.shopStoryboard.instantiateViewController(withIdentifier: "CSCartViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    /// Button Action for Home
    ///
    /// - Parameter sender: any
    func leftHomeBarButtonAction(sender:UIButton) {
        /// Navigate to home
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
        self.navigationController!.popToViewController(viewControllers[0], animated: true);
    }
    
}
extension UIImageView {
    
    /// image with Url
    ///
    /// - Parameter imageURl: set the image with url
    /// - Returns: return Uiimage
    func setImageWithUrl(imageURl:String!){
        if(isValidateNillAndEmpty(string: imageURl)){
            let url = URL(string: imageURl.replacingOccurrences(of: " ", with: ""))
            self.af_setImage(withURL: url!,placeholderImage:UIImage(named: "placeholderImage")!)
        } else {
            self.image = UIImage(named: "placeholderImage")!
            return
        }
    }
    
    /// is validating Image url is empty
    ///
    /// - Parameter string: image url
    /// - Returns: returns true
    func isValidateNillAndEmpty(string:String!)->Bool{
        if(string != nil)&&(string != ""){
            return true
        } else {
            return false
        }
    }
}

//// Story board Extra Feature for create border radius, border width and border Color

extension UIView {
    /// corner radius
    @IBInspectable var borderColor:UIColor? {
        set {
            layer.borderColor = newValue!.cgColor
        }
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor:color)
            }
            else {
                return nil
            }
        }
    }
    @IBInspectable var borderWidth:CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius:CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }

}
/// UItextfield max length for textfield
private var kAssociationKeyMaxLength: Int = 0

extension UITextField {
    
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    
    func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.characters.count > maxLength
            else {
                return
        }
        
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
}
