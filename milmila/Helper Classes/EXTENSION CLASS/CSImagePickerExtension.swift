/*
 * UIImagePickerController Extension
 * This class  is used to global declarion of method to all give class
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import Foundation
import UIKit
import MobileCoreServices


