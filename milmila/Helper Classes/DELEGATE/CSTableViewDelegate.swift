/*
 * CSTableViewDelegate
 * This class  is used as a delegate class for all the tableview class
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */
import UIKit
protocol CSTableViewDelegate:class {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
}
