/*
 * CSCollectionViewDelegate
 * This class  is used as a delegate class for all the collectionview class
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */
import UIKit
@objc protocol CSCollectionDelegate:class {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
  @objc optional func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
}
