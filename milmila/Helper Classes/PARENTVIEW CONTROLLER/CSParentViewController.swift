

import UIKit
import Foundation
import NVActivityIndicatorView
#if os(iOS)
#endif
class CSParentViewController: UIViewController, UIViewControllerTransitioningDelegate,UITabBarControllerDelegate, NVActivityIndicatorViewable {
    
    
    ///MARK:- UIViewController Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.transitioningDelegate = self;
        self.tabBarController?.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        CSSharedData.SharedInstance.visibleController = self
        
    }
    
    
    /// add addChildView
    ///
    /// - Parameters:
    ///   - identifier: identifiers for child view controller
    ///   - storyBoard: child view controller story board
    func addChildView( identifier: String ,storyBoard: UIStoryboard) {
        let controller = storyBoard.instantiateViewController(withIdentifier: identifier)
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
    }
    #if os(iOS)
    /// updateCart Button Action
    ///
    /// - Parameters:
    ///   - selectedCartContentList: product details
    ///   - updateCart: change title of updateButton
//    func addChildViewForProductDescription(selectedCartContentList: CartList! ,updateCart: Bool, product: product!){
//
//        let controller = CSApplicationConstant.shopStoryboard.instantiateViewController(withIdentifier: "") as! CSProductDescriptionViewController
//
//        controller.updateCartIsTrue = updateCart
//
//        if(updateCart == true){
//            controller.editCartContentList = selectedCartContentList
//        } else {
//            controller.productList = product
//        }
//        controller.view.frame = self.view.bounds;
//        controller.willMove(toParentViewController: self)
//        self.view.addSubview(controller.view)
//        self.addChildViewController(controller)
//        controller.didMove(toParentViewController: self)
//    }
    
    /// This method is used to start the loader
    func startAnimating(){
        let size = CGSize(width: 50, height: 50)
        let message = NSLocalizedString("Loading...", comment: "")
        startAnimating(size,message:message, type: NVActivityIndicatorType(rawValue: 3)!,color:UIColor.loaderColor,backgroundColor:UIColor.white,textColor:UIColor.black)
    }
    
    /// This method is used to remove the child view from the parent
    ///
    /// - Parameter controller: controller to remove from parent
    func removeChildView( controller: UIViewController) {
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    /// This method is used to call the api
    func callApi(){
        
    }
    
    
    /// This method to stop the loader
    func stopAnimate(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()) {
            self.stopAnimate()
        }
    }
    
    /// Open Login Screen If User is not Sigin
    func openLoginScreenPopUp(viewController: UIViewController){
        if( CSSharedData.SharedInstance.getAccessToken() != "" )/*IF USER ALL READY LOGIN */{
            callApi()
            return
        }
        ///IF USER IS NOT LOGIN
        let controller = CSApplicationConstant.mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        controller.delegate = viewController as? loginSuccess
        controller.view.frame = self.view.bounds;
        controller.willMove(toParentViewController: self)
        self.view.addSubview(controller.view)
        self.addChildViewController(controller)
        controller.didMove(toParentViewController: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /// Default tabBarController delegate
    ///
    /// - Parameters:
    ///   - tabBarController: Tab bar controller
    ///   - viewController: current view items
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
//        if(tabBarController.selectedIndex != 0) {
//            if( CSSharedData.SharedInstance.getAccessToken() != "" ) {
//                if(viewController.childViewControllers.first?.childViewControllers.first is CSLoginViewController){
//                   let parentController = viewController.childViewControllers.first as! CSParentViewController
//                    removeChildView(controller:viewController.childViewControllers.first!.childViewControllers.first!)
//                    parentController.callApi()
//                }
//            }
//        }
    }
    /// add child View
    ///
    /// - Parameter controller:to add view
    func callLoginPopUp(controller: UIViewController) {
//        let controllerView = CSApplicationConstant.loginStoryboard.instantiateViewController(withIdentifier: CSApplicationConstant.loginViewControllerIdentifier) as! CSLoginViewController
//        controllerView.delegate = controllerView as? loginSuccess
//        controllerView.view.frame = self.view.bounds;
//        controllerView.willMove(toParentViewController: self)
//        controller.view.addSubview(controllerView.view)
//        controller.addChildViewController(controllerView)
//        controllerView.didMove(toParentViewController: self)
    }
    #endif
}
