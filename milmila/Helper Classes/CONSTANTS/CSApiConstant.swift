/*
 * CSApiConstant
 * This class  is used to declare all the application constant
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit


/// Detiled list

let DETAILPRODUCTVIEW = "mmb2b/api/product/details?p=223&_=1509947167912"
/// login reqest
let LOGINAPI = "web/ecommerce/api/v1/login"

/// user profile image update
let USERIMAGEUPLOADAPI = "web/ecommerce/api/v1/customer_image"

/// fetch user details
let USERDETAILSAPI = "web/ecommerce/api/v1/customer"

/// edit user details
let EDITUSERDETAILSAPI = "web/ecommerce/api/v1/customer_edit"

///  Home
let HOMEAPI = "vplay/api/v1/get_home"

/// listing place city
let PLACEAPI = "web/ecommerce/api/v1/places"

/// Video Detail Page
let VIDEODETAIL = "/vplay/api/v1/watch/"

/// Cart page
let CARTAPI = "web/ecommerce/api/v1/cart_items"

/// add to cart
let ADDCARTAPI = "/web/ecommerce/api/v1/add_to_cart"

/// Order List
let ORDERLISTAPI = "web/ecommerce/api/v1/customer_orders"

/// User Registration
let REGAPI = "/web/ecommerce/api/v1/customer"

/// Comment Api details
let COMMENTAPT = "/vplay/api/v1/comments/"

/// Delet Cart
let DELETECARTAPI = "web/ecommerce/api/v1/remove_cart/"

/// like videos
let FAVOURITEAPI = "vplay/api/v1/like/"

/// Place Order Api
let PLACEORDERAPI = "web/ecommerce/api/v1/order"

/// share api
let SHAREVIDEOURL = "vplay/api/v1/share/"

/// Change Password Api
let CHANGEPASSWORD = "web/ecommerce/api/v1/change_password"

