/*
 * CSApplicationConstant
 * This class  is used to declare all the application constant
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit
struct CSApplicationConstant {
/// declare constant for main storyboard
static let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)

/// declare constant for dashboard storyboard
static let dashStoryboard : UIStoryboard = UIStoryboard(name: "Dash", bundle: nil)

/// declare constant for live storyboard
static let liveStoryboard : UIStoryboard = UIStoryboard(name: "Live", bundle: nil)

/// declare constant for profile storyboard
static let profileStoryboard : UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)

/// declare constant for shows storyboard
static let showStoryboard : UIStoryboard = UIStoryboard(name: "Shows", bundle: nil)

/// declare constant for social storyboard
static let socialStoryboard : UIStoryboard = UIStoryboard(name: "Social", bundle: nil)

/// declare constant for shops storyboard
static let shopStoryboard : UIStoryboard = UIStoryboard(name: "Shop", bundle: nil)

/// declare constant for menu storyboard
static let menuStoryboard : UIStoryboard = UIStoryboard(name: "Menu", bundle: nil)

/// declare constant for main storyboard
static let orderStoryboard : UIStoryboard = UIStoryboard(name: "Order", bundle: nil)

/// declare constant for accounts storyboard
static let AccountStoryboard : UIStoryboard = UIStoryboard(name: "Account", bundle: nil)

/// declare constant for wishlist storyboard
static let wishListStoryboard : UIStoryboard = UIStoryboard(name: "WishList", bundle: nil)

/// declare constant for login storyboard
static let loginStoryboard : UIStoryboard = UIStoryboard(name: "Login", bundle: nil)

/// declare constant for Alert storyboard
static let CSAlertStoryboard : UIStoryboard = UIStoryboard(name: "Alert", bundle: nil)

/// declare identifers for uploadViewContoller
static let uploadViewControllerIdentifier:String = "CSUploadVideoViewController"
    
/// declare identifers for congratulationViewContoller
static let congratulationViewControllerIdentifier:String = "CSCongratulationsViewController"

/// identifers menuViewContoller 
static let menuViewControllerIdentifier:String = "CSMenuListViewController"

/// identifers productViewContoller
static let productControllerIdentifier:String = "CSProductDescriptionViewController"

/// identifers productViewContoller
static let addToCartControllerIdentifier:String = "CSAddToCartViewController"

/// identifers cartUpdateViewController
static let cartUpdateControllerIdentifier:String = "CSCartUpdateViewController"

/// identifers loginViewController
static let loginViewControllerIdentifier:String = "CSLoginViewController"

/// identifers NodataView
static let CSNoDataViewIdentifier:String = "CSNoDataView"

/// identifers NointernetView
static let CSNoInternetViewIdentifier:String = "CSNoInternet"

/// identifers NointernetView
static let CSWhoopsViewIdentifier:String = "CSWhoopsView"
    
/// identifers NointernetView
static let CSNoOrderViewIdentifier:String = "CSNoOrderItemView"

/// identifers NointernetView
static let CSNoCartViewIdentifier:String = "CSNoCartItemView"
    
/// identifers VideoCategory collection in Comments
static let CSPostCategoryCellIdentifier:String = "CSPostCollectionIdentifier"

/// identifers CommentTextCell tableview
static let CSTextCommentCellIdentifier:String = "CSTextCommentCellIdentifier"
    
/// identifers UserListCategory tableview
static let CSListCategoryTableIdentifier:String = "CSListCategoryTableIdentifier"
    
/// identifers CSCardListTableIdentifier tableview
static let CSCardListTableIdentifier:String = "CSCardListTableIdentifier"
    
/// identifers profile cell tableview
static let CSProfileMenuListCellIdentifier:String = "CSProfileMenuListCell"
 
/// identifers Series tableview
static let CSSeriesDetailIdentifier:String = "SeriesDetail"
    
/// identifers Series tableview
static let CSUploadViewIdentifier:String = "CSUploadViewIdentifier"

/// declare constant for UIView for edit upload
static var isEditVideoUploadView = false

/// Login Segue Identifier
static let CSLoginSegueViewIdentifier:String = "Loginsegue"
    
/// Login Segue Identifier
static let CSSeriesDetailSegueViewIdentifier:String = "seriesDetailSegue"
    
    
/// country parameter Check
static let paramCountry = "country_id="

/// state parameter Check 
static let paramState = "state_id="
}

struct AlertIdentifiers {
    /// Alert Storyboard
    static let CSAlertStoryBoard: UIStoryboard =
        UIStoryboard(name: "TVAlertView", bundle: nil)
    /// No Data Identifer
    static let CSNoDataIdentifier: String = "CSNoDataView"
    /// No Internet Identifer
    static let CSNoInternetIdentifier: String = "CSNoInternet"
    /// No Video Identifer
    static let CSNoVideoIdentifier: String = "CSVideoNotAvailable"
    /// No Something wentWorng Identifer
    static let CSWhoopsIdentifier: String = "CSWhoopsView"
}
