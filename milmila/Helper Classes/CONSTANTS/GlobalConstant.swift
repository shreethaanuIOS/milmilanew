/*
 * GlobalConstant
 * This class  is used to declare all the constant of api value 
 * @category   milmila
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     milmila Team
 * @copyright  Copyright (C) 2017 milmila. All rights reserved.
 */

import UIKit
import Foundation

/// Base Url constant for Entire App
let PRICESSYMBOLS = " $ "
let USERDEFAULTS = Foundation.UserDefaults.standard
struct GlobalConstants {
    
    //MARK: Login
    struct Login {
        static let CSLoginKey                   = "alreadyLoggedInKey"
        static let CSEmail                      = "email"
        static let CSPassword                   = "password"
        static let CSUserToken                  = ""
        static let CSUserName                   = "userName"
        static let CSImage                      = "image"
        static let CSId                         = "user_id"
        static let CSProfilePicture             = ""
        static let CSMobile                     = "mobile"
        static let CSGender                     = "gender"
        static let CSIsApproved                 = "isApproved"
        static let CSIsActive                   = "isActive"
        static let csaddress                    = "csaddress"
        static let csstreet                     = "csstreet"
        static let csstateee                     = "csstateee"
        static let cscountry                     = "cscountry"
        static let cszip                         = "cszip"
        static let csaddressname                 = "csname"
        static let csphonenumber                 = "csphonenumber"
        static let cslastname                    = "cslastname"
        static let addressof                     = "addressof"
        
        
        
        
        
    }
    struct General {
        static let USERDEFAULTS                 = Foundation.UserDefaults.standard
    }
}
