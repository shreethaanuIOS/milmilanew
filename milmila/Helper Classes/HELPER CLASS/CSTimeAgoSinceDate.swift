 /*
  * CSTimeAgoSinceDate
  * This class  is used to display time
  * @category   Alecia
  * @package    com.contus.Alecia
  * @version    1.0
  * @author     Contus Team <developers@contus.in>
  * @copyright  Copyright (C) 2017 Contus. All rights reserved.
  */
 
 import Foundation
 
 class CSTimeAgoSinceDate:NSObject{
    /// min ago hours ago second ago etc calculated over her
    ///
    /// - Parameters:
    ///   - date: date string
    ///   - currentDate: now date
    ///   - numericDates: return true or false
    /// - Returns: date in min ago hours ago second
    func timeAgoSinceDate(_ date:String,currentDate:Date, numericDates:Bool) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let converteddate = dateFormatter.date(from: date)
        
        
        let calendar = Calendar.current
        let now = currentDate
        let earliest = (now as NSDate).earlierDate(converteddate!)
        let latest = (earliest == now) ? converteddate! : now
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest, options: NSCalendar.Options())
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
    }
    
    /// date convertor
    ///
    /// - Parameter date: date in string "yyyy-MM-dd HH:mm:ss"
    /// - Returns: "dd MMM"
    func convertDateFormatter(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM"///this is you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
    /// nextlive
    func convertToDate(date: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        return date!
    }
    /// month date year format
    func convertMonthDateYearFormate(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "MMM dd YYYY, h:mm a"///this is you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    /// nextlive
    func convertDateFormatterOfMMMAndYear(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM YYYY"///this is you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    ///  notification time convertor
    ///
    /// - Parameter date: date string "yyyy-MM-dd HH:mm:ss"
    /// - Returns: "dd MMM YYYY, HH:MM a" string
    func convertDateNotificationFormatter(date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"//this your string date format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM YYYY, h:mm a"///this is you want to convert format
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        let timeStamp = dateFormatter.string(from: date!)
        return timeStamp
    }
    
   /// Class method for date set
   ///
   /// - Returns: return date in array
   class func currentDateToNextSevenDays()-> [Dictionary<String, Any>]! {
        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())
        var dateList = [Dictionary<String, Any>!]()
        for _ in 1 ... 7 {
            let day = cal.component(.day, from: date)
            var dateAndMonth = Dictionary<String, Any>()
            dateAndMonth["date"] = "\(day)"
            dateAndMonth["month"] = date.getMonthName()
            dateList.append(dateAndMonth)
            date = cal.date(byAdding: .day, value: 1, to: date)!
        }
        return dateList
    }
 }
 
 extension Date {
    func getMonthName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let strMonth = dateFormatter.string(from: self)
        return strMonth
    }
 }
