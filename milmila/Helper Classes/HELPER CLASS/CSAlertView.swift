 /*
  * CSTimeAgoSinceDate
  * This class  is used to display time
  * @category   Alecia
  * @package    com.contus.Alecia
  * @version    1.0
  * @author     Contus Team <developers@contus.in>
  * @copyright  Copyright (C) 2017 Contus. All rights reserved.
  */
 
 
 /// Alert Constants
 
 /// Name of title font in bold
 let alertTitleFont = "HelveticaNeue-Bold"
 
 /// Name of Message font in regular
 let alertMessageFont = "HelveticaNeue"
 
 /// size of font in message and title
 let alertFontSize: CGFloat = 16.0
 
 
 import UIKit
 /// alert delegate
 protocol AlertDelegate{
    func didSelectedAlertView(_ tag:Int,buttonIndex:Int)
 }
 /// class create to show alert
 class AlertView  {
    
    static var instance = AlertView()
    
    /// attribute Title key
    let alertAttributeTitle = "attributedTitle"
    
    /// attribute Message key
    let alertAttributeMessage = "attributedMessage"
    
    ///  change alert Title font size text
    ///
    /// - Parameter title: string to be changed
    /// - Returns: return a attribute strung
    func alertTitleAttribute(title: String!)->NSMutableAttributedString{
        let attributedTitle = NSMutableAttributedString(string:title, attributes:[NSFontAttributeName: UIFont(name: alertTitleFont, size: alertFontSize)!, NSForegroundColorAttributeName : UIColor.black])
        return attributedTitle
    }
    
    ///  change alert Message font size text
    ///
    /// - Parameter message: string to be changed
    /// - Returns: return a attribute strung
    func alertMessageAttribute(message: String!)->NSMutableAttributedString{
        let attributedMessage = NSMutableAttributedString(string:message, attributes:[NSFontAttributeName: UIFont(name: alertMessageFont, size: alertFontSize)!, NSForegroundColorAttributeName : UIColor.black])
        return attributedMessage
    }
    
    /// show alert with out completion block
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - btnTitleOne: ok or cancel buttion accord to type
    ///   - btnTitleTwo: ok or cancel buttion accord to type
    ///   - tag: button tag
    ///   - alertDelegate: setting delegate
    ///   - view: show in with uiview controller
    func showAlertView(_ title: String, message: String,btnTitleOne: String,btnTitleTwo: String,tag: Int, alertDelegate: AlertDelegate, view:UIViewController) {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.black
        alertView.setValue(alertTitleAttribute(title: title), forKey: alertAttributeTitle)
        alertView.setValue(alertMessageAttribute(message: message), forKey: alertAttributeMessage)
        
        
        alertView.addAction(UIAlertAction(title: btnTitleOne, style: .default, handler: { (alertAction) -> Void in
            alertDelegate.didSelectedAlertView(tag, buttonIndex: 0)
        }))
        if !btnTitleTwo.isEmpty {
            alertView.addAction(UIAlertAction(title: btnTitleTwo, style: .cancel, handler: { (alertAction) -> Void in
            }))
        }
        DispatchQueue.main.async(execute: {
            view.present(alertView, animated: true, completion: nil)
        })
    }
    
    /// show alert with completion block
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - btnTitleOne: ok or cancel buttion accord to type
    ///   - btnTitleTwo: ok or cancel buttion accord to type
    ///   - tag: button tag
    ///   - alertDelegate: setting delegate
    ///   - view: show in with uiview controller
    
    func showAlertViewWithBlock(_ title: String, message: String,btnTitleOne: String,btnTitleTwo: String, view:UIViewController, completionOk:@escaping () -> Void, cancel:@escaping () -> Void) {
        
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertView.view.tintColor = UIColor.black
        
        alertView.setValue(alertTitleAttribute(title: title), forKey: alertAttributeTitle)
        alertView.setValue(alertMessageAttribute(message: message), forKey: alertAttributeMessage)
        
        
        let buttonOne = UIAlertAction(title: btnTitleOne, style: .default, handler: { (alertAction) -> Void in
            completionOk()
        })
        buttonOne.setValue(UIColor.red, forKey: "titleTextColor")
        alertView.addAction(buttonOne)
        
        if !btnTitleTwo.isEmpty {
            alertView.addAction(UIAlertAction(title: btnTitleTwo, style: .cancel, handler: { (alertAction) -> Void in
                cancel()
            }))
        }
        
        DispatchQueue.main.async(execute: {
            
            view.present(alertView, animated: true, completion: nil)
        })
    }
    
    /// show alert with login
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - message: alert message
    ///   - btnTitleOne: ok or cancel buttion accord to type
    ///   - btnTitleTwo: ok or cancel buttion accord to type
    ///   - tag: button tag
    ///   - alertDelegate: setting delegate
    ///   - view: show in with uiview controller
    
    func showAlertViewWithLoading(_ title: String,  tag: Int, alertDelegate: AlertDelegate, view:UIViewController) {
        
        let alertView = UIAlertController(title: title, message:"", preferredStyle: .alert)
        alertView.view.tintColor = UIColor.black
        alertView.setValue(alertTitleAttribute(title: title), forKey: alertAttributeTitle)
        let loadingWheel = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        loadingWheel.frame = CGRect(x: alertView.view.frame.size.width-16/2, y: alertView.view.frame.size.height-16, width: 16, height: 16)
        loadingWheel.startAnimating()
        alertView.view.addSubview(loadingWheel)
        
        view.present(alertView, animated: true, completion: nil)
    }
 }
