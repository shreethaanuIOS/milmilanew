//
//  stepsClass.swift
//  milmila
//
//  Created by prasun sarkar on 12/22/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import Foundation
class Step {
    var stepValue = ""
    
    convenience init(_ value:String) {
        self.init()
        self.stepValue=value
    }
}

protocol StepDelegate {
    func stepValueChanged(step:Step, newValue:String) -> Void
}


