/*
 * CSCustomImageView
 * This class  is used as parent class for all Image View
 * @category   Alecia
 * @package    com.contus.Alecia
 * @version    1.0
 * @author     Contus Team <developers@contus.in>
 * @copyright  Copyright (C) 2017 Contus. All rights reserved.
 */

import UIKit

class CSCustomImageView: UIImageView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         let shadowSize : CGFloat = 5.0
         let shadowPath = UIBezierPath(rect: CGRect(x: -shadowSize / 2,
         y: -shadowSize / 2,
         width: self.frame.size.width + shadowSize,
         height: self.frame.size.height + shadowSize))
         self.layer.masksToBounds = false
         self.layer.shadowColor = UIColor.black.cgColor
         self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
         self.layer.shadowOpacity = 0.05
         self.layer.shadowRadius = 10.0
         self.layer.cornerRadius = 10.0
         self.layer.shadowPath = shadowPath.cgPath
    }
    
}
