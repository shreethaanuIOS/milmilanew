//
//  ValidationExtension.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
let ADDRESSACCEPTABLECHARACTERS = "0123456789"
extension String {
    /**
     This method is used Validate a Non Empty Condition
     @return bool Value '0' if it is empty  '1' if it is non empty
     */
    func validateNotEmpty() -> Bool {
        var string: String = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if string.isEqual(NSNull()) {
            string = ""
        }
        return ((string.characters.count ) != 0)
    }
    /**
     This Method Validates a Numerical Digits
     @return Bool value
     */
    func numberValidation() -> Bool {
        let regex: String = "^([0-9]*|[0-9]*[.][0-9]*)$"
        let test = NSPredicate(format: "SELF MATCHES %@", regex)
        let isValid: Bool = test.evaluate(with: self)
        return isValid
    }
    /**
     This method Validates All International Phone Numbers
     @return bool value
     */
    func validatePhone() -> Bool {
        let phoneRegex: String = "^((\\+)|(0)|())[0-9]{5,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        let phoneValidates: Bool = phoneTest.evaluate(with: self)
        return phoneValidates
    }
    /**
     This method is used to append the prices symbols
     @return bool value
     */
    func appendPriceSymbol() -> String {
        if !self.validateNotEmpty() {
            let price = PRICESSYMBOLS + "0.00"
            return price
        }
        let price = PRICESSYMBOLS + self
        return price
    }
    /**
     This method is used to remove the html string
     @return string
     */
    func removeHtmlFromString() -> String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    /**
     This method used to trim empty spaces from string
     @return trimmed string
     */
    func trimString() -> String {
        return trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    /**
     This method is used Validate a Matches Confirmation
     @param confirmation Validatees Matching Confirmation
     @return bool value '0' if confirmed , '1'if not cofirmed
     */
    func validateMatchesConfirmation(_ confirmation: String) -> Bool {
        return (self == confirmation)
    }
    /**
     This method is used Validate a URL
     @param confirmation Validates Matching Confirmation
     @return bool value
     */
    func validateUrl() -> Bool {
        var isValid = false
        if !self.contains("...") {
            let head       = "((http|https)://)?([(w|W)]{3}+\\.)?"
            let tail       = "\\.+[A-Za-z]{2,3}+(\\.)?+(/(.)*)?"
            let urlRegEx = head+"+(.)+"+tail
            let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
            isValid = urlTest.evaluate(with: self.trimString())
        }
        return isValid
    }
    /**
     This method Validates InCharacterSet is Found or Not
     @param characterSet Validate InCharacterSet
     @return bool value '1' if found particular string  , '0' if not found particular string
     */
    func validate(_ characterSet: CharacterSet) -> Bool {
        return ((self as NSString).rangeOfCharacter(from: characterSet.inverted).location == NSNotFound)
    }
    /**
     This method Validates Available of Special Characters in a Alpha Numeric Character Set
     @return bool value '1' if special characters avialable in Alpha Numeric Character Set, '0' if special characters
     is not avialable in Alpha Numeric Character Set
     */
    // Alphanumeric characters, underscore (_), and period (.)
    func validateUsername() -> Bool {
        var characterSet = CharacterSet.alphanumerics
        characterSet.insert(charactersIn: "'_.")
        return validate(characterSet)
    }
    /**
     This method Validates Email
     @return bool value '1'if it is a valid Email, '0' if it is not a valid Email
     */
    func validateEmail(_ stricterFilter: Bool) -> Bool {
        let stricterFilterString: String = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let laxString: String = ".+@.+\\.[A-Za-z]{2}[A-Za-z]*"
        let emailRegex: String = stricterFilter ? stricterFilterString : laxString
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        return emailTest.evaluate(with: self)
    }
    /**
     This method Validates Password
     -Start Anchor. ^
     -Must Contain at Least one uppercase letters.  (?=.*[A-Z])
     -Must Contain at Least one digits .  (?=.*[0-9])
     -Must Contain at Least one special character.  (?=.[$@$#!%?&])
     -password length is 6. {6,}
     @return bool value '1'if it is a valid Password, '0' if it is not a valid Password
     */
    func isPasswordValid() -> Bool {
        let passwordTest = NSPredicate(format: "SELF MATCHES %@",
                                       "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&+=_-])[A-Za-z\\d$@$!%*?&+=_-]{6,}")
        return passwordTest.evaluate(with: self)
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String)
        -> Bool {
            let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: aSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return string == numberFiltered
    }
    
}
extension Date {
    /// This method is used to convert utc to local time
    ///
    /// - Parameter date: date to convert local time
    /// - Returns: return Converted date in string
    func getDateStringFromUTCTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone!
        // change to a readable time format and change to local time zone
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.dateStyle = .long
        dateFormatter.dateFormat = "MMM dd, YYYY"
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
}

