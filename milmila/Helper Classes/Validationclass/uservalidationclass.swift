//
//  uservalidationclass.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class CSUserDetailValidation: NSObject {
    /// Name Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateNameInNormalTextField(textField: AnyObject, viewController: UIViewController) -> Bool {
        let Name = textField as? UITextField
        if Name?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter a name", comment: "error message"))
            return false
        }
        if (Name?.text?.characters.count)! < 3 {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Name should be minium of 3 Characters",
                                  comment: "Name"))
            return false
        }
        return true
    }
    class func validateName(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let Name = textField as? SkyFloatingLabelTextField
        if Name?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a name", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a billing name", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a shipping name", comment: "error message"))
            }
            return false
        }
        if (Name?.text?.characters.count)! < 3 {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Name should be minium of 3 Characters",
                                      comment: "Name"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Billing name should be minium of 3 Characters",
                                      comment: "Name"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Shipping name should be minium of 3 Characters",
                                      comment: "Name"))
            }
            return false
        }
        return true
    }
    /// validateEmail Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateEmail(textField: AnyObject, viewController: UIViewController) -> Bool {
        let email = textField as? SkyFloatingLabelTextField
        if email?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your E-mail id",
                                  comment: "Email"))
            return false
        }
        if !((email?.text?.validateEmail(true))!) {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter a vaild E-mail id",
                                  comment: "Email"))
            return false
        }
        return true
    }
    /// validateEmailForNormalTextField Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateEmailForNormalTextField(textField: AnyObject, viewController: UIViewController) -> Bool {
        let email = textField as? UITextField
        if email?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your E-mail id",
                                  comment: "Email"))
            return false
        }
        if !((email?.text?.validateEmail(true))!) {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter a vaild E-mail id",
                                  comment: "Email"))
            return false
        }
        return true
    }
    /// validate Password Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validatePassword(textField: AnyObject, viewController: UIViewController) -> Bool {
        let password = textField as? SkyFloatingLabelTextField
        if password?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Password field is required",
                                  comment: "password"))
            return false
        }
        if (password?.text?.characters.count)! < 6 {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Password must be atleast 6 Characters",
                                  comment: "password"))
            return false
        }
        return true
    }
    /// Last Name Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateLastName(textField: AnyObject, viewController: UIViewController) -> Bool {
        let lastName = textField as? SkyFloatingLabelTextField
        if lastName?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter a last name", comment: "error message"))
            return false
        }
        return true
    }
    /// Website Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateWebsite(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let Website = textField as? SkyFloatingLabelTextField
        if checkType == 0 {
            if Website?.text?.validateNotEmpty() == false {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a website url", comment: "error message"))
                return false
            }
            if Website?.text?.validateUrl() == false {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a valid website url", comment: "error message"))
                return false
            }
        }
        if checkType == 1 {
            if Website?.text?.validateNotEmpty() == false {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a facebook url",
                                      comment: "error message"))
                return false
            }
            if Website?.text?.validateUrl() == false ||
                Website?.text?.lowercased().range(of:"facebook")  == nil {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a valid facebook url",
                                      comment: "error message"))
                return false
            }
        }
        if checkType == 2 {
            if Website?.text?.validateNotEmpty() == false {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a twitter url",
                                      comment: "error message"))
                return false
            }
            if Website?.text?.validateUrl() == false ||
                Website?.text?.lowercased().range(of:"twitter")  == nil {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a valid twitter url",
                                      comment: "error message"))
                return false
            }
        }
        return true
    }
    /// birthPlace Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateBirthPlace(textField: AnyObject, viewController: UIViewController) -> Bool {
        let birthPlace = textField as? SkyFloatingLabelTextField
        if birthPlace?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your birth place", comment: "error message"))
            return false
        }
        return true
    }
    /// birthDay Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateBirthDay(textField: AnyObject, viewController: UIViewController) -> Bool {
        let birthDay = textField as? SkyFloatingLabelTextField
        class CSUserDetailValidation: NSObject {
            /// Name Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateNameInNormalTextField(textField: AnyObject, viewController: UIViewController) -> Bool {
                let Name = textField as? UITextField
                if Name?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter a name", comment: "error message"))
                    return false
                }
                if (Name?.text?.characters.count)! < 3 {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Name should be minium of 3 Characters",
                                          comment: "Name"))
                    return false
                }
                return true
            }
            class func validateName(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
                let Name = textField as? SkyFloatingLabelTextField
                if Name?.text?.validateNotEmpty() == false {
                    if checkType == 0 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a name", comment: "error message"))
                    }
                    if checkType == 1 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a billing name", comment: "error message"))
                    }
                    if checkType == 2 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a shipping name", comment: "error message"))
                    }
                    return false
                }
                if (Name?.text?.characters.count)! < 3 {
                    if checkType == 0 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Name should be minium of 3 Characters",
                                              comment: "Name"))
                    }
                    if checkType == 1 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Billing name should be minium of 3 Characters",
                                              comment: "Name"))
                    }
                    if checkType == 2 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Shipping name should be minium of 3 Characters",
                                              comment: "Name"))
                    }
                    return false
                }
                return true
            }
            /// validateEmail Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateEmail(textField: AnyObject, viewController: UIViewController) -> Bool {
                let email = textField as? SkyFloatingLabelTextField
                if email?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your E-mail id",
                                          comment: "Email"))
                    return false
                }
                if !((email?.text?.validateEmail(true))!) {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter a vaild E-mail id",
                                          comment: "Email"))
                    return false
                }
                return true
            }
            /// validateEmailForNormalTextField Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateEmailForNormalTextField(textField: AnyObject, viewController: UIViewController) -> Bool {
                let email = textField as? UITextField
                if email?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your E-mail id",
                                          comment: "Email"))
                    return false
                }
                if !((email?.text?.validateEmail(true))!) {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter a vaild E-mail id",
                                          comment: "Email"))
                    return false
                }
                return true
            }
            /// validate Password Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validatePassword(textField: AnyObject, viewController: UIViewController) -> Bool {
                let password = textField as? SkyFloatingLabelTextField
                if password?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Password field is required",
                                          comment: "password"))
                    return false
                }
                if (password?.text?.characters.count)! < 6 {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Password must be atleast 6 Characters",
                                          comment: "password"))
                    return false
                }
                return true
            }
            /// Last Name Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateLastName(textField: AnyObject, viewController: UIViewController) -> Bool {
                let lastName = textField as? SkyFloatingLabelTextField
                if lastName?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter a last name", comment: "error message"))
                    return false
                }
                return true
            }
            /// Website Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateWebsite(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
                let Website = textField as? SkyFloatingLabelTextField
                if checkType == 0 {
                    if Website?.text?.validateNotEmpty() == false {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a website url", comment: "error message"))
                        return false
                    }
                    if Website?.text?.validateUrl() == false {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a valid website url", comment: "error message"))
                        return false
                    }
                }
                if checkType == 1 {
                    if Website?.text?.validateNotEmpty() == false {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a facebook url",
                                              comment: "error message"))
                        return false
                    }
                    if Website?.text?.validateUrl() == false ||
                        Website?.text?.lowercased().range(of:"facebook")  == nil {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a valid facebook url",
                                              comment: "error message"))
                        return false
                    }
                }
                if checkType == 2 {
                    if Website?.text?.validateNotEmpty() == false {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a twitter url",
                                              comment: "error message"))
                        return false
                    }
                    if Website?.text?.validateUrl() == false ||
                        Website?.text?.lowercased().range(of:"twitter")  == nil {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a valid twitter url",
                                              comment: "error message"))
                        return false
                    }
                }
                return true
            }
            /// birthPlace Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateBirthPlace(textField: AnyObject, viewController: UIViewController) -> Bool {
                let birthPlace = textField as? SkyFloatingLabelTextField
                if birthPlace?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your birth place", comment: "error message"))
                    return false
                }
                return true
            }
            /// birthDay Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateBirthDay(textField: AnyObject, viewController: UIViewController) -> Bool {
                let birthDay = textField as? SkyFloatingLabelTextField
                if birthDay?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter a your date of Birth", comment: "error message"))
                    return false
                }
                return true
            }
            /// Occupation Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateOccupation(textField: AnyObject, viewController: UIViewController) -> Bool {
                let occupation = textField as? SkyFloatingLabelTextField
                if occupation?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your occupation", comment: "error message"))
                    return false
                }
                return true
            }
            /// Married Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateMarried(textField: AnyObject, viewController: UIViewController) -> Bool {
                let married = textField as? SkyFloatingLabelTextField
                if married?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your married status", comment: "error message"))
                    return false
                }
                return true
            }
            /// Gender Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validateGender(textField: AnyObject, viewController: UIViewController) -> Bool {
                let married = textField as? SkyFloatingLabelTextField
                if married?.text?.validateNotEmpty() == false {
                    viewController.showToastMessageTop(message:
                        NSLocalizedString("Please enter your gender", comment: "error message"))
                    return false
                }
                return true
            }
            /// Phone Validation
            ///
            /// - Parameter textField: textfield
            /// - Returns: true or false
            class func validatePhone(textField: AnyObject, viewController: UIViewController) -> Bool {
                let phone = textField as? SkyFloatingLabelTextField
                if (phone?.text?.validateNotEmpty())! {
                    if (phone?.text!.characters.count)! < 6 {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Phone number must be atleast 6 Characters",
                                              comment: "error message"))
                        return false
                    }
                    if phone?.text?.validatePhone() == false {
                        viewController.showToastMessageTop(message:
                            NSLocalizedString("Please enter a valid phone Number", comment: "error message"))
                        return false
                    }
                }
                return true
            }
        }

        if birthDay?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter a your date of Birth", comment: "error message"))
            return false
        }
        return true
    }
    /// Occupation Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateOccupation(textField: AnyObject, viewController: UIViewController) -> Bool {
        let occupation = textField as? SkyFloatingLabelTextField
        if occupation?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your occupation", comment: "error message"))
            return false
        }
        return true
    }
    /// Married Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateMarried(textField: AnyObject, viewController: UIViewController) -> Bool {
        let married = textField as? SkyFloatingLabelTextField
        if married?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your married status", comment: "error message"))
            return false
        }
        return true
    }
    /// Gender Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateGender(textField: AnyObject, viewController: UIViewController) -> Bool {
        let married = textField as? SkyFloatingLabelTextField
        if married?.text?.validateNotEmpty() == false {
            viewController.showToastMessageTop(message:
                NSLocalizedString("Please enter your gender", comment: "error message"))
            return false
        }
        return true
    }
    /// Phone Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validatePhone(textField: AnyObject, viewController: UIViewController) -> Bool {
        let phone = textField as? SkyFloatingLabelTextField
        if (phone?.text?.validateNotEmpty())! {
            if (phone?.text!.characters.count)! < 6 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Phone number must be atleast 6 Characters",
                                      comment: "error message"))
                return false
            }
            if phone?.text?.validatePhone() == false {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a valid phone Number", comment: "error message"))
                return false
            }
        }
        return true
    }
}

