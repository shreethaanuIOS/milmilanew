//
//  File2.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import AlamofireImage
import MDNotificationView
import AVFoundation
extension UIViewController {
    /// Setting Space
    ///
    /// - Returns:BarButtonItem
    func setSapcer() -> UIBarButtonItem {
        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacer.width = 0
        return spacer
    }
    /// notification Type error Message display
    ///
    /// - Parameter message: Message as String
    func notificationMessageTop(message: String!) {
        let view = CSCustomNotificationView()
        view.frame.size.height = 60.0
        view.textLabel.text = message
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let notificationView = MDNotificationView(view: view, position: .top)
        notificationView.backgroundColor = UIColor.myBlueColour
        self.view.addSubview(notificationView)
        notificationView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            notificationView.hide()
            notificationView.removeFromSuperview()
        }
    }
    /// notification Type error Message display
    ///
    /// - Parameter message: Message as String
    func successNotificationMessageTop(message: String!) {
        let view = CSCustomNotificationView()
        view.frame.size.height = 60.0
        view.textLabel.text = message
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let notificationView = MDNotificationView(view: view, position: .top)
        notificationView.backgroundColor = UIColor.myBlueColour
        self.view.addSubview(notificationView)
        notificationView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            notificationView.hide()
            notificationView.removeFromSuperview()
        }
    }
    /// Check Normal Login or Social Login
    ///
    /// - Returns: true or false
    func checkNormalLogin() -> Bool {
        if CSSharedData.SharedInstance.getLoginType() == "1" {
            return true
        }
        return false
    }
    /// Show toast message position bottom
    ///
    /// - Parameter message: message to be shown
    func showSuccessToastMessage(message: String!) {
        self.successNotificationMessageTop(message: message)
    }
    /// Show toast message position Top
    ///
    /// - Parameter message: message to be shown
    func showToastMessageTop(message: String!) {
        self.notificationMessageTop(message: message)
    }
    /// is validating Image url is empty
    ///
    /// - Parameter string: image url
    /// - Returns: returns true
    func isValidateNillAndEmpty(string: String!) -> Bool {
        if (string != nil)&&(string != "") {
            return true
        } else {
            return false
        }
    }
}
// MARK: - Button extension to add Loader
extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            indicator.center = CGPoint(x: buttonWidth/2, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}
extension Int {
    /// valid is current page is first
    func validatePageLoader() -> Bool {
        guard self > 1 else { return false }
        return true
    }
}
// MARK: - Dictionary
extension Dictionary {
    /// merge dictionary
    mutating func merge(with dictionary: Dictionary) {
        dictionary.forEach { updateValue($1, forKey: $0) }
    }
    /// merger Dictionary
    func merged(with dictionary: Dictionary) -> Dictionary {
        var dict = self
        dict.merge(with: dictionary)
        return dict
    }
}
extension UIImageView {
    /// image with Url
    ///
    /// - Parameter imageURl: set the image with url
    /// - Returns: return Uiimage
    func setBannerImageWithUrl(imageURl: String!) {
        if isValidateNillAndEmpty(string: imageURl) {
            let imageString: String = imageURl.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed)!
            let url = URL(string: imageString)
            self.af_setImage(withURL: url!, placeholderImage: #imageLiteral(resourceName: "placeholder"))
        } else {
            self.image = #imageLiteral(resourceName: "placeholder")
            return
        }
    }
    /// image with Url
    ///
    /// - Parameter imageURl: set the image with url
    /// - Returns: return Uiimage
    func setImageWithUrl(imageURl: String!) {
        if isValidateNillAndEmpty(string: imageURl) {
            let imageString: String = imageURl.addingPercentEncoding(withAllowedCharacters:
                NSCharacterSet.urlQueryAllowed)!
            let url = URL(string: imageString)
            self.af_setImage(withURL: url!, placeholderImage: UIImage(named: "placeholderImage")!)
        } else {
            self.image = UIImage(named: "placeholderImage")!
            return
        }
    }
    /// is validating Image url is empty
    ///
    /// - Parameter string: image url
    /// - Returns: returns true
    override func isValidateNillAndEmpty(string: String!) -> Bool {
        if (string != nil)&&(string != "") {
            return true
        } else {
            return false
        }
    }
}
/// UItextfield max length for textfield
private var kAssociationKeyMaxLength: Int = 0
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            if let length = objc_getAssociatedObject(self, &kAssociationKeyMaxLength) as? Int {
                return length
            } else {
                return Int.max
            }
        }
        set {
            objc_setAssociatedObject(self, &kAssociationKeyMaxLength, newValue, .OBJC_ASSOCIATION_RETAIN)
            addTarget(self, action: #selector(checkMaxLength), for: .editingChanged)
        }
    }
    /// max character can be set her
    func checkMaxLength(textField: UITextField) {
        guard let prospectiveText = self.text,
            prospectiveText.characters.count > maxLength
            else {
                return
        }
        let selection = selectedTextRange
        let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
        text = prospectiveText.substring(to: maxCharIndex)
        selectedTextRange = selection
    }
    //// place holder color
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:
                self.placeholder != nil ? self.placeholder! : "",
                                                            attributes:  [NSForegroundColorAttributeName: newValue!])
        }
    }
}

extension UIView {
    /// Setting Space
    ///
    /// - Returns:BarButtonItem
    func setSapcer() -> UIBarButtonItem {
        let spacer = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
        spacer.width = 0
        return spacer
    }
    /// notification Type error Message display
    ///
    /// - Parameter message: Message as String
    func notificationMessageTop(message: String!) {
        let view = CSCustomNotificationView()
        view.frame.size.height = 60.0
        view.textLabel.text = message
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let notificationView = MDNotificationView(view: view, position: .top)
        notificationView.backgroundColor = UIColor.myBlueColour
        self.addSubview(notificationView)
        notificationView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            notificationView.hide()
            notificationView.removeFromSuperview()
        }
    }
    /// notification Type error Message display
    ///
    /// - Parameter message: Message as String
    func successNotificationMessageTop(message: String!) {
        let view = CSCustomNotificationView()
        view.frame.size.height = 60.0
        view.textLabel.text = message
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        let notificationView = MDNotificationView(view: view, position: .top)
        notificationView.backgroundColor = UIColor.myBlueColour
        self.addSubview(notificationView)
        notificationView.show()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            notificationView.hide()
            notificationView.removeFromSuperview()
        }
    }
    /// Check Normal Login or Social Login
    ///
    /// - Returns: true or false
    func checkNormalLogin() -> Bool {
        if CSSharedData.SharedInstance.getLoginType() == "1" {
            return true
        }
        return false
    }
    /// Show toast message position bottom
    ///
    /// - Parameter message: message to be shown
    func showSuccessToastMessage(message: String!) {
        self.successNotificationMessageTop(message: message)
    }
    /// Show toast message position Top
    ///
    /// - Parameter message: message to be shown
    func showToastMessageTop(message: String!) {
        self.notificationMessageTop(message: message)
    }
    /// is validating Image url is empty
    ///
    /// - Parameter string: image url
    /// - Returns: returns true
    func isValidateNillAndEmpty(string: String!) -> Bool {
        if (string != nil)&&(string != "") {
            return true
        } else {
            return false
        }
    }
}
