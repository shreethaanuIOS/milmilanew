//
//  userAddressValidation.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class CSUserAddressValidation: NSObject {
    /// Address Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateAddress(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let address = textField as? SkyFloatingLabelTextField
        if address?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a address", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a billing address", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a shipping address", comment: "error message"))
            }
            return false
        }
        return true
    }
    /// Country Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateCountry(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let country = textField as? SkyFloatingLabelTextField
        if country?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a country", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a billing country", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a shipping country", comment: "error message"))
            }
            return false
        }
        return true
    }
    /// State/Province Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateState(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let state = textField as? SkyFloatingLabelTextField
        if state?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a state/province", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a billing state/province", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a shipping state/province", comment: "error message"))
            }
            return false
        }
        return true
    }
    /// City Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateCity(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let city = textField as? SkyFloatingLabelTextField
        if city?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a city", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a billing city", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter a shipping city", comment: "error message"))
            }
            return false
        }
        return true
    }
    /// ZipCode Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateZipCode(textField: AnyObject, viewController: UIViewController, checkType: Int) -> Bool {
        let zipCode = textField as? SkyFloatingLabelTextField
        if zipCode?.text?.validateNotEmpty() == false {
            if checkType == 0 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter zipcode", comment: "error message"))
            }
            if checkType == 1 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter billing zipcode", comment: "error message"))
            }
            if checkType == 2 {
                viewController.showToastMessageTop(message:
                    NSLocalizedString("Please enter shipping zipcode", comment: "error message"))
            }
            return false
        }
        return true
    }
}

