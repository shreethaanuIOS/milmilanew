//
//  File.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//

import UIKit
import DropDown
import SkyFloatingLabelTextField
class CSDropDown: NSObject {
    /// Country Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateCountryList(_ country: [String], parentView: AnyObject) -> Bool {
        let parentViewController = parentView as? UIViewController
        if country.count == 0 {
            parentViewController?.showToastMessageTop(message:
                NSLocalizedString("Country list is empty", comment: "error message"))
            return false
        }
        return true
    }
    /// city Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateCityList(_ city: [String], parentView: AnyObject) -> Bool {
        let parentViewController = parentView as? UIViewController
        if city.count == 0 {
            parentViewController?.showToastMessageTop(message:
                NSLocalizedString("City list is empty", comment: "error message"))
            return false
        }
        return true
    }
    /// state Validation
    ///
    /// - Parameter textField: textfield
    /// - Returns: true or false
    class func validateStateList(_ state: [String], parentView: AnyObject) -> Bool {
        let parentViewController = parentView as? UIViewController
        if state.count == 0 {
            parentViewController?.showToastMessageTop(message:
                NSLocalizedString("State list is empty", comment: "error message"))
            return false
        }
        return true
    }
    /// Gender Dropdown To show gender list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textfield
    class func gnderDropDown(dropDown: DropDown!,
                             textField: AnyObject) {
        let dropDownTextField = textField as? SkyFloatingLabelTextField
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Male", "Female"]
        /// previous selected Index
        dropDown.selectRow(at: dropDownTextField?.tag) // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            dropDownTextField?.tag = index
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
    /// married Dropdown To show married list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textfield
    class func marriedDropDown(dropDown: DropDown!,
                               textField: AnyObject) {
        let dropDownTextField = textField as? SkyFloatingLabelTextField
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = ["Married", "Unmarried"]
        /// previous selected Index
        dropDown.selectRow(at: dropDownTextField?.tag) // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            dropDownTextField?.tag = index
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
    /// city Dropdown To show city list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textField
    ///   - completionHandler: any object
    class func cityDropDown(dropDown: DropDown!,
                            textField: AnyObject,
                            dataSource: [String],
                            completionHandler: @escaping (_ response: Int) -> Void) {
        let dropDownTextField = textField as? SkyFloatingLabelTextField
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = dataSource
        /// previous selected Index
        dropDown.selectRow(at: dropDownTextField?.tag) // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            dropDownTextField?.tag = index
            completionHandler(index)
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
    /// state Dropdown To show city list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textField
    ///   - completionHandler: any object
    class func stateDropDown(dropDown: DropDown!,
                             textField: AnyObject,
                             dataSource: [String],
                             completionHandler: @escaping (_ response: Int) -> Void) {
        let dropDownTextField = textField as? SkyFloatingLabelTextField
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = dataSource
        /// previous selected Index
        dropDown.selectRow(at: dropDownTextField?.tag) // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            dropDownTextField?.tag = index
            completionHandler(index)
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
    /// country Dropdown To show city list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textField
    ///   - completionHandler: any object
    class func countryDropDown(dropDown: DropDown!,
                               textField: AnyObject,
                               dataSource: [String],
                               completionHandler: @escaping (_ response: Int) -> Void) {
        let dropDownTextField = textField as? SkyFloatingLabelTextField
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = dataSource
        /// previous selected Index
        dropDown.selectRow(at: dropDownTextField?.tag) // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            dropDownTextField?.tag = index
            completionHandler(index)
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
    /// category Dropdown To show all category list
    ///
    /// - Parameters:
    ///   - dropDown: dropdown
    ///   - textField: textField
    ///   - completionHandler: any object
    class func categoryDropDown(dropDown: DropDown!,
                                textLabel: AnyObject,
                                dataSource: [String],
                                completionHandler: @escaping (_ response: Int) -> Void) {
        let dropDownTextField = textLabel as? UILabel
        dropDown.show()
        // Top of drop down will be below the anchorView
        dropDown.direction = .bottom
        // When drop down is displayed with `Direction.top`, it will be above the anchorView
        dropDown.topOffset = CGPoint(x: 0, y:60.0)
        /// Dismiss on Tab
        dropDown.dismissMode = .onTap
        // The view to which the drop down will appear on
        dropDown.anchorView = dropDownTextField
        // The list of items to display. Can be changed dynamically
        dropDown.dataSource = dataSource
        /// previous selected Index
        if dropDownTextField?.tag != -1 { dropDown.selectRow(at: dropDownTextField?.tag) } // Int?
        // Action triggered on selection
        dropDown.selectionAction = {(index: Int, item: String) in
            dropDownTextField?.text = item
            if dropDownTextField?.tag != index {
                dropDownTextField?.tag = index
                completionHandler(index)
            }
            dropDown.hide()
        }
        // Will set a custom width instead of the anchor view width
        dropDown.width = dropDownTextField?.frame.size.width
    }
}

