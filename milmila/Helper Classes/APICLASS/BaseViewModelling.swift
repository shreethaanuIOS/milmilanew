//
//  BaseViewModelling.swift
//  milmila
//
//  Created by prasun sarkar on 11/7/17.
//  Copyright © 2017 Milmila. All rights reserved.
//
import UIKit

class CSCBaseViewModel: NSObject {
    
    weak var presentationContext : UIViewController?
    
    convenience init(presentationContext : UIViewController) {
        self.init()
        self.presentationContext = presentationContext
    }
}
