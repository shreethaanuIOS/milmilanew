/*
 * ErroHandleBlock.swift
 * This class  is used to handle all type api error
 */
/// Error Handleing Block
class CSErroHandleBlock {
    /// Faliure case handler
    ///
    /// - Parameters:
    ///   - parentView: parent view controller
    ///   - message: message string
    class func statusCodefailure(parentView: AnyObject, errorCode: Int!, message: String) {
        let parentViewController = parentView as? CSParentViewController
        #if os(iOS)
            if errorCode == 404 {
                return
            }
            if errorCode == 403 {
                parentViewController?.openLoginScreenPopUp(viewController: parentViewController!)
                return
            }
        #elseif os(tvOS)
            if errorCode == 404 {
                parentViewController?.addChildView(identifier:
                    CSApplicationConstant.CSVideoNotAvailable,
                                                   storyBoard:
                    CSTVAlertIdentifiers.CSAlertStoryBoard)
                return
            }
        #endif
        CSSharedData.SharedInstance.displayErrorAlert(parentView: parentViewController!,
                                                      message: message)
    }
    /// Faliure case handler
    ///
    /// - Parameters:
    ///   - parentView: parent view controller
    ///   - message: message string
    class func failure(parentView: AnyObject, errorCode: Int) {
        let parentViewController = parentView as? CSParentViewController
        parentViewController?.stopAnimate()
        #if os(iOS)
            if errorCode == -1009 {
                parentViewController?.addChildView(identifier:
                    CSApplicationConstant.CSNoInternetViewIdentifier,
                                                   storyBoard:
                    CSApplicationConstant.CSAlertStoryboard)
            } else {
                parentViewController?.addChildView(identifier:
                    CSApplicationConstant.CSWhoopsViewIdentifier,
                                                   storyBoard:
                    CSApplicationConstant.CSAlertStoryboard)
            }
        #elseif os(tvOS)
            if errorCode == -1009 {
                parentViewController?.addChildView(identifier:
                    CSTVAlertIdentifiers.CSNoInternetIdentifier,
                                                   storyBoard:
                    CSTVAlertIdentifiers.CSAlertStoryBoard)
            } else {
                parentViewController?.addChildView(identifier:
                    CSTVAlertIdentifiers.CSWhoopsIdentifier,
                                                   storyBoard:
                    CSTVAlertIdentifiers.CSAlertStoryBoard)
            }
        #endif
    }
}

